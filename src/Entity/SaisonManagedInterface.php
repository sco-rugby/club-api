<?php

namespace App\Entity;

use App\Entity\Saison;

/**
 *
 * @author Antoine BOUET
 */
interface SaisonManagedInterface {

    public function getSaison(): ?Saison;

    public function setSaison(?Saison $saison): self;
}
