<?php

namespace App\Entity\Contact;

use Doctrine\ORM\Mapping as ORM;
use ScoRugby\CoreBundle\Exception\InvalidParameterException;
use Symfony\Component\Intl\Countries;
USE ScoRugby\CoreBundle\Entity\EntityInterface;

#[ORM\Entity]
#[ORM\Table(name: "contact_telephone")]
class ContactTelephone implements EntityInterface {

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Column(length: "2", options: ['comment' => 'Code pays ISO 3166-1 alpha-2'])]
    protected ?string $codePays = null;

    #[ORM\Column(length: "20", options: ['comment' => 'sans espace ni autre séparateur'])]
    protected ?string $numero = null;

    #[ORM\Embedded(class: TypeMoyenContact::class, columnPrefix: "tel_")]
    protected TypeMoyenContact $type;

    #[ORM\ManyToOne(inversedBy: 'telephones')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Contact $contact = null;

    public function __construct() {
        $this->type = new TypeMoyenContact();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCodePays(): ?string {
        return $this->codePays;
    }

    public function setCodePays(string $pays): self {
        $pays = strtoupper($pays);
        if (!Countries::exists($pays)) {
            throw new InvalidParameterException(sprintf('Le code %s n\'est pas un code pays valide (ISO 3166-1 alpha-2)', $pays));
        }
        $this->codePays = $pays;

        return $this;
    }

    public function getNumero(): ?string {
        return $this->numero;
    }

    public function setNumero(string $numero): self {
        $this->numero = $numero;

        return $this;
    }

    public function getType(): TypeMoyenContact {
        return $this->type;
    }

    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    public function setContact(?Contact $contact): static
    {
        $this->contact = $contact;

        return $this;
    }
}
