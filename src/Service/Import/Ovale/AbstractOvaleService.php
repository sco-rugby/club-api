<?php

namespace App\Service\Import\Ovale;

use App\Service\Import\AbstractFileImportService;
use App\Entity\Contact\Contact;
use App\Model\Contact\ContactInterface;
use App\Entity\Contact\ContactExterne;
use App\Entity\Contact\Adresse;
use App\Entity\Contact\ContactEmail;
use App\Entity\Contact\ContactTelephone;
use App\Model\Contact\TypeMoyenContact;
use App\Model\Contact\Contact as ContactModel;
use App\Entity\Saison;
use App\Manager\Contact\ContactManager;
use App\Manager\SaisonManager;
use App\Exception\ContactException;
use App\Model\AppliExterne;
use ScoRugby\TraitementBundle\Exception\FailedExecutionException;
use ScoRugby\TraitementBundle\Exception\NoExecutionException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\String\UnicodeString;
use App\Entity\Import\SingleSourceImport;
use App\Entity\Import\AppliMaitre;
use PhpOffice\PhpSpreadsheet\IOFactory;

//TODO : Suppression donnée saison KO
//TODO : Activer Transaction
abstract class AbstractOvaleService extends AbstractFileImportService {

    protected ArrayCollection $contacts;
    protected ArrayCollection $saisons;
    protected AppliMaitre $appli;
    protected SingleSourceImport $src;

    ############
    # Execution du service
    ############

    public function init(): void {
        // init properties
        $this->managers['saison'] = new SaisonManager($this->em->getRepository(Saison::class), $this->params, $this->dispatcher, $this->validator);
        $this->managers['contact'] = new ContactManager($this->em->getRepository(Contact::class), $this->dispatcher, $this->validator);
        $this->contacts = new ArrayCollection();
        $this->saisons = new ArrayCollection();
        $this->creations['contact'] = 0;
        //
        $this->appli = new AppliMaitre();
        $this->appli->setNom(AppliExterne::Ovale->value);
        $this->src = new SingleSourceImport();
        $this->src->setNom(AppliExterne::Ovale->value);
        $this->src->setSynchronizedAt();
        // parent init
        parent::init();
        // populate contacts
        try {
            $contacts = $this->managers['contact']->findAllForSearch();
            foreach ($contacts as $contact) {
                $this->setContactList($contact, null);
            }
        } catch (\Exception $e) {
            throw new FailedExecutionException($e->getMessage());
        }
    }

    public function load(\SplFileInfo $fichier): void {
        try {
            if (!$fichier->isReadable()) {
                throw new NoExecutionException(sprintf('Le fichier %s ne peut pas être lu', $fichier));
            }
            if ($this->hasLogger()) {
                $this->getLogger()->notice(sprintf('Chargement du fichier %s', $fichier));
            }
            $this->fichier->setFichier($fichier);
            $reader = IOFactory::createReaderForFile($fichier->getPathname());
            $reader->setReadDataOnly(true);
            $reader->setDelimiter(',');
            $this->spreadsheet = $reader->load($fichier);
        } catch (\Exception $e) {
            throw new FailedExecutionException($e->getMessage());
        }
    }

    ############
    # Contact
    ############

    protected function isContactCreated(string $nom, string $prenom, ?\DateTimeInterface $dob, ?string $cp): bool {
        return (null !== $this->findContact($nom, $prenom, $dob, $cp));
    }

    protected function setContactList(array $data, ?ContactInterface $contact) {
        if (!key_exists('id', $data) & !key_exists('licence', $data)) {
            $data['id'] = uniqid();
        } elseif (null == $data['id']) {
            $data['id'] = uniqid();
        }
        $data['contact'] = $contact;
        $this->contacts->set($data['id'], $data);
    }

    protected function findContact(string $nom, string $prenom, ?\DateTimeInterface $dob, ?string $cp, bool $id = false): Contact|string|int|null {
        $model = new ContactModel();
        $model->setNom($nom);
        $model->setPrenom($prenom);
        $model->getAdresse()->setCodePostal($cp);
        //
        $this->managers['contact']->normalizeNom($model);
        $normalizer = $this->managers['contact']->createNormalizer();
        $canonizedNom = $normalizer->canonize($model);
        //
        $exprBuilder = Criteria::expr();
        $expr = $exprBuilder->eq('canonizedNom', $canonizedNom);
        $result = $this->contacts->matching(new Criteria($expr));
        if ($result->isEmpty()) {
            return null;
        } elseif (count($result) > 1) {
            forEach ($result as $row) {
                if (null !== $dob) {
                    if ($dob == $row['dateNaissance']) {
                        $result = new ArrayCollection();
                        $result->add($row);
                        break;
                    }
                } else {
                    if ($cp == $row['adresse.codePostal']) {
                        $result = new ArrayCollection();
                        $result->add($row);
                        break;
                    }
                }
            }
            if (count($result) > 1) {
                throw new ContactException(sprintf('%s contacts trouvés avec le même nom (%s %s) et code postal (%s) ou date de naissance', count($result), $nom, $prenom, $cp));
            }
        }
        $data = $result->first();
        if (true === $id) {
            return $data['id'];
        } elseif (null === $data['contact']) {
            $contact = $this->managers['contact']->get(intval($data['id']));
            $this->setContactList($data, $contact);
        } else {
            $contact = $data['contact'];
        }
        return $contact;
    }

    /**
     * Créer ou mettre à jour un contact (en fonction contexte)
     * 
     * si mettre à jour UNIQUEMENT le contact : passer un array $context non null
     * si $context contient 'all' = 'email','telephone','adresse'
     * $context['create'] = true : créer un nv contact (par défault)
     * $context['create'] = false : retrouver dans liste $this->contacts par $data['id'], dans ce cas, préciser les données à mettre à jour
     * 
     * @param array $data
     * @param array $context 'email','telephone','adresse', 'all','create' => true|false
     * @return Contact
     */
    protected function createContact(array $data, array $context = []): Contact {
        if ($this->hasLogger()) {
            $this->logger->info("Création contact " . $data['id'], $data);
            $this->logger->debug(__METHOD__);
        }
        if (empty($context)) {
            $context['create'] = true;
            $context[] = 'email';
            $context[] = 'telephone';
            $context[] = 'adresse';
        } else {
            if (in_array('all', $context, true)) {
                $context[] = 'email';
                $context[] = 'telephone';
                $context[] = 'adresse';
            }
            if (!key_exists('create', $context)) {
                $context['create'] = true;
            }
        }
        if (key_exists('autorisation_ffr', $data)) {
            $listeRouge = $this->toBoolean($data['autorisation_ffr']);
        } else {
            $listeRouge = true;
        }
        // Contact
        if (true === $context['create']) {
            $contact = $this->managers['contact']->createObject();
        } else {
            $recherche = $this->contacts->get($data['id']);
            $contact = $recherche['contact'];
        }
        if (!($contact instanceOf Contact)) {
            throw new ContactException(sprintf('Le contact %s %s DOIT être une instance de %s, %s fourni.', $data['nom'], $data['prenom'], Contact::Class, get_class($contact)));
        }
        $contact->setPublic(false)
                ->setListeRouge($listeRouge)
                ->setNom($data['nom'])
                ->setPrenom($data['prenom'])
                ->setGenre($this->convertirGenre($data['sexe']))
                ->getDateTime()->setCreatedAt($this->src->getSynchronizedAt());
        // Contact externe
        if (in_array('email', $context, true) | in_array('telephone', $context, true)) {
            if (key_exists('licence', $data)) {
                $id = $data['licence'];
            } else {
                $id = $data['email'];
            }
            $appli = new ContactExterne();
            $appli->setAppliMaitre($this->appli);
            $appli->setExternalId($id);
            $appli->setSynchronizedAt($this->src->getSynchronizedAt());
            $contact->addApplisExterne($appli);
            //
            if (in_array('email', $context)) {
                $this->addContactEmail($contact, $data);
            }
            if (in_array('telephone', $context)) {
                $this->addContactPhone($contact, $data);
            }
        }
        // Adresse
        if (in_array('adresse', $context, true)) {
            $contact->setAdresse($this->convertirAdresse($data));
        }
        //
        $this->managers['contact']->setResource($contact);
        $contact = $this->managers['contact']->getResource();
        if (true === $context['create']) {
            $this->creations['contact']++;
        }
        // ajout liste
        $dataListe = [
            "id" => $data['id'],
            "canonizedNom" => $contact->getCanonizedNom(),
            "adresse.codePostal" => $contact->getAdresse()->getCodePostal(),
            "dateNaissance" => null,
        ];
        if (key_exists('date_naissance', $data)) {
            $dataListe['dateNaissance'] = $data['date_naissance'];
        }
        $this->setContactList($dataListe, $contact);
        //
        return $contact;
    }

    protected function addContactEmail(Contact &$contact, array $data): void {
        if (empty($data['email'])) {
            return;
        }
        $email = new ContactEmail();
        $email->setEmail($data['email']);
        $email->getType()->setPrefere(true);
        $email->getType()->setId(TypeMoyenContact::domicile->value);
        $contact->addEmail($email);
    }

    protected function addContactPhone(Contact &$contact, array $data): void {
        $pref = false;
        foreach (['dom' => 'D', 'pro' => 'P', 'port' => 'M'] as $field => $type) {
            if (!empty($data['telephone_' . $field])) {
                $str = (new UnicodeString($data['telephone_' . $field]))
                        ->replace(' ', '')
                        ->replaceMatches('/^\+[0-9]+/', '')
                        ->ensureStart('0');
                $no = $str->split('-');
                $no = (new UnicodeString($no[0]))->truncate(20);
                $tel = new ContactTelephone();
                $tel->setNumero($no);
                if ('port' == $field) {
                    $tel->getType()->setPrefere(true);
                    $pref = true;
                } else {
                    $tel->getType()->setPrefere(false);
                }
                $tel->getType()->setId($type);
                $tel->setCodePays('FR');
                $contact->addTelephone($tel);
            }
        }
        if (false === $pref && !$contact->getTelephones()->isEmpty()) {
            $contact->getTelephones()->first()->getType()->setPrefere(true);
        }
    }

    ############
    # Convertion
    ############

    protected function convertirDate2Saison(string $date): Saison {
        return $this->saisonManager->findByDate($this->convertirDate($date));
    }

    protected function convertirGenre(string $genre): ?string {
        return match ($genre) {
            'Masculin' => 'M',
            'Féminin' => 'F',
            default => null,
        };
    }

    /**
     * Define abstract method AbstractImportService::convertirAdresse()
     * @inheritdoc
     */
    protected function convertirAdresse(array $data): Adresse {
        if ($this->hasLogger()) {
            $this->logger->debug(__METHOD__, $data);
        }
        $complement = [];
        forEach (['lieu-dit', 'immeuble', 'complement'] as $elmt) {
            if (!empty($data[$elmt])) {
                $complement[] = $data[$elmt];
            }
        }
        //
        if (array_key_exists('voie', $data)) {
            $data['rue'] = $data['voie'];
        }
        if (array_key_exists('localite', $data)) {
            $data['ville'] = $data['localite'];
        }
        $adresse = new Adresse();
        $adresse->setAdresse($data['rue']);
        $adresse->setComplement(implode(' - ', $complement));
        $adresse->setCodePostal($data['code_postal']);
        $adresse->setVille($data['ville']);
        $adresse->setPays('FR');
        //
        return $adresse;
    }

    ############
    # Saison
    ############

    protected function convertir($annee): int {
        if (is_string($annee)) {
            $annee = substr($annee, 0, 4);
        }
        return intval($annee);
    }

    protected function findSaison($annee): ?Saison {
        $annee = $this->convertir($annee);
        if ($this->saisons->containsKey($annee)) {
            return $this->saisons->get($annee);
        } else {
            $saison = $this->managers['saison']->convertir($annee);
            $this->saisons->set($saison->getId(), $saison);
            return $this->saisons->get($saison->getId());
        }
        /* $saison = $this->managers['saison']->convertir($annee);
          if (!$this->saisons->containsKey($saison->getId())) {
          $this->saisons->set($saison->getId(), $saison);
          }
          return $this->saisons->get($saison->getId()); */
    }

    protected function setSaison(Saison $saison): void {
        if (!$this->saisons->containsKey($saison->getId())) {
            $this->saisons->set($saison->getId(), $saison);
        }
    }
}
