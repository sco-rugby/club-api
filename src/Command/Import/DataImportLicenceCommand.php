<?php

namespace App\Command\Import;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Model\Import\RapportOvale;
use App\Service\Import\Ovale\ImportLicence;
use ScoRugby\CoreBundle\Exception\Execution\FailedExecutionException;
use ScoRugby\CoreBundle\Exception\Execution\NoExecutionException;

#[AsCommand(
            name: ImportLicence::JOB,
            description: 'Importer la liste des licences d\'un fichier oval-e',
    )]
final class DataImportLicenceCommand extends AbstractDataImportCommand {

    protected function configure(): void {
        parent::configure();
        $this->setHelp(str_replace('<br />', "\n", RapportOvale::OVALE2001->getDescription()));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $io = new SymfonyStyle($input, $output);
        $fichier = $input->getArgument('fichier');
        //
        $io->info(sprintf('Import des licences du fichier %s', $fichier));
        $service = new ImportLicence($this->em, $this->dispatcher, $this->validator, $this->params, $this->logger, $io->createProgressBar());
        try {
            //
            $io->block('Initialisation de l\'import');
            $service->init();
            //
            $io->block('Import du fichier');
            $service->import(new \SplFileInfo($fichier));
            //
            $io->block(" ");
            $event = $service->getEvent();
            $io->success(implode("\n", $event->getCompteRendu())); 
            $result = Command::SUCCESS;
        } catch (NoExecutionException $ex) {
            $io->success($ex->getMessage());
            $result = Command::SUCCESS;
        } catch (FailedExecutionException $ex) {
            $io->warning($ex->getMessage());
            $result = Command::SUCCESS;
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());
            $result = Command::FAILURE;
        } finally {
            $service->shutdown();
        }
        return $result;
    }
}
