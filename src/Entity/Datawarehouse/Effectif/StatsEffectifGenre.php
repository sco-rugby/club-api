<?php

namespace App\Entity\Datawarehouse\Effectif;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
class StatsEffectifGenre extends StatsEffectif {

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2, nullable: true)]
    protected $prcSection = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2, nullable: true)]
    protected $prcGenre = null;

    public function setEffectifSection(int $nb): self {
        $this->prcSection = $this->calculerPourcentage($nb);
        return $this;
    }

    public function setEffectifGenre(int $nb): self {
        $this->prcGenre = $this->calculerPourcentage($nb);
        return $this;
    }

}
