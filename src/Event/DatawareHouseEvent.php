<?php

namespace App\Event;

/**
 * Description of DatawareHouseEvent
 *
 * @author Antoine BOUET
 */
class DatawareHouseEvent {

    public const INIT = 'datawarehouse.init';
    public const BUILD = 'datawarehouse.build';
    public const SHUTDOWN = 'datawarehouse.shutdown';
    public const EXCEPTION = 'datawarehouse.exception';
}
