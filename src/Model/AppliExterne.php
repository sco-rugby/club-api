<?php

namespace App\Model;

/**
 *
 * @author Antoine BOUET
 */
enum AppliExterne: string {

    case Ovale = 'Oval-e';
    case HelloAsso = 'Helloasso';
    case OVH = 'OVH';
}
