<?php

namespace App\Command\Datawarehouse;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\Datawarehouse\BuildLicence;

#[AsCommand(
            name: 'datawarehouse:delete:licence',
            description: 'Supprimer les données "licences" de l\'entrepôt de données',
    )]
class LicenceDeleteCommand extends AbstractDeleteCommand {

    protected function configure(): void {
        parent::configure();
        $this->addOption('global', 'g', InputOption::VALUE_NONE, 'Calculer les stats pour saison en cours');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $io = new SymfonyStyle($input, $output);
        try {
            $result = parent::execute($input, $output);
            if (Command::SUCCESS != $result) {
                return $result;
            }
            //
            if ($input->getOption('complet')) {
                $service = new BuildLicence($this->em, $this->saisonManager, $this->dispatcher, $this->logger, $io->createProgressBar());
                $nb = $service->purgeLicenceAggregee();
                $io->success(sprintf('%s licences aggrégées supprimées', $nb));
            }
            return Command::SUCCESS;
        } catch (NoExecutionException $ex) {
            $io->info($ex->getMessage());
            return Command::SUCCESS;
        } catch (FailedExecutionException $ex) {
            $io->warning($ex->getMessage());
            return Command::FAILURE;
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());
            return Command::FAILURE;
        }
    }

    protected function deleteProcess(int $annee, SymfonyStyle $io): void {
        $io->info(sprintf('Supression des données "licence" pour la saison %s', $annee));
        $saison = $this->saisonManager->convertir($annee);
        $service = new BuildLicence($this->em, $this->saisonManager, $this->dispatcher, $this->logger, $io->createProgressBar());
        $nb = $service->purge($saison);
        $service->shutdown();
        $io->success(sprintf('%s licences supprimées', $nb));
    }
}
