<?php

namespace App\Webhook\Consumer;

use Symfony\Component\RemoteEvent\Attribute\AsRemoteEventConsumer;
use Symfony\Component\RemoteEvent\Consumer\ConsumerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\RemoteEvent\RemoteEvent;
use App\Service\Import\Helloasso\ImportPayment;
use App\Service\Import\Helloasso\ImportOrder;

#[AsRemoteEventConsumer('helloasso')]
final class HelloassoWebhookConsumer implements ConsumerInterface {

    public function __construct(private EntityManagerInterface $em, private EventDispatcherInterface $dispatcher) {
        return;
    }

    public function consume(RemoteEvent $event): void {
        switch ($event->getId()) {
            case Event::EVENT_TYPE_FORM :
                $service = new ImportForm($this->em, $this->dispatcher);
                $service->import($event->getPayload());
                break;
            case Event::EVENT_TYPE_ORDER :
                $service = new ImportOrder($this->em, $this->dispatcher);
                $service->import($event->getPayload());
                break;
            case Event::EVENT_TYPE_PAYMENT :
                $service = new ImportPayment($this->em, $this->dispatcher);
                $service->import($event->getPayload());
                break;
        }
    }
}
