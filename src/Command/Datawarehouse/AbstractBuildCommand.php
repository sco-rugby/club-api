<?php

namespace App\Command\Datawarehouse;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Manager\SaisonManager;
use ScoRugby\CoreBundle\Exception\Execution\FailedExecutionException;
use ScoRugby\CoreBundle\Exception\Execution\NoExecutionException;

abstract class AbstractBuildCommand extends Command {

    public function __construct(protected EntityManagerInterface $em, protected SaisonManager $saisonManager, protected EventDispatcherInterface $dispatcher, protected ?LoggerInterface $logger) {
        parent::__construct();
    }

    protected function configure(): void {
        $this
                ->addArgument('debut', InputArgument::OPTIONAL, 'Saison de début')
                ->addArgument('fin', InputArgument::OPTIONAL, 'Saison de fin')
                ->addOption('courante', 'c', InputOption::VALUE_NONE, 'Calculer les stats pour saison en cours')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $io = new SymfonyStyle($input, $output);
        try {
            if ($input->getOption('courante')) {
                $saison = $this->saisonManager->findEnCours();
                $input->setArgument('debut', $saison->getId());
                $input->setArgument('fin', $saison->getId());
            } elseif (!$input->getArgument('debut')) {
                $saison = $this->saisonManager->findEnCours();
                $argAnnee = $io->ask($this->getDefinition()->getArgument('debut')->getDescription() . ' ?', $saison->getId());
                $input->setArgument('debut', $argAnnee);
                $input->setArgument('fin', $argAnnee);
            } elseif ($input->getArgument('debut') && !$input->getArgument('fin')) {
                $input->setArgument('fin', $input->getArgument('debut'));
            }
            $debut = $this->saisonManager->convertir($input->getArgument('debut'));
            $fin = $this->saisonManager->convertir($input->getArgument('fin'));
            for ($i = $debut->getId(); $i <= $fin->getId(); $i++) {
                $this->buildProcess($i, $io);
            }
            return Command::SUCCESS;
        } catch (NoExecutionException $ex) {
            $io->info($ex->getMessage());
            return Command::SUCCESS;
        } catch (FailedExecutionException $ex) {
            $io->warning($ex->getMessage());
            return Command::FAILURE;
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());
            return Command::FAILURE;
        }
    }

    abstract protected function buildProcess(int $annee, SymfonyStyle $io): void;
}
