<?php

namespace App\Entity\Contact;

use App\Repository\Contact\ContactExterneRepository;
use Doctrine\ORM\Mapping as ORM;
USE ScoRugby\CoreBundle\Entity\EntityInterface;
use App\Entity\Import\ExternalReference;

#[ORM\Entity]
#[ORM\Table(name: "contact_externe")]
class ContactExterne extends ExternalReference implements EntityInterface {

    #[ORM\ManyToOne(inversedBy: 'applisExternes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Contact $contact = null;

    public function getContact(): ?Contact {
        return $this->contact;
    }

    public function setContact(?Contact $contact): self {
        $this->contact = $contact;

        return $this;
    }
}
