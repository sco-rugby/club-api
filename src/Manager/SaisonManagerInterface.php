<?php

namespace App\Manager;

use App\Entity\Saison;

interface SaisonManagerInterface {

    public function getSaison(): ?Saison;

    public function setSaison(Saison $saison);
}
