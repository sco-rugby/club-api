<?php

namespace App\Service\Import;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Psr\Log\LoggerInterface;
use App\Service\CommandTrait;
use ScoRugby\CoreBundle\Service\TransactionTrait;

abstract class AbstractImportService implements ImportServiceInterface {

    use CommandTrait,
        TransactionTrait;

    protected int $lecture = 0;
    protected $managers = [];
    protected $creations = [];
    protected $cr = [];

    public function __construct(protected EntityManagerInterface $em, protected EventDispatcherInterface $dispatcher, protected ?ValidatorInterface $validator = null, protected ?ContainerBagInterface $params = null, protected ?LoggerInterface $logger = null, protected ?ProgressBar $progressBar = null) {
        return;
    }

    ############
    # Execution du service
    ############

    public function init(): void {
        /*if ($this->hasLogger()) {
            $this->getLogger()->info("Initialisation de l'import");
        }
        $this->initTransaction();*/
    }

    public function shutdown(): void {
        //$this->shutDownTransaction();
        $this->endCommand();
    }

    ############
    # Méthodes spécifiques
    ############

    protected function hasCompteRendu(): bool {
        return count($this->cr) > 0;
    }

    protected function getCompteRendu(): array {
        return $this->cr;
    }

    protected function setCompteRendu(array $cr): self {
        $this->cr = $cr;
        return $this;
    }

    protected function addCompteRendu(string $msg): void {
        $this->cr[] = $msg;
    }

    protected function hasLogger(): bool {
        return !is_null($this->logger);
    }

    protected function getLogger(): LoggerInterface {
        return $this->logger;
    }

    public function __destruct() {
        if ($this->hasLogger()) {
            $this->logger->debug(static::class);
        }
        $this->shutdown();
    }
}
