<?php

namespace App\Command\Datawarehouse;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\Datawarehouse\BuildEffectif;
use ScoRugby\CoreBundle\Exception\Execution\FailedExecutionException;
use ScoRugby\CoreBundle\Exception\Execution\NoExecutionException;

#[AsCommand(
            name: BuildEffectif::JOB,
            description: 'Construit les tables liées aux infos des effectifs dans l\'entrepôt de données',
    )]
class EffectifBuildCommand extends AbstractBuildCommand {

    protected function buildProcess(int $annee, SymfonyStyle $io): void {
        $saison = $this->saisonManager->get($annee);

        $io->info(sprintf('Calcul des données "effectif" pour la saison %s', $annee));
        $service = new BuildEffectif($this->em, $this->saisonManager, $this->dispatcher, $this->logger, $io->createProgressBar());
        try {
            $io->block('Initialisation du traitement');
            $service->init($saison);

            $io->block(" ");
            $io->block(sprintf('Construction des stats %s du club', $annee));
            try {
                $service->buildStatsClub();
            } catch (NoExecutionException $ex) {
                $io->warning($ex->getMessage());
            }
            $io->block(" ");
            $io->block(sprintf('Construction des stats %s par section', $annee));
            try {
                $service->buildStatsSection();
            } catch (NoExecutionException $ex) {
                $io->warning($ex->getMessage());
            }

            $io->block(" ");
            $io->block("Enregistrement des données");
            try {
                $service->buildData();
            } catch (\Exception $ex) {
                throw new FailedExecutionException(sprintf('Erreur enregistrements données : %s', $ex->getMessage()));
            }
            $io->block(" ");
            $io->success(sprintf('Les stats "effectif" %s sont calculées', $annee));
        } catch (Exception $ex) {
            throw $ex;
        } finally {
            $service->shutdown();
        }
    }
}
