<?php

namespace App\Service\Datawarehouse;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Saison;
use App\Model\SaisonInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Console\Helper\ProgressBar;
use Psr\Log\LoggerInterface;
use App\Manager\SaisonManager;
use App\Service\CommandTrait;
use ScoRugby\CoreBundle\Service\TransactionTrait;
use ScoRugby\CoreBundle\Exception\Execution\FailedExecutionException;

Abstract class AbstractBuildService implements DatawarehouseBuilderInterface {

    use CommandTrait,
        TransactionTrait;

    protected ?Saison $saison;

    public function __construct(protected EntityManagerInterface $em, protected SaisonManager $saisonManager, protected EventDispatcherInterface $dispatcher, protected ?LoggerInterface $logger = null, protected ?ProgressBar $progressBar = null) {
        return;
    }
/**
 * 
 * @param SaisonInterface $saison
 * @return void
 * @throws FailedExecutionException
 * @throws \InvalidArgumentException
 * @throws EntityNotFoundException
 */
    public function init(SaisonInterface $saison): void {
        try {
            $saisonArray = (array) $saison; // cast to array to check if empty
            if (empty($saisonArray)) {
                throw new \InvalidArgumentException('La saison doit être renseignée');
            } elseIf (null === $this->saison = $this->em->getRepository(Saison::class, 'default')->find($saison->getId())) {
                throw new EntityNotFoundException(sprintf('La saison %s n\'existe pas', $saison->getId()));
            }
            $this->initTransaction();
            $this->purge($saison);
        } catch (\Exception $e) {
            $this->setRollback();
            throw new FailedExecutionException($e->getMessage());
        }
    }
/**
 * Shutdown process
 * 
 * @return void
 */
    public function shutdown(): void {
        $this->shutDownTransaction(); // commit or rollbach transaction
    }

    protected function hasLogger(): bool {
        return !is_null($this->logger);
    }

    protected function getLogger(): LoggerInterface {
        return $this->logger;
    }
}
