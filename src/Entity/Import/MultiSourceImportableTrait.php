<?php

namespace App\Entity\Import;

use Doctrine\Common\Collections\Collection;

trait MultiSourceImportableTrait {

    public function addApplisExterne(ExternalReferenceInterface $applisExterne): self {
        if (!$this->ApplisExternes->contains($applisExterne)) {
            $this->ApplisExternes->add($applisExterne);
            $applisExterne->setOrganisation($this);
        }

        return $this;
    }

    public function removeApplisExterne(ExternalReferenceInterface $applisExterne): self {
        if ($this->ApplisExternes->removeElement($applisExterne)) {
            // set the owning side to null (unless already changed)
            if ($applisExterne->getOrganisation() === $this) {
                $applisExterne->setOrganisation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OrganisationExterne>
     */
    public function getApplisExternes(): Collection {
        return $this->applisExternes;
    }

    /**
     * @return Collection<int, OrganisationExterne>
     */
    public function setApplisExternes(Collection $applisExternes): self {
        $this->applisExternes = $applisExternes;
        return $this;
    }
}
