<?php

namespace App\Repository\Affilie;

use App\Entity\Affilie\Affilie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\ImportableRepositoryInferface;
use App\Entity\Import\ImportableInterface;

/**
 * @extends ServiceEntityRepository<Affilie>
 *
 * @method Affilie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Affilie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Affilie[]    findAll()
 * @method Affilie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffilieRepository extends ServiceEntityRepository implements ImportableRepositoryInferface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Affilie::class);
    }

    public function save(Affilie $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Affilie $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByExternalId(string $externalId): ?ImportableInterface {
        return $this->findBy(['externalId' => $externalId]);
    }

    public function findByAppliMaitre(string $appliMaitre): array {
        return $this->findBy(['appliMaitre' => $appliMaitre]);
    }

    public function findByImportedDate(\DateTimeInterface $date): array {
        return $this->findBy(['importedAt' => $date]);
    }
}
