<?php

namespace App\Service\Import;

interface ImportServiceInterface {

    public function import(mixed $data): void;
}
