<?php

namespace App\Model\Import;

use Doctrine\ORM\EntityManagerInterface;
use App\Service\Import\Helloasso\ImportAdhesion;
use App\Service\Import\Helloasso\ImportForm;
use App\Service\Import\Helloasso\ImportInscription;
use App\Service\Import\Helloasso\ImportOrder;
use App\Service\Import\Helloasso\ImportPayment;

enum HelloAsso: string implements ImportInterface {

    case Adhesion = 'adhésions';
    case Form = 'form';
    case Inscription = 'inscriptions';
    case Order = 'orders';
    case Payment = 'paiement';

    public function createService(EntityManagerInterface $em): ImportServiceInterface {
        $service = match ($this) {
            HelloAsso::Adhesion => ImportAdhesion::class,
            HelloAsso::Form => ImportForm::class,
            HelloAsso::Inscription => ImportInscription::class,
            HelloAsso::Order => ImportOrder::class,
            HelloAsso::Payment => ImportPayment::class,
        };
        return new $service($em);
    }

    public function getDescription(): string {
        return match ($this) {
            HelloAsso::Adhesion => 'Liste des paiements des adhésions',
            HelloAsso::Form => '',
            HelloAsso::Inscription => 'Listes des inscriptions à un événement',
            HelloAsso::Order => 'Listes des orders',
            HelloAsso::Payment => 'Listes des paiements',
        };
    }

    public function getMapping(): array {
        return match ($this) {
            HelloAsso::Adhesion => [],
            HelloAsso::Form => [],
            HelloAsso::Inscription => [],
            HelloAsso::Order => [],
            HelloAsso::Payment => [],
        };
    }
}
