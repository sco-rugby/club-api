<?php

namespace App\Manager\Evenement;

use ScoRugby\CoreBundle\Manager\AbstractDispatchingManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Repository\Evenement\EvenementRepository;
use App\Entity\Import\SingleSourceImport;
use Helloasso\Models\Forms\FormPublicModel;

final class EvenementManager extends AbstractDispatchingManager {

    public function __construct(EvenementRepository $repository, ?EventDispatcherInterface $dispatcher = null, ?ValidatorInterface $validator = null, ?FormFactoryInterface $formFactory = null) {
        parent::__construct($repository, $dispatcher, $validator, $formFactory);
    }

    private function getAppliExterne(string $id): SingleSourceImport {
        return (new SingleSourceImport())
                        ->setNom(AppliExterne::HelloAsso->value)
                        ->setSynchronizedAt()
                        ->setExternalId($id);
    }

    protected function populate($properties): void {
        if (!is_object($properties)) {
            throw new InvalidResourceException(sprinf('', get_type($properties)));
        } elseif (!($properties instanceof FormPublicModel)) {
            throw new InvalidResourceException(sprinf('', get_class($properties)));
        }
        $event->setLibelle($data->getTitle())
                ->setDescription($data->getDescription())
                ->setDebut($data->getStartDate())
                ->setFin($data->getEndDate())
                ->setAppliMaitre($this->getAppliExterne($data->getI));
        if (true !== $this->validate()) {
            throw new InvalidResourceException();
        }
    }
}
