<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use App\Entity\Club\Fonction;

/**
 * @author Antoine BOUET
 */
final class FonctionFixture extends AbstractDataFixture implements FixtureGroupInterface {

    public function load(ObjectManager $manager) {
        foreach ($this->loadDataFile() as $data) {
            $fonction = new Fonction();
            $fonction->setId($data['id']);
            $fonction->setLibelle($data['libelle']);
            if (!empty($data['slug'])) {
                $fonction->getSlug()->setString($data['slug']);
            } else {
                $fonction->getSlug()->setString($data['libelle']);
            }
            if (null != $data['description']) {
                $fonction->setDescription($data['description']);
            }
            $manager->persist($fonction);
        }
        $manager->flush();
    }

    public static function getGroups(): array {
        return ['club'];
    }
}
