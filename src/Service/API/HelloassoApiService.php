<?php

namespace App\Service\API;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Psr\Log\LoggerInterface;
use App\Model\Helloasso\FormType;
use App\Model\Helloasso\QueryParams;
use Symfony\Component\Yaml\Yaml;

/**
 * Description of HelloassoApiService
 *
 * @author Antoine BOUET
 */
final class HelloassoApiService implements APIServiceInterface {

    CONST FILE = '../config/packages/parameter.yaml';

    private ?string $accessToken;
    private ?string $refreshToken;
    private ?string $jwtToken;
    private array $queryParams = [];
    private ?int $expires = null; //The lifetime in seconds of the access token

    public function __construct(private HttpClientInterface $client, private EntityManagerInterface $em, private EventDispatcherInterface $dispatcher, private ValidatorInterface $validator, private ContainerBagInterface $params, private LoggerInterface $logger) {
        return;
    }

    /**
     * @inheritDoc
     */
    public function connect(): void {
        if ($this->params->has('appli.helloasso.refresh_time')) {
            $refreshDate = new \DateTimeImmutable($this->params->get('appli.helloasso.refresh_time'));
        } else {
            $refreshDate = new \DateTimeImmutable();
        }
        $interval = $refreshDate->diff(new \DateTimeImmutable());
        if (0 == $interval->format('%a')) {
            $request = $this->client->request(
                    'POST',
                    $this->params->get('appli.helloasso.oauth_api') . '/token',
                    [
                        'headers' => [
                            'Content-Type' => 'application/x-www-form-urlencoded',
                        ],
                        'body' => [
                            'client_id' => $this->params->get('appli.helloasso.client_id'),
                            'client_secret' => $this->params->get('appli.helloasso.secret'),
                            'grant_type ' => 'client_credentials'
                        ],
                    ]
            );
            $response = $request->toArray();
            $this->accessToken = $response['access_token'];
            $this->refreshToken = $response['refresh_token'];
            $this->expires = $response['expires_in'];
            //store token + expiry date
            $params = Yaml::parseFile(self::FILE);
            $now = new \DateTimeImmutable();
            $duration = new \DateInterval(sprintf('P%sS', $this->expires));
            $params['appli']['helloasso']['refresh_token'] = $this->refreshToken;
            $params['appli']['helloasso']['refresh_time'] = $now->add($duration)->format('c');
            $yaml = Yaml::dump($params);
            file_put_contents(self::FILE, $yaml);
        } else {
            $this->refreshToken = $this->params->has('appli.helloasso.refresh_token');
        }
        $this->refresh();
    }

    /**
     * @inheritDoc
     */
    public function refresh(): void {
        $request = $this->client->request(
                'POST',
                $this->params->get('appli.helloasso.oauth_api') . '/token',
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                    'body' => [
                        'client_id' => $this->params->get('appli.helloasso.client_id'),
                        'grant_type ' => 'refresh_token',
                        'refresh_token' => $this->refreshToken,
                    ],
                ]
        );
        $response = $request->toArray();
        $this->jwtToken = $response['access_token'];
        $this->expires = $response['expires_in'];
    }

    /**
     * @inheritDoc
     */
    public function disconnect(): void {
        $request = $this->client->request(
                'GET',
                $this->params->get('appli.helloasso.oauth_api') . '/disconnect',
                [
                    'headers' => [
                        'Authorization' => $this->getAccessToken(),
                    ]
                ]
        );
    }

    /**
     * Forms
     */

    /**
     * Create a simplified event for an Organism
     */
    public function createEvent($data) {
        /* POST
          /organizations/{organizationSlug}/forms/{formType}/action/quick-create */
    }

    /**
     * Get a list of formTypes
     * 
     * @param array $states List of Form States to filter with. If none specified, it won't filter results.
     * @return void
     */
    public function getFormTypes(array $states = []): ResponseInterface {
        $this->clearQueryParams();
        if (!empty($states)) {
            $this->addQueryParams('states', $states);
        }
        return $this->send('GET', $this->params->get('appli.helloasso.api_base') . '/organizations/' . $this->params->get('appli.helloasso.slug') . '/formTypes');
    }

    /**
     *  Get the forms
     * 
     * @return ResponseInterface
     */
    public function getForms(): ResponseInterface {
        $this->clearQueryParams();
        return $this->send('GET', $this->params->get('appli.helloasso.api_base') . '/organizations/' . $this->params->get('appli.helloasso.slug') . '/forms');
    }

    /**
     * Orders - Items
     * */

    /**
     * Get the detail of an item contained in an order
     *  
     * @param type $itemId
     * @param bool $withDetails Set to true to return CustomFields and Options. Default value : false
     */
    public function getItem($itemId, bool $withDetails = false): ResponseInterface {
        $this->clearQueryParams();
        $this->addQueryParams(QueryParams::WITH_DETAILS, $withDetails);
        return $this->send('GET', $this->params->get('appli.helloasso.api_base') . '/items/' . $itemId);
    }

    /**
     * Get a list of items sold by an organization
     */
    public function getItems(): ResponseInterface {

        return $this->send('GET', $this->params->get('appli.helloasso.api_base') . '/organizations/' . $this->params->get('appli.helloasso.slug') . '/items', ['query' => ['withDetails' => $withDetails]]);
    }

    /**
     * Get detailed information about a specific order
     * 
     * @param type $orderId
     * @return ResponseInterface
     */
    public function getOrder($orderId, bool $withDetails = false): ResponseInterface {
        $this->clearQueryParams();
        $this->addQueryParams(QueryParams::WITH_DETAILS, $withDetails);
        return $this->send('GET', $this->params->get('appli.helloasso.api_base') . '/orders/' . $orderId);
    }

    /**
     * Get form orders
     * 
     * @param FormType $type The form type
     * @param string $slug The form slug
     * @return ResponseInterface
     */
    public function getFormOrder(FormType $type, string $slug): ResponseInterface {
        return $this->send('GET', $this->params->get('appli.helloasso.api_base') . '/organizations/' . $this->params->get('appli.helloasso.slug') . '/forms/' . $type->value . '/' . $slug . '/orders');
    }

    /**
     *  Get a list of orders
     * 
     * @return ResponseInterface
     */
    public function getOrders(): ResponseInterface {
        return $this->send('GET', $this->params->get('appli.helloasso.api_base') . '/organizations/' . $this->params->get('appli.helloasso.slug') . '/forms/' . $type->value . '/' . $slug . '/orders');
    }

    public function cancelOrder($orderId): ResponseInterface {
        return $this->send('POST', $this->params->get('appli.helloasso.api_base') . '/orders/' . $orderId . '/cancel');
    }

    /**
     * Payments management
     * */

    /**
     * 
     * Get detailed information about a specific payment
     * 
     * @param type $paymentId
     * @return ResponseInterface
     */
    public function getPayement($paymentId): ResponseInterface {
        $this->clearQueryParams();
        return $this->send('GET', $this->params->get('appli.helloasso.api_base') . '/payments/' . $this->params->get('appli.helloasso.slug') . '/forms/' . $type->value . '/' . $slug . '/orders');
    }

    /**
     * Get information about payments made in a specific form
     * 
     * @param FormType $type
     * @param string $slug
     * @return ResponseInterface
     */
    public function getFormPayments(FormType $type, string $slug): ResponseInterface {
        return $this->send('GET', $this->params->get('appli.helloasso.api_base') . '/organizations/' . $this->params->get('appli.helloasso.slug') . '/forms/' . $type->value . '/' . $slug . '/payments');
    }

    /**
     * Get information about payments made
     * 
     * @return ResponseInterface
     */
    public function getPayments(): ResponseInterface {
        return $this->send('GET', $this->params->get('appli.helloasso.api_base') . '/organizations/' . $this->params->get('appli.helloasso.slug') . '/payments');
    }

    public function searchPayements(): ResponseInterface {
        return $this->send('GET', $this->params->get('appli.helloasso.api_base') . '/organizations/' . $this->params->get('appli.helloasso.slug') . '/payments/search');
    }

    /**
     * 
     * @param type $paymentId
     * @return ResponseInterface
     */
    public function refundPayement($paymentId): ResponseInterface {
        return $this->send('POST', $this->params->get('appli.helloasso.api_base') . '/payments/' . $paymentId . '/refund');
    }

    public function send(string $method, string $uri, array $options = []): ResponseInterface {
        if (!empty($this->queryParams)) {
            $options = array_merge_recursive($options, ['query' => $this->queryParams]);
        }
        $options = array_merge_recursive($options, ['headers' => ['Authorization' => 'Bearer ' . $this->jwtToken]]);
        return $this->client->request(strtoupper($method), $uri, $options);
    }

    public function clearQueryParams(): void {
        $this->queryParams = [];
    }

    public function addQueryParams(QueryParams $key, mixed $value): void {
        if (is_bool($value) & true === $value) {
            $value = 'true';
        } elseif (is_bool($value) & false === $value) {
            $value = 'false';
        }
        $this->queryParams[$key->value] = $value;
    }

    public function setQueryParams(array $params): void {
        $this->queryParams = $params;
    }
}
