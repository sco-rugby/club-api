<?php

namespace App\Repository\Datawarehouse\Licence;

use App\Model\SaisonInterface;
use App\Entity\Datawarehouse\Licence\LicenceSaison;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMapping;
use App\Repository\SaisonManagedRepositoryInterface;

/**
 * @extends ServiceEntityRepository<LicenceSaison>
 *
 * @method LicenceSaison|null find($id, $lockMode = null, $lockVersion = null)
 * @method LicenceSaison|null findOneBy(array $criteria, array $orderBy = null)
 * @method LicenceSaison[]    findAll()
 * @method LicenceSaison[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LicenceSaisonRepository extends ServiceEntityRepository implements SaisonManagedRepositoryInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, LicenceSaison::class);
    }

    public function save(LicenceSaison $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(LicenceSaison $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findGeomapDataBySaison(SaisonInterface $saison): array {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('licence_id', 'licence_id');
        $rsm->addScalarResult('debut', 'debut', 'boolean');
        $rsm->addScalarResult('arret', 'arret', 'boolean');
        $rsm->addScalarResult('joueur', 'joueur', 'boolean');
        $rsm->addScalarResult('educateur', 'educateur', 'boolean');
        $rsm->addScalarResult('arbitre', 'arbitre', 'boolean');
        $rsm->addScalarResult('dirigeant', 'dirigeant', 'boolean');
        $rsm->addScalarResult('section_id', 'section_id');
        $rsm->addScalarResult('parent_id', 'parent_id');
        $rsm->addScalarResult('commune_id', 'commune_id');
        $rsm->addScalarResult('nom', 'nom');
        $rsm->addScalarResult('latitude', 'latitude');
        $rsm->addScalarResult('longitude', 'longitude');
        // Requête SQL pour optimiser temps de réponse (toutes les entités n'ont pas de relations et ne doivent pas en avoir)
        $query = $this->getEntityManager()->createNativeQuery('SELECT ls.licence_id, ls.debut, ls.arret, ls.joueur, ls.educateur, ls.arbitre, ls.dirigeant, afs.section_id, s.parent_id, v.id commune_id, v.nom, v.latitude, v.longitude FROM dwh_licence_saison ls JOIN affilie_section afs ON afs.affilie_id=ls.licence_id AND afs.saison_id=ls.saison_id JOIN section s ON s.id=afs.section_id JOIN affilie a ON a.id=ls.licence_id JOIN contact c ON c.affilie_id=a.id JOIN commune v ON v.id=c.commune_id WHERE ls.saison_id = :saison', $rsm);
        $query->setParameter('saison', $saison->getId());
        return $query->getResult();
    }

    public function findSectionDataBySaison(SaisonInterface $saison): array {
        // Déterminer champs du SQL
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('licence_id', 'licence_id');
        $rsm->addScalarResult('age', 'age', 'integer');
        $rsm->addScalarResult('nombresaisons', 'nombre_saisons', 'integer');
        $rsm->addScalarResult('debut', 'debut', 'boolean');
        $rsm->addScalarResult('arret', 'arret', 'boolean');
        $rsm->addScalarResult('joueur', 'joueur', 'boolean');
        $rsm->addScalarResult('educateur', 'educateur', 'boolean');
        $rsm->addScalarResult('arbitre', 'arbitre', 'boolean');
        $rsm->addScalarResult('dirigeant', 'dirigeant', 'boolean');
        $rsm->addScalarResult('soigneur', 'soigneur', 'boolean');
        $rsm->addScalarResult('section_id', 'section_id');
        $rsm->addScalarResult('parent_id', 'parent_id');
        // Requête SQL pour optimiser temps de réponse (toutes les entités n'ont pas de relations et ne doivent pas en avoir)
        $query = $this->getEntityManager()->createNativeQuery('SELECT ls.*, afs.section_id, s.parent_id FROM dwh_licence_saison ls JOIN affilie_section afs ON afs.affilie_id=ls.licence_id AND afs.saison_id=ls.saison_id JOIN section s ON s.id=afs.section_id JOIN affilie a ON a.id=ls.licence_id WHERE ls.saison_id = :saison', $rsm);
        $query->setParameter('saison', $saison->getId());

        return $query->getResult();
    }

    public function deleteSaison(SaisonInterface $saison, bool $flush = false) {
        $nb = $this->createQueryBuilder('ls')
                ->delete()
                ->where('ls.saison = :saison')
                ->setParameter('saison', $saison->getId())
                ->getQuery()
                ->execute();
        if ($flush) {
            $this->getEntityManager()->flush();
        }
        return $nb;
    }

    public function findBySaison(SaisonInterface $saison): array {
        return $this->createQueryBuilder('ls')
                        ->where('ls.saison = :saison')
                        ->setParameter('saison', $saison->getId())
                        ->orderBy('ls.licence', 'ASC')
                        ->getQuery()
                        ->getResult();
    }
}
