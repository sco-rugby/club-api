<?php

namespace App\Repository\Contact;

use App\Entity\Contact\Commune;
use App\Entity\Contact\Adresse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Seriliazer\CommuneNormalizer;

/**
 * @extends ServiceEntityRepository<Commune>
 *
 * @method Commune|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commune|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commune[]    findAll()
 * @method Commune[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommuneRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Commune::class);
    }

    public function save(Commune $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Commune $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByAdresse(Adresse $adresse): ?Commune {
        if (null === $adresse->getVille()) {
            return null;
        }
        $adresses = $this->createQueryBuilder('c')
                ->andWhere('c.canonizedNom = :nom')
                ->setParameter('nom', $this->canonizeNom($adresse->getVille()))
                ->getQuery()
                ->getResult()
        ;
        if (empty($adresses) || count($adresses) > 1) {
            return null;
        } else {
            return $adresses[0];
        }
    }

    public function findByNom(string $nom): ?Commune {
        return $this->findOneBy(['canonizedNom' => $this->canonizeNom($nom)]);
    }

    public function findByNomEtCp(string $nom, string $cp): ?Commune {
        return $this->createQueryBuilder('c')
                        ->leftJoin('c.codePostaux', 'p')
                        ->where("c.canonizedNom LIKE :nom")
                        ->andWhere("p.codePostal = :cp")
                        ->setParameter('nom', '%' . $nom . '%')
                        ->setParameter('cp', $cp)
                        ->getQuery()
                        ->getOneOrNullResult();
    }

    private function canonizeNom(string $nom): string {
        $normalizer = new CommuneNormalizer();
        return $normalizer->normalize($nom, 'string', ['canonize' => true]);
    }
}
