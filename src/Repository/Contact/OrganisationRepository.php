<?php

namespace App\Repository\Contact;

use App\Entity\Contact\Organisation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\ImportableRepositoryInferface;
use App\Entity\Import\ImportableInterface;

/**
 * @extends ServiceEntityRepository<Organisation>
 *
 * @method Organisation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Organisation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Organisation[]    findAll()
 * @method Organisation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganisationRepository extends ServiceEntityRepository implements ImportableRepositoryInferface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Organisation::class);
    }

    public function save(Organisation $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Organisation $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByExternalId(string $externalId): ?ImportableInterface {
        return $this->findBy(['externalId' => $externalId]);
    }

    public function findByAppliMaitre(string $appliMaitre): array {
        return $this->findBy(['appliMaitre' => $appliMaitre]);
    }

    public function findByImportedDate(\DateTimeInterface $date): array {
        return $this->findBy(['importedAt' => $date]);
    }
}
