<?php

namespace App\Model\Evenement;

use ScoRugby\CoreBundle\Entity\Slug;

enum Categorie: string implements CategorieInterface {

    case REUNION = 'reunion';
    case MATCH_DOMICILE = 'domicile';
    case MATCH_DEPLACEMENT = 'deplacement';
    case ENTRAINEMENT = 'entrainement';

    public function getId(): ?string {
        return $this->name;
    }

    public function setId(string $id): self {
        return $this;
    }

    public function getLibelle(): ?string {
        return match ($this) {
            TypeEvenement::REUNION => 'Réunion',
            TypeEvenement::MATCH_DOMICILE => 'Match à domicile',
            TypeEvenement::MATCH_DEPLACEMENT => 'Déplacement',
            TypeEvenement::ENTRAINEMENT => 'Entrainement',
        };
    }

    public function setLibelle(string $libelle): self {
        return $this;
    }

    public function getSlug(): Slug {
        $slug = new Slug();
        return $slug->setString($slug->sluggify($this->value));
    }

    public function getDescription(): ?string {
        return '';
    }

    public function setDescription(?string $description): self {
        return $this;
    }
}
