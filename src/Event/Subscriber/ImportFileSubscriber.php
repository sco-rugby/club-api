<?php

namespace App\Event\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\DatawarehouseBuild;
use App\Event\ImportLicenceEvent;
use App\Model\Import\RapportOvale;
use ScoRugby\TraitementBundle\Model\Traitement;
use ScoRugby\CoreBundle\Event\ExceptionEvent;
use App\Service\Datawarehouse\BuildDatawarehouse;
use ScoRugby\CoreBundle\Exception\NoExecutionException;
use ScoRugby\TraitementBundle\Exception\FailedExecutionException;
use App\Manager\SaisonManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\EntityManagerInterface;

//TODO : Implementer transaction
final class ImportFileSubscriber implements EventSubscriberInterface {

    private Traitement $traitement;

    public function __construct(private MessageBusInterface $bus, private EntityManagerInterface $em, private SaisonManager $saisonManager, private EventDispatcherInterface $dispatcher) {
        return;
    }

    /*
      endProcess
      setSuccess */

    public static function getSubscribedEvents(): array {
        return [
            ImportLicenceEvent::INIT => ['onImportInit'],
            ImportLicenceEvent::START => ['onImportStart'],
            ImportLicenceEvent::IMPORTED => ['onImportLicence'],
            ImportLicenceEvent::SHUTDOWN => ['onImportShutdown'],
            ImportLicenceEvent::FAILURE => ['onImportError'],
        ];
    }

    private function createTraitement(ImportLicenceEvent $event): void {
        $key[] = $event->getFichier()->getType()->name;
        //$key[] = str_replace($event->getFichier()->getFichier()->getFilename(), $event->getFichier()->getExtension(), '');
        $this->traitement = new Traitement(ImportLicenceEvent::JOB, implode('-', $key), 'ClubImportFile');
    }

    public function onImportInit(ImportLicenceEvent $event): void {
        $this->createTraitement($event);
    }

    public function onImportStart(ImportLicenceEvent $event): void {
        if (!isset($this->traitement)) {
            $this->createTraitement($event);
        }
        $this->traitement->setDescription($event->getFichier()->getType()->value);
        $this->traitement->addContext('lignes', $event->getFichier()->getNbLignes());
        $this->traitement->addContext('fichier', $event->getFichier()->__toString());
    }

    public function onImportLicence(ImportLicenceEvent $event): void {
        $service = new BuildDatawarehouse($this->em, $this->saisonManager, $this->dispatcher);
        //try {
            forEach ($event->getFichier()->getSaisons() as $saison) {
                $service->init($saison);
                $service->build();
                $service->shutdown();
            }
        /*} catch (NoExecutionException | FailedExecutionException $ex) {
            
        }*/
    }

    public function onImportShutdown(ImportLicenceEvent $event): void {
        switch ($event->getFichier()->getType()) {
            case RapportOvale::OVALE2001:
                foreach ($event->getFichier()->getSaisons() as $saison) {
                    $this->bus->dispatch(new DatawarehouseBuild($saison));
                }
                break;
        }
        //TODO : Mettre fin au traitement
        /* if (!isset($this->traitement)) {
          $this->createTraitement($event);
          } */
    }

    public function onImportError(ExceptionEvent $event): void {
        
    }
}
