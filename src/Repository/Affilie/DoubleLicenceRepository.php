<?php

namespace App\Repository\Affilie;

use App\Entity\Affilie\DoubleLicence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\SaisonManagedRepositoryInterface;
use App\Model\SaisonInterface;

/**
 * @extends ServiceEntityRepository<DoubleLicence>
 *
 * @method DoubleLicence|null find($id, $lockMode = null, $lockVersion = null)
 * @method DoubleLicence|null findOneBy(array $criteria, array $orderBy = null)
 * @method DoubleLicence[]    findAll()
 * @method DoubleLicence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DoubleLicenceRepository extends ServiceEntityRepository implements SaisonManagedRepositoryInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, DoubleLicence::class);
    }

    public function save(DoubleLicence $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(DoubleLicence $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function deleteSaison(SaisonInterface $saison, bool $flush = false) {
        $nb = $this->createQueryBuilder('dl')
                ->delete()
                ->where('dl.saison = :saison')
                ->setParameter('saison', $saison->getId())
                ->getQuery()
                ->execute();
        if ($flush) {
            $this->getEntityManager()->flush();
        }
        return $nb;
    }

    public function findBySaison(SaisonInterface $saison): array {
        return $this->createQueryBuilder('dl')
                        ->where('dl.saison = :saison')
                        ->setParameter('saison', $saison->getId())
                        ->orderBy('dl.licence', 'ASC')
                        ->getQuery()
                        ->getResult();
    }
}
