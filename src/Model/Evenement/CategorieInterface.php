<?php

namespace App\Model\Evenement;

use ScoRugby\CoreBundle\Model\TaxonomieInterface;

/**
 *
 * @author Antoine BOUET
 */
interface CategorieInterface extends TaxonomieInterface {
    
}
