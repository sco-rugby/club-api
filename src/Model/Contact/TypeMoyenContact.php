<?php

namespace App\Model\Contact;

/**
 *
 * @author Antoine BOUET
 */
enum TypeMoyenContact: string {

    case domicile = "D";
    case professionel = "P";
    case mobile = "M";
    case autre = "A";
}
