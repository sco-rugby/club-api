<?php

namespace App\Service\Import;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Service\Import\ApiImportInterface;
use ScoRugby\API\Handler\ResponseHandler;

//TODO : Suppression donnée saison KO
abstract class AbstractAPIImportService extends AbstractImportService implements ApiImportInterface {

    protected function handleResponse($data, string $class): Object {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $handler = new ResponseHandler(new Serializer($normalizers, $encoders));
        return $handler->deserialize($data, $class);
    }
}
