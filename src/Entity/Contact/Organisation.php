<?php

namespace App\Entity\Contact;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\Contact\Adresse;
use App\Entity\Contact\Contact;
use App\Model\Contact\Organisation as BaseOrganisation;
use App\Entity\Import\MultiSourceImportableInterface;
use App\Entity\Import\MultiSourceImportableTrait;
use App\Entity\Import\ExternalReferenceInterface;
use App\Entity\Club\Club;
use App\Collection\CollectionOrganisation;
use App\Repository\Contact\OrganisationRepository;
use ScoRugby\CoreBundle\Entity\DateTimeBlameableInterface;
use ScoRugby\CoreBundle\Entity\DateTimeBlameable;
use ScoRugby\CoreBundle\Entity\EntityInterface;

#[ORM\Entity(repositoryClass: OrganisationRepository::class)]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string', length: 5)]
#[ORM\DiscriminatorMap(['' => Organisation::class, 'club' => Club::class])]
class Organisation extends BaseOrganisation implements EntityInterface, DateTimeBlameableInterface, MultiSourceImportableInterface {

    use MultiSourceImportableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Column(length: 100)]
    protected ?string $nom = null;

    #[ORM\Embedded(class: Adresse::class, columnPrefix: false)]
    protected Adresse $adresse;

    #[ORM\Column(length: 1)]
    protected ?string $etat = 'A';

    #[ORM\Embedded(class: DateTimeBlameable::class, columnPrefix: false)]
    protected DateTimeBlameable $datetime;

    #[ORM\ManyToMany(targetEntity: Contact::class, inversedBy: "organisations")]
    protected Collection $contacts;

    /**
     * @var Collection<int, OrganisationExterne>
     */
    #[ORM\OneToMany(targetEntity: OrganisationExterne::class, mappedBy: 'organisation')]
    private Collection $applisExternes;

    public function __construct() {
        parent::__construct();
        $this->contacts = new ArrayCollection();
        $this->datetime = new DateTimeBlameable();
        $this->ApplisExternes = new CollectionOrganisation();
        $this->applisExternes = new ArrayCollection();
    }

    public function getDateTime(): DateTimeBlameable {
        return $this->datetime;
    }

    /**
     * @return Collection<int, Contact>
     */
    public function getContacts(): Collection {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self {
        if (!$this->contacts->contains($contact)) {
            $this->contacts->add($contact);
            $contact->addOrganisation($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self {
        if ($this->contacts->removeElement($contact)) {
            $contact->removeOrganisation($this);
        }

        return $this;
    }
}
