<?php

namespace App\Command\Datawarehouse;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\Datawarehouse\BuildDatawarehouse;

#[AsCommand(
            name: 'datawarehouse:delete',
            description: 'Supprimer les données de l\'entrepôt de donnée',
    )]
class DatawarehouseDeleteCommand extends AbstractDeleteCommand {

    protected function deleteProcess(int $annee, SymfonyStyle $io): void {
        $io->info(sprintf('Supression des données l\'entrepôt de données pour la saison %s', $annee));
        try {
            $saison = $this->saisonManager->convertir($annee);
            $service = new BuildDatawarehouse($this->em, $this->saisonManager, $this->dispatcher, $this->logger, $io->createProgressBar());
            $service->purge($saison);
            $io->success('Données datawarehouse supprimées');
        } catch (Exception $ex) {
            throw $ex;
        } finally {
            $service->shutdown();
        }
    }
}
