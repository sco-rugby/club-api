<?php

namespace App\Model\Competition;

enum Type: string {

    case Aller = 'A';
    case Retour = 'R';
    case Libre = '!l';

}
