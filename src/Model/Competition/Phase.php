<?php

namespace App\Model\Competition;

enum Phase: string {

    case Qualification = 'Q';
    case Brassage = 'B';
    case Finale = 'F';

}
