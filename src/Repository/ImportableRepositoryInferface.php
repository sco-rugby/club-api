<?php

namespace App\Repository;

use App\Entity\Import\ImportableInterface;

/**
 *
 * @author Antoine BOUET
 */
interface ImportableRepositoryInferface {

    public function findByExternalId(string $externalId): ?ImportableInterface;

    public function findByAppliMaitre(string $appliMaitre): array;

    public function findByImportedDate(\DateTimeInterface $date): array;
}
