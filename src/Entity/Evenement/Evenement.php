<?php

namespace App\Entity\Evenement;

use App\Model\Evenement\Evenement as BaseEvenement;
use App\Repository\Evenement\EvenementRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\Import\MultiSourceImportableInterface;
use App\Entity\Import\MultiSourceImportableTrait;
use App\Collection\ParticipantCollection;
use ScoRugby\CoreBundle\Entity\SluggifyInterface;
use ScoRugby\CoreBundle\Entity\Slug;

#[ORM\Entity(repositoryClass: EvenementRepository::class)]
#[ORM\UniqueConstraint(name: "unq_evenement_slug", columns: ["slug"])]
class Evenement extends BaseEvenement implements MultiSourceImportableInterface, SluggifyInterface {

    use MultiSourceImportableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'evenements')]
    private ?Type $type = null;

    #[ORM\Embedded(class: Slug::class, columnPrefix: false)]
    protected Slug $slug;

    #[ORM\OneToMany(targetEntity: Participant::class, mappedBy: 'evenement',)]
    protected Collection $participants;

    #[ORM\OneToMany(targetEntity: EvenementExterne::class, mappedBy: 'evenement')]
    protected Collection $applisExternes;

    /**
     * @var Collection<int, Calendrier>
     */
    #[ORM\ManyToMany(targetEntity: Calendrier::class, inversedBy: 'evenements')]
    private Collection $calendriers;

    public function __construct() {
        parent::__construct();
        $this->applisExternes = new ArrayCollection();
        $this->participants = new ParticipantCollection();
        $this->slug = new Slug();
        $this->calendriers = new ArrayCollection();
    }

    public function getSlug(): Slug {
        return $this->slug;
    }

    /**
     * @return Collection<int, Participant>
     */
    public function getParticipants(): Collection {
        return $this->participants;
    }

    public function addParticipant(Participant $participant): static {
        if (!$this->participants->contains($participant)) {
            $this->participants->add($participant);
            $participant->setEvenement($this);
        }

        return $this;
    }

    public function removeParticipant(Participant $participant): static {
        if ($this->participants->removeElement($participant)) {
            // set the owning side to null (unless already changed)
            if ($participant->getEvenement() === $this) {
                $participant->getEvenement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Calendrier>
     */
    public function getCalendriers(): Collection {
        return $this->calendriers;
    }

    public function addCalendrier(Calendrier $calendrier): static {
        if (!$this->calendriers->contains($calendrier)) {
            $this->calendriers->add($calendrier);
        }

        return $this;
    }

    public function removeCalendrier(Calendrier $calendrier): static {
        $this->calendriers->removeElement($calendrier);

        return $this;
    }
}
