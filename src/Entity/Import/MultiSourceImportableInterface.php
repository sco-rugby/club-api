<?php

namespace App\Entity\Import;

use Doctrine\Common\Collections\Collection;

/**
 *
 * @author Antoine BOUET
 */
interface MultiSourceImportableInterface extends ImportableInterface {

    public function getApplisExternes(): Collection;

    public function setApplisExternes(Collection $applisExternes): self;

    public function addApplisExterne(ExternalReferenceInterface $applisExterne): self;

    public function removeApplisExterne(ExternalReferenceInterface $applisExterne): self;
}
