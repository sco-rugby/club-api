<?php

namespace App\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Club\FonctionContact;
use App\Entity\Saison;
use App\Entity\Club\Section;

class CollectionFonction extends ArrayCollection {

    public function club(Saison $saison = null): ArrayCollection {
        return $this->filter(function (FonctionContact $fonction, $saison = null) {
                    if (is_null($fonction->getSection()) | $fonction->getSaison() == $saison) {
                        return $fonction;
                    }
                });
    }

    public function bySection(Section $section, Saison $saison = null): ArrayCollection {
        return $this->filter(function (FonctionContact $fonction, $section, $saison = null) {
                    if ($fonction->getSection()->getId() == $section->getId()) {
                        if (is_null($saison) | $fonction->getSaison() == $saison) {
                            return $fonction;
                        }
                    }
                });
    }
}
