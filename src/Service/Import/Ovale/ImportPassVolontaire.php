<?php

namespace App\Service\Import\Ovale;

use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Club\Fonction;
use App\Entity\Club\FonctionContact;
use App\Model\Import\RapportOvale;
use App\Event\ImportFichierEvent;
use ScoRugby\CoreBundle\Exception\Execution\FailedExecutionException;

//TODO : activer transaction
final class ImportPassVolontaire extends AbstractOvaleService {

    const JOB = 'passvolontaire:import';

    private Fonction $fonction;

    const BENEVOLE = 'BENE';

    ############
    # Execution du service
    ############

    public function init(): void {
        parent::init();
        //
        $this->creations['benevole'] = 0;
        $this->fonction = $this->em->getRepository(Fonction::class)->find(self::BENEVOLE);
        $this->benevoles = new ArrayCollection();
        //
        try {
            //
            foreach ($this->em->getRepository(FonctionContact::class)->findBy(['fonction' => self::BENEVOLE]) as $benevole) {
                $this->setBenevoleList($benevole);
            }
        } catch (\Exception $e) {
            $this->setError('init PassVolontaire : ' . $e->getMessage(), get_class($e));
            throw new FailedExecutionException($e->getMessage());
        }
    }

    public function import(mixed $data): void {
        $key[] = (RapportOvale::OVALE2050)->name;
        $key[] = str_replace($data->getFilename, $data->getExtension, '');
        //$this->getTraitement()->initProcess('ImportLicence', implode('-', $key), self::JOB);
        //$this->getTraitement()->setDescription((RapportOvale::OVALE2050)->value);
        parent::import($data);
        $worksheet = $this->spreadsheet->getActiveSheet();
        if ($this->hasLogger()) {
            $this->logger->debug('Ouvrir ' . $worksheet->getTitle());
        }
        $rows = $this->readSpreadsheet(RapportOvale::OVALE2050, $worksheet);
        //
        $result = true;
        try {
            $this->startProcess(count($rows));
            $this->startCommand(count($rows));
            foreach ($rows as $no => $data) {
                $saison = $this->managers['saison']->convertir($data['saison']);
                try {
                    if ($this->hasLogger()) {
                        $this->logger->info(sprintf('traitement %s %s pour %s', $data['nom'], $data['prenom'], $saison));
                    }
                    if (!$this->isBenevoleCreated($data['nom'], $data['prenom'], $saison)) {
                        $benevole = $this->createBenevole($data);
                    }
                } catch (\Exception $ex) {
                    $this->setFailure($ex->getMessage() . ';' . implode(';', $data));
                    $result = false;
                }
                //
                $this->lecture++;
                $this->nextStep();
            }
            if ($this->creations['contact'] > 0) {
                $this->addCompteRendu($this->creations['contact'] . ' contacts créés');
            }
            if ($this->creations['benevole'] > 0) {
                $this->addCompteRendu($this->creations['benevole'] . ' bénévoles créés');
            }
            if (false === $result) {
                throw new FailedExecutionException('Des erreurs sont survenues lors de l\'import.');
            }
            if ($this->hasCompteRendu()) {
                $this->addCompteRendu(sprintf('%s lignes lues : Aucune modification', $this->lecture));
                $this->addCompteRendu(sprintf('Fichier %s ignoré', $this->fichier->getFilename()));
            } else {
                $msg[] = sprintf('%s lignes lues', $this->lecture);
                $msg = array_merge($msg, $this->getCompteRendu());
                $msg[] = sprintf('Fichier %s importé', $this->fichier->getFilename());
                $this->setCompteRendu($msg);
                try {
                    $event = new ImportFichierEvent(RapportOvale::OVALE2050, $this->fichier);
                    $this->dispatcher->dispatch($event, ImportFichierEvent::BENEVOLE);
                } catch (\Exception $ex) {
                    return;
                }
            }
            $this->setSuccess($this->getCompteRendu());
        } catch (FailedExecutionException $e) {
            $this->setFailure('import : ' . $e->getMessage(), get_class($e));
            throw new FailedExecutionException($e->getMessage());
        } catch (\Exception $e) {
            $this->setError('import : ' . $e->getMessage(), get_class($e));
            throw new FailedExecutionException($e->getMessage());
        }
    }

    ############
    # Méthodes spécifiques
    ############

    private function isBenevoleCreated(string $nom, string $prenom, int $saison): bool {
        if (!$this->isContactCreated($nom, $prenom, null)) {
            return false;
        }
        $contactId = $this->findContact($nom, $prenom, null)->getId();
        return null !== $this->findBenevole($contactId, $saison);
    }

    private function findBenevole(int $contactId, int $saison): ?FonctionContact {
        return $this->benevoles->get($this->convertirBenevoleKey($contactId, $saison));
    }

    private function createBenevole(array $data): FonctionContact {
        if ($this->hasLogger()) {
            $this->logger->debug('Création bénévole');
        }
        if (false !== $this->isContactCreated($data['nom'], $data['prenom'], $data['code_postal'])) {
            $found = $this->findContact($data['nom'], $data['prenom'], $data['code_postal']);
            $contact = $this->managers['contact']->get($found->getId());
        } else {
            $contact = $this->createContact($data);
        }
        $benevole = (new FonctionContact())
                ->setSaison($this->findSaison($data['saison']))
                ->setContact($contact)
                ->setFonction($this->fonction)
                ->setNote($data['fonctions']);
        //
        $this->em->persist($benevole);
        $this->creations['benevole']++;
        // Ajout à liste
        $this->setBenevoleList($benevole);
        //
        return $benevole;
    }

    private function setBenevoleList(FonctionContact $benevole) {
        $this->benevoles->set($this->convertirBenevoleKey($benevole->getContact()->getId(), $benevole->getSaison()->getId()), $benevole);
    }

    private function convertirBenevoleKey(int $contactId, int $saison) {
        return sprintf("%s/%s", $contactId, $saison);
    }
}
