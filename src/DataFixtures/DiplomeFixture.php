<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use App\Entity\Affilie\Diplome;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\String\UnicodeString;

/**
 * @author Antoine BOUET
 */
final class DiplomeFixture extends AbstractDataFixture implements FixtureGroupInterface {

    public function load(ObjectManager $manager) {
        foreach ($this->loadDataFile() as $data) {
            $diplome = new Diplome();
            $diplome->setId($data['id']);
            $diplome->setLibelle($data['libelle']);
            $slug = (new AsciiSlugger())->slug($data['libelle']);
            $libelle = (new UnicodeString($slug))
                    ->folded()
                    ->upper();
            $diplome->setCanonizedLibelle($libelle);
            if (!empty($data['slug'])) {
                $diplome->getSlug()->setString($data['slug']);
            } else {
                $diplome->getSlug()->setString($data['libelle']);
            }
            if (null != $data['description']) {
                $diplome->setDescription($data['description']);
            }
            $manager->persist($diplome);
        }
        $manager->flush();
    }

    public static function getGroups(): array {
        return ['affilie'];
    }
}
