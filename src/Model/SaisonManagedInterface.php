<?php

namespace App\Model;

use App\Model\SaisonInterface;

interface SaisonManagedInterface {

    public function getSaison(): ?SaisonInterface;

    public function setSaison(SaisonInterface $saison);
}
