<?php

namespace App\Command\Datawarehouse;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\Datawarehouse\BuildEffectif;

#[AsCommand(
            name: 'datawarehouse:delete:effectif',
            description: 'Supprimer les données "effectifs" de l\'entrepôt de données',
    )]
class EffectifDeleteCommand extends AbstractDeleteCommand {

    protected function deleteProcess(int $annee, SymfonyStyle $io): void {
        $io->info(sprintf('Supression des données "effectifs" pour la saison %s', $annee));
        $saison = $this->saisonManager->convertir($annee);
        $service = new BuildEffectif($this->em, $this->saisonManager, $this->dispatcher, $this->logger, $io->createProgressBar());
        $nb = $service->purge($saison);
        $service->shutdown();
        $io->success(sprintf('%s données "effectif" supprimées', $nb));
    }
}
