<?php

namespace App\Manager\Helloasso;

use ScoRugby\API\Manager\TokenManagerInterface;

class TokenManager implements TokenManagerInterface {

    private ?string $accessToken;
    private ?string $refreshToken;
    private ?string $jwtToken;
    private array $queryParams = [];
    private ?int $expires = null; //The lifetime in seconds of the access token

    public function __construct(private readonly HttpClientInterface $httpClient, private readonly ResponseHandler $responseHandler, private readonly string $clientId, private readonly string $clientSecret) {
        return;
    }

    public function getAccessToken(): string {
        if ($this->params->has('appli.helloasso.refresh_time')) {
            $refreshDate = new \DateTimeImmutable($this->params->get('appli.helloasso.refresh_time'));
        } else {
            $refreshDate = new \DateTimeImmutable();
        }
        $interval = $refreshDate->diff(new \DateTimeImmutable());
        if (0 == $interval->format('%a')) {
            $request = $this->client->request(
                    'POST',
                    $this->params->get('appli.helloasso.oauth_api') . '/token',
                    [
                        'headers' => [
                            'Content-Type' => 'application/x-www-form-urlencoded',
                        ],
                        'body' => [
                            'client_id' => $this->clientId,
                            'client_secret' => $this->clientSecret,
                            'grant_type ' => 'client_credentials'
                        ],
                    ]
            );
            $response = $request->toArray();
            $this->accessToken = $response['access_token'];
            $this->refreshToken = $response['refresh_token'];
            $this->expires = $response['expires_in'];
            //store token + expiry date
            $params = Yaml::parseFile(self::FILE);
            $now = new \DateTimeImmutable();
            $duration = new \DateInterval(sprintf('P%sS', $this->expires));
            $params['appli']['helloasso']['refresh_token'] = $this->refreshToken;
            $params['appli']['helloasso']['refresh_time'] = $now->add($duration)->format('c');
            $yaml = Yaml::dump($params);
            file_put_contents(self::FILE, $yaml);
        } else {
            $this->refreshToken = $this->params->has('appli.helloasso.refresh_token');
        }
        $this->refresh();
    }

    public function refresh(): string {
        $request = $this->client->request(
                'POST',
                $this->params->get('appli.helloasso.oauth_api') . '/token',
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                    'body' => [
                        'client_id' => $this->params->get('appli.helloasso.client_id'),
                        'grant_type ' => 'refresh_token',
                        'refresh_token' => $this->refreshToken,
                    ],
                ]
        );
        $response = $request->toArray();
        $this->jwtToken = $response['access_token'];
        $this->expires = $response['expires_in'];
    }

    /**
     * @inheritDoc
     */
    public function disconnect(): void {
        $request = $this->client->request(
                'GET',
                $this->params->get('appli.helloasso.oauth_api') . '/disconnect',
                [
                    'headers' => [
                        'Authorization' => $this->getAccessToken(),
                    ]
                ]
        );
    }
}
