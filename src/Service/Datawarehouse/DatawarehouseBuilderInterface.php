<?php

namespace App\Service\Datawarehouse;

use App\Model\SaisonInterface;

interface DatawarehouseBuilderInterface {

    public function init(SaisonInterface $saison): void;

    public function purge(SaisonInterface $saison): int;

    public function build(): void;

    public function shutdown(): void;
}
