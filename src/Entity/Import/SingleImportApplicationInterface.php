<?php

namespace App\Entity\Import;

/**
 *
 * @author Antoine BOUET
 */
interface SingleSourceImportableInterface extends ImportableInterface, ExternalReferenceInterface {
    
}
