<?php

namespace App\Manager;

use App\Model\AppliExterne;

/**
 *
 * @author Antoine BOUET
 */
interface MultiSourceManagerInterface {

    public function isModificationAllowed(AppliExterne $appli): bool;
}
