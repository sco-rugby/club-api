<?php

namespace App\Seriliazer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\String\UnicodeString;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * Description of ComuneNormalizer
 *
 * @author Antoine BOUET
 */
class CommuneNormalizer implements NormalizerInterface {

    public function normalize(mixed $object, ?string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null {
        $str = (string) $object;
        $str = (string) (new UnicodeString($str))
                        ->trim()
                        ->title(true)
                        ->replace('S/', 'sur ')
                        ->replace('Sur ', 'sur ')
                        ->replace(' Le ', ' le ')
                        ->replace(' La ', ' la ')
                        ->replace(' Les ', ' les ')
                        ->replace("L'", "l'")
                        ->replace("D'", "d'")
                        ->replace(' De ', ' de ')
                        ->replace(' En ', ' en ')
                        ->replaceMatches('/(St[\s|\-|\.]\s*)|(Saint[\s|\-|\.]\s*)/', 'Saint-')
                        ->replaceMatches('/(Ste[\s|\-|\.]\s*)|(Sainte[\s|\-|\.]\s*)/', 'Sainte-')
                        ->toString();
        if (key_exists('canonize', $context) && $context['canonize'] == true) {
            $str = $this->canonize($str);
        }
        return $str;
    }

    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool {
        return is_string($data) || $data instanceof \Stringable;
    }

    public function getSupportedTypes(?string $format): array {
        return [
            'string' => true,
            \Stringable::class => true
        ];
    }

    private function canonize(string $nom): string {
        $str = (new AsciiSlugger())->slug($nom, ' ');
        return (string) (new UnicodeString($str))
                        ->folded()
                        ->upper()
                        ->replaceMatches('/SAINT./', 'ST ')
                        ->replaceMatches('/SAINTE./', 'STE ')
                        ->toString();
    }
}
