<?php

namespace App\Repository\Affilie;

use App\Entity\Affilie\Adhesion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\SaisonManagedRepositoryInterface;
use App\Model\SaisonInterface;
use App\Repository\ImportableRepositoryInferface;
use App\Entity\Import\ImportableInterface;

class AdhesionRepository extends ServiceEntityRepository implements SaisonManagedRepositoryInterface, ImportableRepositoryInferface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Adhesion::class);
    }

    public function save(Paiement $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Paiement $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findBySaison(SaisonInterface $saison): array {
        return $this->findBy(['saison' => $saison->getId()]);
    }

    public function deleteSaison(SaisonInterface $saison, bool $flush = false) {
        $nb = $this->createQueryBuilder('a')
                ->delete()
                ->where('a.saison = :saison')
                ->setParameter('saison', $saison->getId())
                ->getQuery()
                ->execute();
        if ($flush) {
            $this->getEntityManager()->flush();
        }
        return $nb;
    }

    public function findByExternalId(string $externalId): ?ImportableInterface {
        return $this->findBy(['externalId' => $externalId]);
    }

    public function findByAppliMaitre(string $appliMaitre): array {
        return $this->findBy(['appliMaitre' => $appliMaitre]);
    }

    public function findByImportedDate(\DateTimeInterface $date): array {
        return $this->findBy(['importedAt' => $date]);
    }
}
