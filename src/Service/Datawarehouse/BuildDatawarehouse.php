<?php

namespace App\Service\Datawarehouse;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Psr\Log\LoggerInterface;
use App\Manager\SaisonManager;
use App\Model\SaisonInterface;

final class BuildDatawarehouse extends AbstractBuildService implements DatawarehouseBuilderInterface {

    public const JOB = 'datawarehouse:build';

    private $services = [];

    public function __construct(protected EntityManagerInterface $em, protected SaisonManager $saisonManager, protected EventDispatcherInterface $dispatcher, protected ?LoggerInterface $logger = null, protected ?ProgressBar $progressBar = null) {
        $this->em->getConnection()->setAutoCommit(false);
        parent::__construct($this->em, $this->saisonManager, $dispatcher, $this->logger, $this->progressBar);
        $this->services['BuildLicence'] = new BuildLicence($this->em, $this->saisonManager, $this->dispatcher, $this->logger, $this->progressBar);
        $this->services['BuildEffectif'] = new BuildEffectif($this->em, $this->saisonManager, $this->dispatcher, $this->logger, $this->progressBar);
        $this->services['BuildGeomap'] = new BuildGeomap($this->em, $this->saisonManager, $this->dispatcher, $this->logger, $this->progressBar);
    }

    public function init(SaisonInterface $saison): void {
        $this->initTransaction();
        $this->setChained();
        foreach ($this->services as $nom => $service) {
            if ($this->hasLogger()) {
                $this->getLogger()->debug(sprintf('==== init %s ===', $nom));
            }
            $service->setChained();
            $service->init($saison);
        }
    }

    public function purge(SaisonInterface $saison): int {
        $this->initTransaction();
        $this->setChained();
        $total = 0;
        foreach ($this->services as $nom => $service) {
            if ($this->hasLogger()) {
                $this->getLogger()->debug(sprintf('==== %s ===', $nom));
            }
            $service->setChained();
            $nb = $service->purge($saison);
            $total += $nb;
        }
        return $total;
    }

    public function build(): void {
        foreach ($this->services as $nom => $service) {
            if ($this->hasLogger()) {
                $this->getLogger()->debug(sprintf('==== %s ===', $nom));
            }
            $service->build();
        }
    }

    public function shutdown(): void {
        foreach ($this->services as $service) {
            $service->shutdown();
            $service->endChained();
        }
        $this->endChained();
        $this->shutDownTransaction();
    }
}
