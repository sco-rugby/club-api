<?php

namespace App\Manager\Helloasso;

use ScoRugby\API\Manager\AbstractAPIManager;

final class APIManager extends AbstractAPIManager {

    private string $slug;

    public function __construct(private APICallerInterface $caller, private ContainerBagInterface $params) {
        parent::__construct($caller, $params);
        $this->baseUrl = $this->params->get('appli.helloasso.api_base');
        $this->slug = $this->params->get('appli.helloasso.slug');
    }
   public function getClassName(): string {
        return '';
    }
    public function getResourceName(): string {
        return $this->slug;
    }
}
