<?php

namespace App\Model;

/**
 *
 * @author Antoine BOUET
 */
enum MoyenPaiement: string {
    case Cash = 'Cash';
    case Check = 'Check';
    case BankTransfer = 'BankTransfer';
}
