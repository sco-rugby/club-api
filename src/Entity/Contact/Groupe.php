<?php

namespace App\Entity\Contact;

use App\Entity\Club\Fonction;
use App\Repository\GroupeRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use ScoRugby\CoreBundle\Model\GroupeInterface;
use App\Entity\Club\Equipe;
use App\Entity\Club\Section;
use ScoRugby\CoreBundle\Entity\AbstractTaxonomie;

#[ORM\Entity(repositoryClass: GroupeRepository::class)]
#[ORM\UniqueConstraint(name: "unq_groupe_slug", columns: ["slug"])]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string', length: 10)]
#[ORM\DiscriminatorMap(['' => Groupe::class, 'equipe' => Equipe::class, 'section' => Section::Class])]
class Groupe extends AbstractTaxonomie {

    #[ORM\OneToMany(targetEntity: GroupeContact::class, mappedBy: "groupe", orphanRemoval: "true")]
    protected Collection $contacts;

    #[ORM\Column(length: 100, nullable: true)]
    protected ?string $mailingList = null;

    #[ORM\ManyToOne(inversedBy: 'groupes')]
    protected ?Fonction $fonction = null;

    public function getEmailList(): ?string {
        return $this->mailingList;
    }

    public function setEmailList(?string $mailingList): self {
        $this->mailingList = $mailingList;

        return $this;
    }

    public function getFonction(): ?Fonction {
        return $this->fonction;
    }

    public function setFonction(?Fonction $fonction): static {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * @return Collection<int, GroupeContact>
     */
    public function getContacts(): Collection {
        return $this->contacts;
    }

    public function addContact(GroupeContact $contact): self {
        if (!$this->contacts->contains($contact)) {
            $this->contacts->add($contact);
            $contact->setGroupe($this);
        }

        return $this;
    }

    public function removeContact(GroupeContact $contact): self {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getGroupe() === $this) {
                $contact->setGroupe(null);
            }
        }

        return $this;
    }
}
