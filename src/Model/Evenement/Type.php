<?php

namespace App\Model\Evenement;

use ScoRugby\CoreBundle\Model\TaxonomyTrait;
use ScoRugby\CoreBundle\Model\TaxonomieInterface;
use ScoRugby\CoreBundle\Entity\SluggifyInterface;

class Type implements TypeEvenementInterface, TaxonomieInterface, SluggifyInterface {

    use TaxonomyTrait;
}
