<?php

namespace App\Entity;

use App\Entity\Affilie\Adhesion;
use App\Entity\Contact\Contact;
use App\Repository\PaiementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Import\ImportableInterface;
use App\Entity\Import\ImportApplicationInterface;
use App\Entity\Import\SingleSourceImport;
use App\Model\MoyenPaiement;

#[ORM\Entity(repositoryClass: PaiementRepository::class)]
class Paiement implements ImportableInterface {
    /* private OrderLight $order;
      private array $items;

      private string $paymentReceiptUrl;
      private string $fiscalReceiptUrl;
      private int $amount;
      private int $amountTip;

      private \DateTime $cashOutDate;
      private \DateTime $date;

      private PaymentCashOutState $cashOutState;
      private PaymentMeans $paymentMeans;
      private PaymentState $state;
      private PaymentType $type;
      private PaymentOffLineMeansModel $paymentOffLineMean; */

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    protected ?string $externalId = null;

    #[ORM\Column(length: 1)]
    private ?string $moyen = null;

    #[ORM\Column]
    private ?int $montant = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'installments')]
    private ?self $parent = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(length: 15)]
    private ?string $etat = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $recu = null;

    #[ORM\ManyToOne(inversedBy: 'paiements')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Contact $payeur = null;

    #[ORM\Embedded(class: SingleSourceImport::class)]
    private SingleSourceImport $appli;

    /**
     * @var Collection<int, self>
     */
    #[ORM\OneToMany(targetEntity: self::class, mappedBy: 'parent')]
    private Collection $installments;

    #[ORM\ManyToOne(inversedBy: 'paiements')]
    private ?Adhesion $adhesion = null;

    public function __construct() {
        $this->installments = new ArrayCollection();
        $this->appli = new SingleSourceImport();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getMoyen(): ?MoyenPaiement {
        return $this->moyen;
    }

    public function setMoyen(MoyenPaiement $moyen): static {
        $this->moyen = $moyen;

        return $this;
    }

    public function getMontant(): ?int {
        return $this->montant;
    }

    public function setMontant(int $montant): static {
        $this->montant = $montant;

        return $this;
    }

    public function getParent(): ?self {
        return $this->parent;
    }

    public function setParent(?self $parent): static {
        $this->parent = $parent;

        return $this;
    }

    public function getAppliMaitre(): ?ImportApplicationInterface {
        return $this->appli;
    }

    public function setAppliMaitre(ImportApplicationInterface $appli): self {
        $this->appli = $appli;
        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getInstallments(): Collection {
        return $this->installments;
    }

    public function addInstallment(self $installment): static {
        if (!$this->installments->contains($installment)) {
            $this->installments->add($installment);
            $installment->setParent($this);
        }

        return $this;
    }

    public function removeInstallment(self $installment): static {
        if ($this->installments->removeElement($installment)) {
            // set the owning side to null (unless already changed)
            if ($installment->getInstallment() === $this) {
                $installment->setParent(null);
            }
        }

        return $this;
    }

    public function getDate(): ?\DateTimeInterface {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): static {
        $this->date = $date;

        return $this;
    }

    public function getEtat(): ?string {
        return $this->etat;
    }

    public function setEtat(string $etat): static {
        $this->etat = $etat;

        return $this;
    }

    public function getRecu(): ?string {
        return $this->recu;
    }

    public function setRecu(?string $recu): static {
        $this->recu = $recu;

        return $this;
    }

    public function getPayeur(): ?Contact {
        return $this->payeur;
    }

    public function setPayeur(?Contact $payeur): static {
        $this->payeur = $payeur;

        return $this;
    }

    public function getAdhesion(): ?Adhesion {
        return $this->adhesion;
    }

    public function setAdhesion(?Adhesion $adhesion): static {
        $this->adhesion = $adhesion;

        return $this;
    }
}
