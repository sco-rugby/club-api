<?php

namespace App\Entity\Contact;

use Doctrine\ORM\Mapping as ORM;
use ScoRugby\CoreBundle\Entity\EntityInterface;
use App\Entity\Import\ExternalReference;

#[ORM\Entity]
#[ORM\Table(name: "organisation_externe")]
final class OrganisationExterne extends ExternalReference implements EntityInterface {

    #[ORM\ManyToOne(inversedBy: 'applisExternes', cascade: ['all'])]
    private ?Organisation $organisation = null;

    public function getOrganisation(): ?Organisation {
        return $this->organisation;
    }

    public function setOrganisation(?Organisation $organisation): static {
        $this->organisation = $organisation;

        return $this;
    }
}
