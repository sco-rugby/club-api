<?php

namespace App\Entity\Evenement;

use App\Repository\Evenement\EvenemenExterneRepository;
use Doctrine\ORM\Mapping as ORM;
USE ScoRugby\CoreBundle\Entity\EntityInterface;
use App\Entity\Import\ExternalReference;

#[ORM\Entity(repositoryClass: EvenemenExterneRepository::class)]
#[ORM\Table(name: "evenement_externe")]
class EvenementExterne extends ExternalReference implements EntityInterface {

    #[ORM\ManyToOne(inversedBy: 'applisExternes', cascade: ['all'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Evenement $evenement = null;

    public function getEvenement(): ?Evenement {
        return $this->evenement;
    }

    public function setEvenement(?Evenement $evenement): self {
        $this->evenement = $evenement;

        return $this;
    }
}
