<?php

namespace App\Repository\Datawarehouse\Effectif;

use App\Model\SaisonInterface;
use App\Entity\Datawarehouse\Effectif\DureeGenre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\SaisonManagedRepositoryInterface;

/**
 * @extends ServiceEntityRepository<Duree>
 *
 * @method Duree|null find($id, $lockMode = null, $lockVersion = null)
 * @method Duree|null findOneBy(array $criteria, array $orderBy = null)
 * @method Duree[]    findAll()
 * @method Duree[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DureeGenreRepository extends ServiceEntityRepository implements SaisonManagedRepositoryInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, DureeGenre::class);
    }

    public function save(Duree $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Duree $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function deleteSaison(SaisonInterface $saison, bool $flush = false) {
        $nb = $this->createQueryBuilder('d')
                ->delete()
                ->where('d.saison = :saison')
                ->setParameter('saison', $saison->getId())
                ->getQuery()
                ->execute();
        if ($flush) {
            $this->getEntityManager()->flush();
        }
        return $nb;
    }

    public function findBySaison(SaisonInterface $saison): array {
        return $this->createQueryBuilder('l')
                        ->where('d.saison = :saison')
                        ->setParameter('saison', $saison->getId())
                        ->orderBy('d.licence', 'ASC')
                        ->getQuery()
                        ->getResult();
    }
}
