<?php

namespace App\Entity\Datawarehouse\Effectif;

use App\Repository\Datawarehouse\Effectif\DureeGenreRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Club\Section;
use App\Entity\Saison;

#[ORM\Entity(repositoryClass: DureeGenreRepository::class)]
#[ORM\Table(name: 'dwh_duree_genre')]
#[ORM\UniqueConstraint(name: 'unq_duree_genre', columns: ["saison_id", "section_id", "genre", "duree"])]
class DureeGenre {

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    protected ?Saison $saison = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Section $section = null;

    #[ORM\Column]
    private ?string $genre = null;

    #[ORM\Column]
    protected ?int $duree = null;

    #[ORM\Embedded(class: StatsPrcGenre::class)]
    protected StatsPourcentage $encours;

    #[ORM\Embedded(class: StatsPrcGenre::class)]
    protected StatsPourcentage $arret;

    public function __construct() {
        $this->encours = new StatsPrcGenre();
        $this->arret = new StatsPrcGenre();
    }

    public function getGenre(): ?string {
        return $this->genre;
    }

    public function setGenre(string $genre): self {
        $this->genre = $genre;
        return $this;
    }

    public function getSaison(): ?Saison {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self {
        $this->saison = $saison;

        return $this;
    }

    public function getSection(): ?Section {
        return $this->section;
    }

    public function setSection(?Section $section): self {
        $this->section = $section;

        return $this;
    }

    public function getDuree(): ?int {
        return $this->duree;
    }

    public function setDuree(int $duree): self {
        $this->duree = $duree;

        return $this;
    }

    public function getArret(): StatsPourcentage {
        return $this->arret;
    }

    public function getEnCours(): StatsPourcentage {
        return $this->encours;
    }
}
