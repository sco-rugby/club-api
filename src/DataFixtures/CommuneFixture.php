<?php

namespace App\DataFixtures;

use App\Entity\Contact\Commune;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use ScoRugby\TraitementBundle\Exception\FailedExecutionException;
use ScoRugby\TraitementBundle\Exception\NoExecutionException;

/**
 * Description of CommuneFixture
 *
 * @author Antoine BOUET
 */
class CommuneFixture extends AbstractDataFixture implements FixtureGroupInterface {

    private array $headers = [];

    public function load(ObjectManager $manager) {
        try {
            $fichier = $this->guessDataFile();
            if (!$fichier->isReadable()) {
                throw new NoExecutionException(sprintf('Le fichier %s ne peut pas être lu', $fichier));
            }
            $communes = [];
            $fp = @fopen($fichier, "r");
            $this->setHeaders($fp);
            while (($line = fgets($fp, 4096)) !== false) {
                $data = $this->normalize($line);
                $commune = new Commune();
                $commune->setId($data['id']);
                $commune->setType($data['type']);
                $commune->setCanonizedNom($data['canonized_nom']);
                $commune->setNom($data['nom']);
                $commune->setLatitude(floatval($data['latitude']));
                $commune->setLongitude(floatval($data['longitude']));
                $commune->setCodeINSEE($data['code_insee']);
                if ('true' == strtolower($data['rgrpmt'])) {
                    $commune->setRegroupement(true);
                } else {
                    $commune->setRegroupement(false);
                }
                if (null != $data['parent_id']) {
                    $parent = $manager->getRepository(Commune::class)->find($data['parent_id']);
                    $commune->setParent($parent);
                }
                $manager->persist($commune);
                $manager->flush();
                $manager->clear();
            }
            if (!feof($fp)) {
                throw new NoExecutionException("Error: unexpected fgets() fail");
            }
            fclose($fp);
        } catch (\Exception $e) {
            throw new FailedExecutionException($e->getMessage());
        }
    }

    private function setHeaders($fp): void {
        $line = fgets($fp, 4096);
        $line = str_replace("\n", '', $line);
        $this->headers = explode(',', $line);
    }

    private function normalize(string $line): array {
        $line = str_replace(array('"', "\n"), '', $line);
        return array_combine($this->headers, explode(',', $line));
    }

    public static function getGroups(): array {
        return ['adresse', 'contact'];
    }
}
