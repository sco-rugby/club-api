<?php

namespace App\Command\Datawarehouse;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\Datawarehouse\BuildDatawarehouse;
use ScoRugby\CoreBundle\Exception\NoExecutionException;

#[AsCommand(
            name: BuildDatawarehouse::JOB,
            description: 'Construit l\'entrepôt de données',
    )]
class DatawarehouseBuildCommand extends AbstractBuildCommand {

    protected function buildProcess(int $annee, SymfonyStyle $io): void {
        $saison = $this->saisonManager->get($annee);

        $io->info(sprintf('Construction de l\'entrepôt de données pour la saison %s', $annee));
        $service = new BuildDatawarehouse($this->em, $this->saisonManager, $this->dispatcher, $this->logger, $io->createProgressBar());
        try {
            $io->block('Initialisation du traitement');
            $service->init($saison);

            $io->block(" ");
            $io->block(sprintf('Construction des données %s', $annee));
            try {
                $service->build();
            } catch (NoExecutionException $ex) {
                $io->warning($ex->getMessage());
            }
            $io->block(" ");
            $io->success(sprintf('La construction des données %s sont calculées', $annee));
        } catch (Exception $ex) {
            throw $ex;
        } finally {
            $service->shutdown();
        }
    }
}
