<?php

namespace App\Service\Import;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileImportInterface extends ImportServiceInterface {

    public function load(UploadedFile $fichier): void;

    public function isLoaded(): bool;
}
