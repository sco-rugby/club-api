<?php

namespace App\Command\Datawarehouse;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\Datawarehouse\BuildGeomap;

#[AsCommand(
            name: 'datawarehouse:delete:geomap',
            description: 'Supprimer les données "geomap" de l\'entrepôt de données',
    )]
class GeomapDeleteCommand extends AbstractDeleteCommand {

    protected function deleteProcess(int $annee, SymfonyStyle $io): void {
        $io->info(sprintf('Supression des données "geomap" pour la saison %s', $annee));
        $saison = $this->saisonManager->convertir($annee);
        $service = new BuildGeomap($this->em, $this->saisonManager, $this->dispatcher, $this->logger, $io->createProgressBar());
        $nb = $service->purge($saison);
        $service->shutdown();
        $io->success(sprintf('%s données "geomap" supprimées', $nb));
    }
}
