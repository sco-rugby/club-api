<?php

namespace App\Manager\Contact;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Repository\Contact\ContactRepository;
use App\Entity\Contact\Commune;
use App\Entity\Contact\AdresseInterface;
use App\Model\Contact\ContactInterface;
use App\Seriliazer\ContactNormalizer;
use ScoRugby\CoreBundle\Manager\AbstractDispatchingManager;
use ScoRugby\CoreBundle\Model\ManagedResourceInterface;
use App\Manager\MultiSourceManagerInterface;
use App\Model\AppliExterne;

final class ContactManager extends AbstractDispatchingManager implements MultiSourceManagerInterface {

    private $adresseManager;

    public function __construct(ContactRepository $repository, ?EventDispatcherInterface $dispatcher = null, ?ValidatorInterface $validator = null, ?FormFactoryInterface $formFactory = null) {
        parent::__construct($repository, $dispatcher, $validator, $formFactory);
        $this->adresseManager = new AdresseManager($this->repository->getEntityManager()->getRepository(Commune::class));
    }

    public function setResource(ManagedResourceInterface $resource): void {
        if (!($resource instanceof ContactInterface)) {
            throw new \InvalidArgumentException(sprintf("Parameter MUST be an instance of %s. %s given", ContactInterface::class, get_class($resource)));
        }
        if (!$resource->getAdresse()->isEmpty()) {
            $adresse = $resource->getAdresse();
            $commune = $this->normalizeAdresse($adresse);
            $resource->setAdresse($adresse);
            $resource->setCommune($commune);
        }
        parent::setResource($resource);
    }

    public function createNormalizer(): ContactNormalizer {
        return new ContactNormalizer();
    }

    public function normalize(ContactInterface &$contact): void {
        $this->normalizeNom($contact);
        $this->normalizeAdresse($contact);
    }

    public function normalizeNom(ContactInterface &$contact): void {
        if (null != $contact->getNom() | null != $contact->getPrenom()) {
            $normalizer = $this->createNormalizer();
            $result = $normalizer->normalize($contact, ContactInterface::class);
            $contact->setNom($result['nom']);
            $contact->setPrenom($result['prenom']);
        }
    }

    /**
     * Définir Adresse et Commune pour geolocalisation
     */
    public function normalizeAdresse(AdresseInterface &$adresse): ?Commune {
        $this->adresseManager->normalize($adresse);
        // 
        if (null === $adresse->getVille()) {
            return null;
        }
        $commune = null;
        if (null != $adresse->getVille()) {
            $commune = $this->adresseManager->findByVille($adresse->getVille());
        }
        if (null == $commune) {
            $commune = $this->adresseManager->findApproxNom($adresse->getVille(), $adresse->getCodePostal());
        }
        if (null !== $commune) {
            $adresse->setVille($commune->getNom());
            if ($commune->isRegroupement() & null != $adresse->getComplement()) {
                $cmplmt = $this->adresseManager->findByVille($adresse->getComplement());
                if (null !== $cmplmt) {
                    $commune = $cmplmt;
                }
            }
        }
        return $commune;
    }

    public function isModificationAllowed(AppliExterne $appli): bool {
        return true;
    }
}
