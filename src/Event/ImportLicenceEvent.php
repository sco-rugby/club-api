<?php

namespace App\Event;

use App\Model\Import\Fichier;
use Symfony\Contracts\EventDispatcher\Event;

final class ImportLicenceEvent extends Event {

    public const JOB = 'import:licence';
    public const INIT = 'import.licence.init';
    public const START = 'import.licence.start';
    public const IMPORTED = 'import.licence.imported';
    public const SHUTDOWN = 'import.licence.shutdown';
    public consT FAILURE = 'import.licence.error';

    private $cr = [];

    public function __construct(protected Fichier $fichier) {
        return;
    }

    public function getFichier(): Fichier {
        return $this->fichier;
    }

    public function getCompteRendu(): array {
        return $this->cr;
    }

    public function setCompteRendu(array $cr): void {
        $this->cr = $cr;
    }
}
