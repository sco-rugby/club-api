<?php

namespace App\Entity\Club;

use App\Entity\Contact\Contact;
use App\Repository\Club\FonctionContactRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Saison;
use ScoRugby\CoreBundle\Entity\EntityInterface;

#[ORM\Entity(repositoryClass: FonctionContactRepository::class)]
#[ORM\Table(name: "contact_fonction")]
class FonctionContact implements EntityInterface {

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'fonctions', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Contact $contact = null;

    #[ORM\ManyToOne(inversedBy: 'contacts')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Fonction $fonction = null;

    #[ORM\ManyToOne(inversedBy: 'fonctions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Saison $saison = null;

    #[ORM\ManyToOne(inversedBy: 'fonctions')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Section $section = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $note = null;

    public function getId(): ?int {
        return $this->id;
    }

    public function getNote(): ?string {
        return $this->note;
    }

    public function setNote(?string $note): self {
        $this->note = $note;

        return $this;
    }

    public function getSaison(): ?Saison {
        return $this->saison;
    }

    public function setSaison(Saison $saison): self {
        $this->saison = $saison;
        return $this;
    }

    public function getContact(): ?Contact {
        return $this->contact;
    }

    public function setContact(?Contact $contact): self {
        $this->contact = $contact;

        return $this;
    }

    public function getFonction(): ?Fonction {
        return $this->fonction;
    }

    public function setFonction(?Fonction $fonction): self {
        $this->fonction = $fonction;

        return $this;
    }

    public function getSection(): ?Section {
        return $this->section;
    }

    public function setSection(?Section $section): self {
        $this->section = $section;

        return $this;
    }

    public function hasSection(): bool {
        return (bool) (null !== $this->section);
    }
}
