<?php

namespace App\Service\Datawarehouse;

use App\Entity\Saison;
use App\Model\SaisonInterface;
use App\Entity\Affilie\Affilie;
use App\Entity\Affilie\Licence as LicenceAffilie;
use App\Entity\Datawarehouse\Licence\Licence as LicenceAggregee;
use App\Entity\Datawarehouse\Licence\LicenceSaison;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\ORM\EntityNotFoundException;
use ScoRugby\TraitementBundle\Exception\FailedExecutionException;
use ScoRugby\TraitementBundle\Exception\NoExecutionException;

/**
 * Construit les stats licence aggrégées et détaillées par saison
 *    Licence\Licence
 *    Licence\LicenceSaison
 */
final class BuildLicence extends AbstractBuildService {

    public const JOB = 'datawarehouse:build:licence';

    private $saisonEnCours,
            $aggregations,
            $affilies = [],
            $licences = [],
            $saisons = [];

    /**
     * Initialisation du traitement pour une saison
     * - lister saison à traiter + saison en cours
     * - lister les licences déjà aggrégées
     * 
     * @param SaisonInterface $saison
     * @return void
     * @throws \Exception
     * @throws EntityNotFoundException
     * @throws FailedExecutionException
     */
    public function init(SaisonInterface $saison): void {
        parent::init($saison);
        // Saison en cours
        $this->saisonEnCours = $this->saisonManager->findEnCours();
        if (null === $this->saisonEnCours) {
            throw new EntityNotFoundException('Pas de saison en cours');
        }
        if ($this->hasLogger()) {
            $this->getLogger()->debug(sprintf('saison en cours : %s', $this->saisonEnCours));
        }
        $this->saisons[$this->saisonEnCours->getId()] = $this->saisonEnCours;
        //
        if ($this->saison->getId() != $this->saisonEnCours->getId()) {
            $this->saisons[$this->saison->getId()] = $this->saison;
        }
        //
        try {
            // Lister les licences déjà aggrégées
            //$dw = $this->em->getRepository(LicenceAggregee::class, 'datawarehouse')->findAll();
            $dw = $this->em->getRepository(LicenceAggregee::class)->findAll();
            if (empty($dw)) {
                $dw = [];
            }
            $this->aggregations = new ArrayCollection($dw);
            if ($this->hasLogger()) {
                $this->getLogger()->debug(sprintf('%s affilies trouvés dans datawarehouse', $this->aggregations->count()));
            }
        } catch (\Exception $e) {
            $this->setRollback();
            throw new FailedExecutionException($e->getMessage());
        }
    }

    /**
     * Purger les données "licence" poour une saison
     * 
     * @param SaisonInterface $saison
     * @return int
     */
    public function purge(SaisonInterface $saison): int {
        if ($this->hasLogger()) {
            $this->getLogger()->info(sprintf('supprimer les aggrégations "licence" de la saison %s', $saison));
        }
        //$nb = $this->em->getRepository(LicenceSaison::class, 'datawarehouse')->deleteSaison($saison, true);
        $nb = $this->em->getRepository(LicenceSaison::class)->deleteSaison($saison, true);
        if ($this->hasLogger()) {
            $this->getLogger()->debug($nb . ' données supprimées');
        }
        return $nb;
    }

    /**
     * Purger les données "licence aggrégéé"
     * NOTE : Ces données sont globales et pas pour une saison
     * 
     * @return int
     */
    protected function purgeLicenceAggregee(): int {
        if ($this->hasLogger()) {
            $this->getLogger()->info('supprimer toutes les licences aggrégées dans le datawarehouse');
        }
        $nb = $this->em->getRepository(LicenceAggregee::class)
                ->createQueryBuilder('ls')
                ->delete()
                ->getQuery()
                ->execute();
        $this->em()->flush();
        return $nb;
    }

    /**
     * Traitement de construction :
     * - cacul des stats licence
     * - enregistement en base
     * 
     * @return void
     * @throws FailedExecutionException
     */
    public function build(): void {
        try {
            // Lister les licences club pour la saison, si aucune => terminer le processus
            if (empty($this->getLicences())) {
                throw new NoExecutionException(sprintf('Aucune licence club pour la saison %s', $this->saison));
            }
            $nb = count($this->licences);
            if ($this->hasLogger()) {
                $this->getLogger()->info(sprintf('%s licences à aggréger', $nb));
            }
            //
            $this->buildStats();
            $this->buildData();
            //
        } catch (\Exception $ex) {
            $this->setRollback();
            throw new FailedExecutionException($ex->getMessage());
        } catch (NoExecutionException $ex) {
            if ($this->hasLogger()) {
                $this->getLogger()->info($ex->getMessage());
            }
        }
    }

    /**
     * Caculer les stats "licence"
     * 
     * @return void
     * @throws \Exception
     */
    public function buildStats(): void {
        if ($this->hasLogger()) {
            $this->getLogger()->info('Calcul des stats licence');
        }
        try {
            $this->startCommand(count($this->licences));
            foreach ($this->getLicences() as $licence) {
                if ($this->hasLogger()) {
                    $this->getLogger()->info(sprintf('Traitement licence %s pour %s (%s)', $licence->getQualite()->getId(), $licence->getAffilie(), $licence->getAffilie()->getId()));
                }
                $this->calculerStatsAffilie($licence->getAffilie());
                $this->calculerStatsSaison($licence);
                //
                $this->nextStep();
            }
        } catch (\Exception $e) {
            if ($this->hasLogger()) {
                $this->getLogger()->error('buildStats : ' . $e->getMessage(), get_class($e));
            }
            throw $e;
        } finally {
            $this->endCommand();
        }
    }

    /**
     * Enregistement des données calculées en base
     * 
     * @return void
     * @throws \Exception
     */
    public function buildData(): void {
        if ($this->hasLogger()) {
            $this->getLogger()->info('Enregistements des stats licence');
        }
        try {
            $this->startCommand(count($this->affilies));
            $criteria = new Criteria();
            forEach ($this->affilies as $id => $data) {
                // Recherche affilié
                $expr = new Comparison('id', '=', $id);
                $criteria->where($expr);
                $coll = $this->aggregations->matching($criteria);
                // Si pas encore aggrégée
                if ($coll->isEmpty()) {
                    $stat = new LicenceAggregee();
                } else {
                    $stat = $coll->first();
                }
                $this->setDataAfffilie($stat, $data);
//                $this->em->getRepository(LicenceAggregee::class, 'datawarehouse')->save($stat);
                $this->em->getRepository(LicenceAggregee::class)->save($stat);
                // Licence saison
                $licenceSaison = $this->createSaison($stat);
                $this->setDataSaison($licenceSaison, $data);
//                $this->em->getRepository(LicenceSaison::class, 'datawarehouse')->save($licenceSaison);
                $this->em->getRepository(LicenceSaison::class)->save($licenceSaison);
                //
                $this->nextStep();
            }
            $this->em->flush();
        } catch (\Exception $e) {
            if ($this->hasLogger()) {
                $this->getLogger()->error('buildData : ' . $e->getMessage(), get_class($e));
            }
            throw $e;
        } finally {
            $this->endCommand();
        }
    }

    ###########
    # DONNEES BRUTES
    ###########

    /**
     * Retrouver toutes les licences pour une saison
     * 
     * @return array
     */
    private function getLicences(): array {
        if (empty($this->licences)) {
//            $this->licences = $this->em->getRepository(LicenceAffilie::class, 'default')->findBySaison($this->saison);
            $this->licences = $this->em->getRepository(LicenceAffilie::class)->findBySaison($this->saison);
        }
        return $this->licences;
    }

    ###########
    # AFFILIE
    ###########

    /**
     * @see BuildLicence::buildStats
     * @param Affilie $affilie
     * @return void
     */
    private function calculerStatsAffilie(Affilie $affilie): void {
        if (key_exists($affilie->getId(), $this->affilies)) {
            return;
        }
        if ($this->hasLogger()) {
            $this->getLogger()->info(sprintf('Traitement affilié %s (%s)', $affilie, $affilie->getId()));
        }
        //$stats = $this->em->getRepository(LicenceAffilie::class, 'default')->getStatsAffilie($affilie);
        $stats = $this->em->getRepository(LicenceAffilie::class)->getStatsAffilie($affilie);
        // Déterminer stats
        $affilieId = $affilie->getId();
        $this->affilies[$affilieId]['id'] = $affilieId; // utlisé par BuildLicence::setDataAfffilie()
        $this->affilies[$affilieId]['nom'] = $affilie->getContact()->getNom();
        $this->affilies[$affilieId]['prenom'] = $affilie->getContact()->getPrenom();
        $this->affilies[$affilieId]['date_naissance'] = $affilie->getDateNaissance();
        $this->affilies[$affilieId]['1ere_affiliation'] = $affilie->getPremiereAffiliation();
        $this->affilies[$affilieId]['nb_saisons'] = $stats['nb'];
        if (!array_key_exists($stats['debut'], $this->saisons)) {
            //$this->saisons[$stats['debut']] = $this->em->getRepository(Saison::class, 'default')->find($stats['debut']);
            $this->saisons[$stats['debut']] = $this->saisonManager->get($stats['debut']);
        }
        $this->affilies[$affilieId]['saison_debut'] = $stats['debut'];
        // Fin
        if ($stats['fin'] < $this->saisonEnCours->getId()) {
            if (!array_key_exists($stats['fin'], $this->saisons)) {
                //$this->saisons[$stats['fin']] = $this->em->getRepository(Saison::class, 'default')->find($stats['fin']);
                $this->saisons[$stats['fin']] = $this->saisonManager->get($stats['fin']);
            }
            $this->affilies[$affilieId]['saison_fin'] = $stats['fin'];
        } else {
            $this->affilies[$affilieId]['saison_fin'] = null;
        }
        $this->affilies[$affilieId]['arbitre'] = false;
        $this->affilies[$affilieId]['educateur'] = false;
        $this->affilies[$affilieId]['dirigeant'] = false;
        $this->affilies[$affilieId]['joueur'] = false;
        $this->affilies[$affilieId]['soigneur'] = false;
        $this->affilies[$affilieId]['debut'] = false;
        $this->affilies[$affilieId]['arret'] = false;
        if ($this->saison->getId() == $stats['debut']) {
            $this->affilies[$affilieId]['debut'] = true;
        }
        if ($this->saison->getId() == $this->affilies[$affilieId]['saison_fin']) {
            $this->affilies[$affilieId]['arret'] = true;
        }
    }

    private function setDataAfffilie(LicenceAggregee &$licence, array $data): void {
        if ($this->hasLogger()) {
            $this->getLogger()->info(sprintf('Enregistements des stats Affilié %s', $licence->getId()), $data);
        }
        if (null === $licence->getId()) {
            $licence->setId($data['id'])
                    ->setNom($data['nom'])
                    ->setPrenom($data['prenom'])
                    ->setDateNaissance($data['date_naissance'])
                    ->setPremiereAffiliation($data['1ere_affiliation']);
        }
        $licence->setNombreSaisons($data['nb_saisons']);
        // Debut
        if (null === $licence->getSaisonDebut() || $data['saison_debut'] != $licence->getSaisonDebut()->getId()) {
            $saisonDebut = $this->saisons[$data['saison_debut']];
            $this->saisonManager->setResource($saisonDebut);
            $licence->setSaisonDebut($saisonDebut);
            $licence->setAgeDebut($this->saisonManager->calculerAge($licence));
        }
        // Fin
        if (null !== $data['saison_fin']) {
            $saisonFin = $this->saisons[$data['saison_fin']];
            $this->saisonManager->setResource($saisonFin);
            $licence->setSaisonFin($saisonFin);
            $licence->setAgeFin($this->saisonManager->calculerAge($licence));
        } else {
            // âge actuel
            $this->saisonManager->setResource($this->saisonEnCours);
            $licence->setAgeActuel($this->saisonManager->calculerAge($licence));
        }
        // 1ere affiliation
        if (null === $licence->getPremiereAffiliation()) {
            $licence->setPremiereAffiliation($licence->getSaisonDebut());
        } elseif ($licence->getPremiereAffiliation()->getId() > $licence->getSaisonDebut()->getId()) {
            $licence->setPremiereAffiliation($licence->getSaisonDebut());
        }
    }

    ###########
    # SAISON
    ###########

    private function calculerStatsSaison(LicenceAffilie $licence): void {
        $affilieId = $licence->getAffilie()->getId();
        switch ($licence->getQualite()->getGroupe()) {
            case 'A':
                $this->affilies[$affilieId]['arbitre'] = true;
                break;
            case 'E':
                $this->affilies[$affilieId]['educateur'] = true;
                break;
            case 'D':
                $this->affilies[$affilieId]['dirigeant'] = true;
                break;
            case 'J':
                $this->affilies[$affilieId]['joueur'] = true;
                break;
            case 'S':
                $this->affilies[$affilieId]['soigneur'] = true;
                break;
        }
    }

    private function createSaison(LicenceAggregee $licence): LicenceSaison {
        $licenceSaison = new LicenceSaison();
        $licenceSaison->setLicence($licence);
        $licenceSaison->setSaison($this->saison);
        $this->saisonManager->setResource($this->saison);
        $licenceSaison->setAge($this->saisonManager->calculerAge($licence));
//        $licenceSaison->setNombreSaisons($this->em->getRepository(LicenceAffilie::class, 'default')->getNbSaisons($licence, $this->saison));
        $licenceSaison->setNombreSaisons($this->em->getRepository(LicenceAffilie::class)->getNbSaisons($licence, $this->saison));
        return $licenceSaison;
    }

    private function setDataSaison(LicenceSaison &$licence, array $data): void {
        if ($this->hasLogger()) {
            $this->getLogger()->info(sprintf('Enregistements des stats Affilié %s pour %s', $licence->getId(), $licence->getSaison()), $data);
        }
        $licence->setArbitre($data['arbitre']);
        $licence->setEducateur($data['educateur']);
        $licence->setDirigeant($data['dirigeant']);
        $licence->setJoueur($data['joueur']);
        $licence->setSoigneur($data['soigneur']);
        $licence->setDebut($data['debut']);
        $licence->setArret($data['arret']);
    }
}
