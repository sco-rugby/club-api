<?php

namespace App\Seriliazer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\String\UnicodeString;
use App\Model\AdresseInterface;

/**
 * Description of AdresseMormalizer
 *
 * @author Antoine BOUET
 */
class AdresseNormalizer implements NormalizerInterface {

    private readonly CommuneNormalizer $communeNormalizer;

    public function __construct() {
        $this->communeNormalizer = new CommuneNormalizer();
    }

    public function normalize(mixed $object, ?string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null {
        $elmt['adresse'] = $object->getAdresse();
        $elmt['complement'] = $object->getComplement();
        $elmt['ville'] = $object->getVille();
        //
        foreach ($elmt as $id => $value) {
            if (!empty($value)) {
                $elmt[$id] = (new UnicodeString($value))->trim()
                        ->folded()->title(true)
                        ->replace(' Le ', ' le ')
                        ->replace("L'", "l'")
                        ->replace("D'", "d'")
                        ->replace(' De ', ' de ')
                        ->replace(' Des ', ' des ')
                        ->replace(' Du ', ' du ')
                        ->replace(' En ', ' en ')
                        ->toString();
            }
        }
        //
        if (!empty($object->getVille())) {
            $elmt['ville'] = $this->communeNormalizer->normalize($object->getVille(), 'string', ['canonize' => true]);
        }
        if (!empty($object->getCodePostal())) {
            $elmt['code_postal'] = (new UnicodeString($object->getCodePostal()))->trim()->replace(' ', '')->toString();
        } else {
            $elmt['code_postal'] = null;
        }
        return $elmt;
    }

    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool {
        return $data instanceof AdresseInterface;
    }

    public function getSupportedTypes(?string $format): array {
        return [
            AdresseInterface::class => true
        ];
    }
}
