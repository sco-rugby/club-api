<?php

namespace App\Entity\Evenement;

use ScoRugby\CalendrierBundle\Repository\CalendrierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ScoRugby\CoreBundle\Entity\AbstractTaxonomie;
use ScoRugby\CoreBundle\Entity\SluggifyInterface;
use ScoRugby\CoreBundle\Entity\Slug;
use App\Model\Evenement\CalendrierInterface;
use App\Model\Evenement\CalendarEventInterface;

#[ORM\Entity(repositoryClass: CalendrierRepository::class)]
#[ORM\UniqueConstraint(name: "unq_calendrier_slug", columns: ["slug"])]
class Calendrier extends AbstractTaxonomie implements CalendrierInterface, SluggifyInterface {

    /**
     * @var Collection<int, Evenement>
     */
    #[ORM\ManyToMany(targetEntity: Evenement::class, mappedBy: 'calendriers')]
    private Collection $evenements;

    public function __construct() {
        parent::__construct();
        $this->groupes = new ArrayCollection();
        $this->evenements = new ArrayCollection();
        $this->slug = new Slug();
    }

    public function getSlug(): Slug {
        return $this->slug;
    }

    /**
     * @return Collection<int, Evenement>
     */
    public function getEvenements(): Collection {
        return $this->evenements;
    }

    public function addEvenement(CalendarEventInterface $evenements): static {
        if (!$this->evenements->contains($evenements)) {
            $this->evenements->add($evenements);
            $evenements->addCalendrier($this);
        }

        return $this;
    }

    public function removeEvenement(CalendarEventInterface $evenements): static {
        if ($this->evenements->removeElement($evenements)) {
            $evenements->removeCalendrier($this);
        }

        return $this;
    }
}
