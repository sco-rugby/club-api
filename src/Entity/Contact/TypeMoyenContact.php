<?php

namespace App\Entity\Contact;

use Doctrine\ORM\Mapping as ORM;
use App\Model\Contact\TypeMoyenContact as MoyenContact;
use ScoRugby\CoreBundle\Exception\InvalidParameterException;

/**
 * Description of Type
 *
 * @author Antoine BOUET
 */
#[ORM\Embeddable]
class TypeMoyenContact {

    #[ORM\Column(length: 1, options: [
            "comment" => "D domicile,P pro,M mobile,A autre+type_libelle",
            'default' => "A"]
        )]
    protected ?string $type = null;

    #[ORM\Column(length: 20, nullable: true)]
    protected ?string $libelle = null;

    #[ORM\Column]
    protected bool $prefere = false;

    public function getId(): ?string {
        return $this->type;
    }

    public function setId(string $id): self {
        $type = MoyenContact::tryFrom($id);
        if (null === $type) {
            throw new InvalidParameterException(sprintf('A type MUST be on of %s. %s given', implode(',', array_column(MoyenContact::cases(), 'values')), $id));
        }
        $this->type = $id;
        return $this;
    }

    public function getLibelle(): ?string {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self {
        $this->libelle = $libelle;
        return $this;
    }

    public function isPrefere(): ?bool {
        return $this->prefere;
    }

    public function setPrefere(bool $prefere = true): self {
        $this->prefere = $prefere;

        return $this;
    }
}
