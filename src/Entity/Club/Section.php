<?php

namespace App\Entity\Club;

use App\Entity\Affilie\AffilieSection;
use App\Entity\Affilie\TarifLicence;
use App\Repository\Club\SectionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use App\Collection\CollectionFonction;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Contact\Groupe;
use App\Entity\Saison;
use ScoRugby\CoreBundle\Model\ColorisableInterface;
use \ScoRugby\CoreBundle\Model\ColorisableTrait;

#[ORM\Entity(repositoryClass: SectionRepository::class, readOnly: true)]
class Section extends Groupe implements ColorisableInterface {

    use ColorisableTrait;

    #[ORM\Column(length: 1, nullable: true)]
    private ?string $sexe = null;

    #[ORM\Column(nullable: true)]
    private ?int $ageDebut = null;

    #[ORM\Column(nullable: true)]
    private ?int $ageFin = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'sousSections')]
    private ?self $parent = null;

    #[ORM\OneToMany(mappedBy: 'section', targetEntity: AffilieSection::class)]
    private Collection $affilies;

    #[ORM\OneToMany(mappedBy: 'section', targetEntity: FonctionContact::class)]
    private Collection $fonctions;

    #[ORM\OneToMany(targetEntity: self::class, mappedBy: 'parent')]
    private Collection $sousSections;

    /**
     * @var Collection<int, TarifLicence>
     */
    #[ORM\OneToMany(targetEntity: TarifLicence::class, mappedBy: 'section', orphanRemoval: true)]
    private Collection $tarifs;

    public function __construct() {
        parent::__construct();
        $this->affilies = new ArrayCollection();
        $this->fonctions = new CollectionFonction();
        $this->sousSections = new ArrayCollection();
        $this->tarifs = new ArrayCollection();
    }

    public function isMixte(): ?bool {
        return (null === $this->sexe);
    }

    public function getSexe(): ?string {
        return $this->sexe;
    }

    public function setSexe(?string $sexe): self {
        $this->sexe = $sexe;
        return $this;
    }

    public function getAgeDebut(): ?int {
        return $this->ageDebut;
    }

    public function setAgeDebut(int $ageDebut): static {
        $this->ageDebut = $ageDebut;

        return $this;
    }

    public function getAgeFin(): ?int {
        return $this->ageFin;
    }

    public function setAgeFin(?int $ageFin): static {
        $this->ageFin = $ageFin;

        return $this;
    }

    /**
     * @return Collection<int, AffilieSection>
     */
    public function getAffilies(): Collection {
        return $this->affilies;
    }

    public function addAffilie(AffilieSection $affilie): self {
        if (!$this->affilies->contains($affilie)) {
            $this->affilies->add($affilie);
            $affilie->setSection($this);
        }

        return $this;
    }

    public function removeAffilie(AffilieSection $affilie): self {
        if ($this->affilies->removeElement($affilie)) {
            // set the owning side to null (unless already changed)
            if ($affilie->getSection() === $this) {
                $affilie->setSection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, FonctionContact>
     */
    public function getFonctions(Saison $saison = null): Collection {
        return $this->fonctions->bySection($this, $sasison);
    }

    public function addFonction(FonctionContact $fonction): self {
        if (!$this->fonctions->contains($fonction)) {
            $this->fonctions->add($fonction);
            $fonction->setSection($this);
        }

        return $this;
    }

    public function removeFonction(FonctionContact $fonction): self {
        if ($this->fonctions->removeElement($fonction)) {
            // set the owning side to null (unless already changed)
            if ($fonction->getSection() === $this) {
                $fonction->setSection(null);
            }
        }

        return $this;
    }

    public function getParent(): ?self {
        return $this->parent;
    }

    public function setParent(?self $parent): self {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getSousSections(): Collection {
        return $this->sousSections;
    }

    public function addSousSection(self $sousSection): self {
        if (!$this->sousSections->contains($sousSection)) {
            $this->sousSections->add($sousSection);
            $sousSection->setParent($this);
        }

        return $this;
    }

    public function removeSousSection(self $sousSection): self {
        if ($this->sousSections->removeElement($sousSection)) {
            // set the owning side to null (unless already changed)
            if ($sousSection->getParent() === $this) {
                $sousSection->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Tarif>
     */
    public function getTarifs(): Collection {
        return $this->tarifs;
    }

    public function addTarif(Tarif $tarif): self {
        if (!$this->tarifs->contains($tarif)) {
            $this->tarifs->add($tarif);
            $tarif->setSection($this);
        }

        return $this;
    }

    public function removeTarif(Tarif $tarif): self {
        if ($this->tarifs->removeElement($tarif)) {
            // set the owning side to null (unless already changed)
            if ($tarif->getSection() === $this) {
                $tarif->setSection(null);
            }
        }

        return $this;
    }
}
