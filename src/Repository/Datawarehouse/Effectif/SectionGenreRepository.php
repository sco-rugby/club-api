<?php

namespace App\Repository\Datawarehouse\Effectif;

use App\Model\SaisonInterface;
use App\Entity\Datawarehouse\Effectif\SectionGenre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\SaisonManagedRepositoryInterface;

/**
 * @extends ServiceEntityRepository<Effectif>
 *
 * @method Effectif|null find($id, $lockMode = null, $lockVersion = null)
 * @method Effectif|null findOneBy(array $criteria, array $orderBy = null)
 * @method Effectif[]    findAll()
 * @method Effectif[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SectionGenreRepository extends ServiceEntityRepository implements SaisonManagedRepositoryInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, SectionGenre::class);
    }

    public function save(Section $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Section $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function deleteSaison(SaisonInterface $saison, bool $flush = false) {
        $nb = $this->createQueryBuilder('s')
                ->delete()
                ->where('s.saison = :saison')
                ->setParameter('saison', $saison->getId())
                ->getQuery()
                ->execute();
        if ($flush) {
            $this->getEntityManager()->flush();
        }
        return $nb;
    }

    public function findBySaison(SaisonInterface $saison): array {
        return $this->createQueryBuilder('s')
                        ->where('s.saison = :saison')
                        ->setParameter('saison', $saison->getId())
                        ->orderBy('s.licence', 'ASC')
                        ->getQuery()
                        ->getResult();
    }
}
