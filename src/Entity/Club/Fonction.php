<?php

namespace App\Entity\Club;

use App\Entity\Contact\Groupe;
use App\Repository\Club\FonctionRepository;
use App\Collection\CollectionFonction;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use ScoRugby\CoreBundle\Entity\AbstractTaxonomie;

#[ORM\Entity(repositoryClass: FonctionRepository::class)]
class Fonction extends AbstractTaxonomie {

    #[ORM\OneToMany(mappedBy: 'fonction', targetEntity: FonctionContact::class, orphanRemoval: true)]
    private Collection $contacts;

    /**
     * @var Collection<int, Groupe>
     */
    #[ORM\OneToMany(targetEntity: Groupe::class, mappedBy: 'fonction')]
    private Collection $groupes;

    #[ORM\Column(type: Types::JSON, nullable: true)]
    private ?array $droits = null;

    public function __construct() {
        parent::__construct();
        $this->contacts = new CollectionFonction();
        $this->groupes = new ArrayCollection();
    }

    /**
     * @return Collection<int, FonctionsClub>
     */
    public function getContacts(): Collection {
        return $this->contacts;
    }

    public function addContact(FonctionContact $contact): self {
        if (!$this->contacts->contains($contact)) {
            $this->contacts->add($contact);
            $contact->setFonction($this);
        }

        return $this;
    }

    public function removeContact(FonctionContact $contact): self {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getFonction() === $this) {
                $contact->setFonction(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Groupe>
     */
    public function getGroupes(): Collection {
        return $this->groupes;
    }

    public function addGroupe(Groupe $groupe): static {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes->add($groupe);
            $groupe->setFonction($this);
        }

        return $this;
    }

    public function removeGroupe(Groupe $groupe): static {
        if ($this->groupes->removeElement($groupe)) {
            // set the owning side to null (unless already changed)
            if ($groupe->getFonction() === $this) {
                $groupe->setFonction(null);
            }
        }

        return $this;
    }

    public function getDroits(): ?array
    {
        return $this->droits;
    }

    public function setDroits(?array $droits): static
    {
        $this->droits = $droits;

        return $this;
    }
}
