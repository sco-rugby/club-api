<?php

namespace App\Service;

use App\Model\ContactInterface;

/**
 *
 * @author Antoine BOUET
 */
interface UsingContactInterface {

    public function findContactByName(string $nom, string $prenom, ?string $cp): ?ContactInterface;

    public function findContactByEmail(): ?ContactInterface;

    public function findContactByPhone(): ?ContactInterface;

    public function createContact(array $data): ContactInterface;

    public function isContactCreated(string $nom, string $prenom, ?string $cp): bool;
}
