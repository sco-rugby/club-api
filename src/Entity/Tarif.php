<?php

namespace App\Entity;

use App\Repository\TarifRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Affilie\TarifLicence;
use App\Entity\Import\ImportableInterface;
use ScoRugby\CoreBundle\Entity\SluggifyInterface;
use ScoRugby\CoreBundle\Entity\Slug;
use App\Entity\Import\ImportApplicationInterface;
use App\Entity\Import\SingleSourceImport;

#[ORM\Entity(repositoryClass: TarifRepository::class)]
#[ORM\MappedSuperclass]
#[ORM\InheritanceType("SINGLE_TABLE")]
#[ORM\DiscriminatorColumn(name: "class", type: "string", length: 7)]
#[ORM\DiscriminatorMap(['' => Tarif::class, 'licence' => TarifLicence::class])]
#[ORM\UniqueConstraint(name: "unq_tarif_slug", columns: ["slug"])]
class Tarif implements ImportableInterface, SluggifyInterface {

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $prix = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'promotions')]
    private ?self $tarif = null;

    #[ORM\Embedded(class: Slug::class, columnPrefix: false)]
    private Slug $slug;

    #[ORM\Embedded(class: SingleSourceImport::class, columnPrefix: false)]
    private SingleSourceImport $appli;

    /**
     * @var Collection<int, self>
     */
    #[ORM\OneToMany(targetEntity: self::class, mappedBy: 'tarif')]
    private Collection $promotions;

    public function __construct() {
        $this->promotions = new ArrayCollection();
        $this->appli = new SingleSourceImport();
        $this->slug = new Slug();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getPrix(): ?int {
        return $this->prix;
    }

    public function setPrix(int $prix): static {
        $this->prix = $prix;

        return $this;
    }

    public function getTarif(): ?self {
        return $this->tarif;
    }

    public function setTarif(?self $tarif): static {
        $this->tarif = $tarif;

        return $this;
    }

    public function getSlug(): Slug {
        return $this->slug;
    }

    public function getAppliMaitre(): ?ImportApplicationInterface {
        return $this->appli;
    }

    public function setAppliMaitre(ImportApplicationInterface $appli): self {
        $this->appli = $appli;
        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getPromotions(): Collection {
        return $this->promotions;
    }

    public function addPromotion(self $promotion): static {
        if (!$this->promotions->contains($promotion)) {
            $this->promotions->add($promotion);
            $promotion->setTarif($this);
        }

        return $this;
    }

    public function removePromotion(self $promotion): static {
        if ($this->promotions->removeElement($promotion)) {
            // set the owning side to null (unless already changed)
            if ($promotion->getTarif() === $this) {
                $promotion->setTarif(null);
            }
        }

        return $this;
    }
}
