<?php

namespace App\Entity\Evenement;

use App\Entity\Contact\Contact;
use App\Entity\Import\ImportableInterface;
use App\Entity\Import\ImportApplicationInterface;
use App\Entity\Import\SingleSourceImport;
use App\Repository\Evenement\ParticipantRepository;
use ScoRugby\CoreBundle\Entity\DateTimeBlameableInterface;
use ScoRugby\CoreBundle\Entity\DateTimeBlameable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: ParticipantRepository::class)]
class Participant implements ImportableInterface, DateTimeBlameableInterface {

    CONST REPONSE_ABSENT = 0;
    CONST REPONSE_PRESENT = 1;
    CONST REPONSE_INDECIS = 2;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Embedded(class: DateTimeBlameable::class, columnPrefix: false)]
    private DateTimeBlameable $datetime;

    #[ORM\Column(nullable: true)]
    private ?int $reponse = null;

    #[ORM\Column(nullable: true)]
    private ?bool $presence = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    private ?Uuid $token;

    #[ORM\ManyToOne(inversedBy: "participants")]
    private ?Evenement $evenement = null;

    #[ORM\ManyToOne(inversedBy: 'participations')]
    #[ORM\JoinColumn(nullable: false)]
    protected ?Contact $contact = null;

    #[ORM\Embedded(class: SingleSourceImport::class)]
    protected SingleSourceImport $appli;

    public function __construct() {
        $this->datetime = new DateTimeBlameable();
        $this->appli = new SingleSourceImport();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getToken(): ?Uuid {
        return $this->token;
    }

    public function getDateTime(): DateTimeBlameable {
        return $this->datetime;
    }

    public function getReponse(): ?int {
        return $this->reponse;
    }

    public function setReponsePresent() {
        $this->reponse = self::REPONSE_PRESENT;
    }

    public function setReponseAbsent() {
        $this->reponse = self::REPONSE_ABSENT;
    }

    public function setReponseIndecis() {
        $this->reponse = self::REPONSE_INDECIS;
    }

    public function getPresence(): ?bool {
        return $this->presence;
    }

    public function setPresence(?bool $presence = true): static {
        $this->presence = $presence;

        return $this;
    }

    public function getContact(): ?Contact {
        return $this->contact;
    }

    public function setContact(?Contact $contact): static {
        $this->contact = $contact;

        return $this;
    }

    public function getEvenement(): ?Evenement {
        return $this->evenement;
    }

    public function setEvenement(?Evenement $evenement): static {
        $this->evenement = $evenement;

        return $this;
    }

    //[PrePersist]
    public function generateToken(PrePersistEventArgs $eventArgs) {
        $this->token = Uuid::v5();
    }

    public function getAppliMaitre(): ?ImportApplicationInterface {
        return $this->appli;
    }

    public function setAppliMaitre(ImportApplicationInterface $appli): self {
        $this->appli = $appli;
        return $this;
    }
}
