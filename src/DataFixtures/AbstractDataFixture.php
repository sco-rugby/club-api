<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use ScoRugby\TraitementBundle\Exception\FailedExecutionException;
use ScoRugby\TraitementBundle\Exception\NoExecutionException;

/**
 * Description of AbstractDataFixture
 *
 * @author Antoine BOUET
 */
abstract class AbstractDataFixture extends Fixture {

    protected function guessDataFile(): \SplFileInfo {
        $reflectionClass = new \ReflectionClass($this);
        $nom = strtolower((str_replace('Fixture', '', $reflectionClass->getShortName())));
        return new \SplFileInfo(dirname(__FILE__) . '/../../data/' . $nom . '.csv');
    }

    protected function loadDataFile(): array {
        try {
            $fichier = $this->guessDataFile();
            if (!$fichier->isReadable()) {
                throw new NoExecutionException(sprintf('Le fichier %s ne peut pas être lu', $fichier));
            }
            $reader = new Csv();
            $encoding = Csv::guessEncoding($fichier->getPathname());
            $reader->setInputEncoding($encoding);
            $reader->setDelimiter(',');
            $reader->setEnclosure('"');
            $reader->setSheetIndex(0);
            $reader->setReadDataOnly(true);
            $csv = $reader->load($fichier->getPathname());
            $worksheet = $csv->getActiveSheet();
            $rows = [];
            foreach ($worksheet->getRowIterator() as $row) {
                if (1 == $row->getRowIndex()) {
                    $headers = [];
                    foreach ($row->getCellIterator() as $cell) {
                        $headers[] = $cell->getValue();
                    }
                } else {
                    $values = [];
                    foreach ($row->getCellIterator() as $cell) {
                        $values[] = $cell->getValue();
                    }
                    $rows[] = array_combine($headers, $values);
                }
            }
            return $rows;
        } catch (\Exception $e) {
            throw new FailedExecutionException($e->getMessage());
        }
    }
}
