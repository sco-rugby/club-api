<?php

namespace App\Repository\Contact;

use App\Entity\Contact\Contact;
use App\Entity\Contact\ContactExterne;
use App\Entity\Affilie\Affilie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\ImportableRepositoryInferface;
use App\Entity\Import\ImportableInterface;

/**
 * @extends ServiceEntityRepository<Evenement>
 */
class ContactRepository extends ServiceEntityRepository implements ImportableRepositoryInferface {

    public function __construct(ManagerRegistry $registry) {
        ServiceEntityRepository::__construct($registry, Contact::class);
    }

    public function save(Contact $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Contact $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllForSearch(): array {
        return $this->createQueryBuilder('c')
                        ->select('c.id')
                        ->addSelect('c.canonizedNom')
                        ->addSelect('c.adresse.codePostal')
                        ->addSelect('a.dateNaissance')
                        ->leftJoin('c.affilie', 'a')
                        ->orderBy('c.nom', 'ASC')
                        ->addOrderBy('c.prenom', 'ASC')
                        ->getQuery()
                        ->getScalarResult();
    }

    protected function findContactByDoB(string $nom, string $prenom, \DateTimeInterface $dob): ?Contact {
        return $this->getEntityManager()
                        ->createQuery("SELECT c FROM App\Entity\Affilie\Affilie a JOIN a.contact c  WHERE a.date_naissance = :dob and canonized_nom=:nom and canonized_prenom=:prenom")
                        ->setParameter('nom', $nom)
                        ->setParameter('prenom', $prenom)
                        ->setParameter('dob', $dob)
                        ->getQuery()
                        ->getSingleResult();
    }

    protected function findContactByEmail(string $email): ?Contact {
        return $this->getEntityManager()
                        ->createQuery("SELECT c FROM ScoRugby\ContactBundle\Entity\ContactEmail e JOIN e.contact c WHERE e.email = :email")
                        ->setParameter('email', $email)
                        ->getQuery()
                        ->getSingleResult();
    }

    protected function findContactByPhone(string $phone): ?Contact {
        return $this->getEntityManager()
                        ->createQuery("SELECT c FROM ScoRugby\ContactBundle\Entity\ContactTelephone p JOIN p.contact c WHERE p.numero = :phone")
                        ->setParameter('phone', $phone)
                        ->getQuery()
                        ->getSingleResult();
    }

    public function findByExternalId(string $externalId): ?ImportableInterface {
        return $this->createQueryBuilder('c')
                        ->innerJoin(ContactExterne::class, 'e')
                        ->where('e.externalId = :externalId')
                        ->setParameter('externalId', $externalId)
                        ->getQuery()
                        ->getOneOrNullResult();
    }

    public function findByAppliMaitre(string $appliMaitre): array {
        return $this->createQueryBuilder('c')
                        ->innerJoin(ContactExterne::class, 'e')
                        ->where('e.appli = :appli')
                        ->setParameter('appli', $appliMaitre)
                        ->getQuery()
                        ->getOneOrNullResult();
    }

    public function findByImportedDate(\DateTimeInterface $date): array {
        return createQueryBuilder('c')
                        ->innerJoin(ContactExterne::class, 'e')
                        ->where('e.synchronizedAt = :date')
                        ->setParameter('date', $date)
                        ->getQuery()
                        ->getOneOrNullResult();
    }
}
