<?php

namespace App\Entity\Affilie;

use App\Repository\Affilie\DiplomeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ScoRugby\CoreBundle\Entity\AbstractTaxonomie;

#[ORM\Entity(repositoryClass: DiplomeRepository::class, readOnly: true)]
class Diplome extends AbstractTaxonomie {

    #[ORM\OneToMany(mappedBy: 'diplome', targetEntity: AffilieDiplome::class)]
    private Collection $affilies;

    #[ORM\Column(length: 100)]
    private ?string $canonizedLibelle = null;

    public function __construct() {
        parent::__construct();
        $this->affilies = new ArrayCollection();
    }

    public function getCanonizedLibelle(): ?string {
        return $this->canonizedLibelle;
    }

    public function setCanonizedLibelle(string $libelle): self {
        $this->canonizedLibelle = $libelle;

        return $this;
    }

    /**
     * @return Collection<int, AffilieDiplome>
     */
    public function getAffilies(): Collection {
        return $this->affilies;
    }

    public function addAffily(AffilieDiplome $affily): self {
        if (!$this->affilies->contains($affily)) {
            $this->affilies->add($affily);
            $affily->setDiplome($this);
        }

        return $this;
    }

    public function removeAffily(AffilieDiplome $affily): self {
        if ($this->affilies->removeElement($affily)) {
            // set the owning side to null (unless already changed)
            if ($affily->getDiplome() === $this) {
                $affily->setDiplome(null);
            }
        }

        return $this;
    }
}
