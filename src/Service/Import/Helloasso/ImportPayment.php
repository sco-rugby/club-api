<?php

namespace App\Service\Import\Helloasso;

use App\Service\Import\AbstractApiImportService;
use Helloasso\Service\EventService;
use Helloasso\Models\Statistics\Payer;
use App\Entity\Import\SingleSourceImport;
use App\Model\AppliExterne;
use App\Entity\Paiement;
use App\Entity\Contact\Contact;

/**
 * Description of ImportPayment
 *
 * @author Antoine BOUET
 */
class ImportPayment extends AbstractApiImportService {

    public function import(mixed $data): void {

        $eventService = new EventService($this->params->get('appli.helloasso.client_id'), $this->params->get('appli.helloasso.secret'), $this->params->get('appli.helloasso.slug'));
        $importObj = $eventService->decode($data)->getData();
        //
        $appli = new SingleSourceImport();
        $appli->setNom(AppliExterne::HelloAsso->value);
        $appli->setSynchronizedAt();
        $appli->setExternalId($importObj->getId());
        //
        $paiement = new Paiement();
        $paiement->setAppliMaitre($appli);
        //$paiement->setMoyen($importObj->getPaymentMeans()->value);
        $paiement->setMontant($importObj->getAmount());

        /* #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'installments')]
          private ?self $parent = null;

          #[ORM\Column(type: Types::DATETIME_MUTABLE)]
          private ?\DateTimeInterface $date = null;

          $paiement->setEtat($importObj->getState());

          #[ORM\Column(length: 255, nullable: true)]
          private ?string $recu = getPaymentReceiptUrl */

        $paiement->setPayeur($this->convertPayer($importObj->get));
    }

    public function convertPayer(Payer $payer): Contact {
        
    }
}
