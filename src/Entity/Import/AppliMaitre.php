<?php

namespace App\Entity\Import;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final class AppliMaitre implements ImportApplicationInterface {

    #[ORM\Column(length: 10)]
    private ?string $appli = null;

    public function getNom(): ?string {
        return $this->appli;
    }

    public function setNom(string $appli): self {
        $this->appli = $appli;
        return $this;
    }
    public function __toString(): string {
        return (string) $this->getNom();
    }
}
