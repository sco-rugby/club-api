<?php

namespace App\Model\Import;

use App\Service\Import\ImportServiceInterface;
use App\Service\Import\Ovale\ImportAffilie;
use App\Service\Import\Ovale\ImportLicence;
use App\Service\Import\Ovale\ImportPassVolontaire;
use Doctrine\ORM\EntityManagerInterface;

enum RapportOvale: string implements ImportInterface {

    case OVALE2001 = 'OVALE2-001 : Liste des licences';
    case OVALE2004 = 'OVALE2-004 : Liste des affiliés';
    case OVALE2045 = 'OVALE2-045 : Liste des clubs engagés dans compétition';
    case OVALE2046 = 'OVALE2-046 : Agenda des rencontres';
    case OVALE2047 = 'OVALE2-047 : Calendrier de la compétion';
    case OVALE2050 = 'OVALE2-050 : Pass Volontaires';

    public function createService(EntityManagerInterface $em): ImportServiceInterface {
        $service = match ($this) {
            RapportOvale::OVALE2001 => ImportLicence::class,
            RapportOvale::OVALE2004 => ImportAffilie::class,
            RapportOvale::OVALE2050 => ImportPassVolontaire::class,
        };
        return new $service($em);
    }

    public function getDescription(): string {
        return match ($this) {
            RapportOvale::OVALE2001 => 'Liste des licences de la structure.<br />Plusieurs lignes pour une personne possédant plusieurs licences',
            RapportOvale::OVALE2004 => 'Liste des personnes ayant eu au moins une licence dans la structure.<br />Une seule ligne par personne, quel que soit son nombre de licence',
            RapportOvale::OVALE2045 => 'Liste des clubs engagés dans compétition',
            RapportOvale::OVALE2046 => 'Agenda des rencontres d\'une compétition',
            RapportOvale::OVALE2047 => 'Calendrier de la compétion',
            RapportOvale::OVALE2050 => 'Extraction des Pass Volontaires',
        };
    }

    public function getMapping(): array {
        return match ($this) {
            RapportOvale::OVALE2001 => ['ligue_code', 'ligue_nom', 'club_code', 'club_nom', 'cd', 'saison', 'licence', 'date_première_affiliation', 'nom', 'prenom', 'sexe', 'nationalite', 'annee_naissance', 'date_naissance', 'ville_naissance', 'age', 'classe_age', 'qualite', 'autorisation_ffr', 'autorisation_tiers', '1ere_ligne', 'lca', 'dat', 'diplomes', 'type_contrat', 'qualite_date_début', 'qualite_date_fin', 'email', 'telephone_dom', 'telephone_pro', 'telephone_port', 'geocodage_', 'voie', 'localite', 'lieu-dit', 'immeuble', 'complement', 'code_postal', 'bassin'],
            RapportOvale::OVALE2004 => ['saison', 'ligue_code', 'ligue_nom', 'club_code', 'club_nom', 'cd','licence', 'date_première_affiliation','nom', 'prenom', 'nationalite', 'sexe', 'rue' ,'ville','code_postal','pays', 'telephone_dom', 'telephone_pro', 'telephone_port', 'email', 'data_perso_ffr', 'data_perso_tiers','date_naissance', 'ville naissance', 'qualites', 'nom_tuteur_1', 'prenom_tuteur_1', 'fonction_tuteur_1', 'nom_tuteur_2', 'prenom_tuteur_2', 'fonction_tuteur_2', 'bassin'],
            RapportOvale::OVALE2045 => ['code,competition,phase,type,poule,equipe,code_structure'],
            RapportOvale::OVALE2046 => ['code', 'competition', 'phase', 'nom', 'type', 'date'],
            RapportOvale::OVALE2047 => ['competition,phase,poule,journee,rencontre,date,heure,equipe_locale,equipe_visiteuse,score,club_terrain,terrain,terrain_cp,terrain_ville,terrain_etage,terrain_immeuble,terrain_voie,terrain_lieu_dit,terrain_pays'],
            RapportOvale::OVALE2050 => ['ligue_code', 'ligue_nom', 'cd', 'club_code', 'club_nom', 'nom', 'prenom', 'sexe', 'date_naissance', 'nationalite', 'voie', 'localite', 'lieu-dit', 'immeuble', 'complement', 'code_postal', 'email', 'telephone_dom', 'telephone_port', 'telephone_pro', 'telephone_public', 'saison', 'fonctions', 'bassin'],
        };
    }
}
