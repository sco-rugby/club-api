<?php

namespace App\Repository\Affilie;

use App\Entity\Affilie\AffilieSection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\SaisonManagedRepositoryInterface;
use App\Model\SaisonInterface;

/**
 * @extends ServiceEntityRepository<AffilieSection>
 *
 * @method AffilieSection|null find($id, $lockMode = null, $lockVersion = null)
 * @method AffilieSection|null findOneBy(array $criteria, array $orderBy = null)
 * @method AffilieSection[]    findAll()
 * @method AffilieSection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffilieSectionRepository extends ServiceEntityRepository implements SaisonManagedRepositoryInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, AffilieSection::class);
    }

    public function save(AffilieSection $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AffilieSection $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function deleteSaison(SaisonInterface $saison, bool $flush = false) {
        $nb = $this->createQueryBuilder('s')
                ->delete()
                ->where('s.saison = :saison')
                ->setParameter('saison', $saison->getId())
                ->getQuery()
                ->execute();
        if ($flush) {
            $this->getEntityManager()->flush();
        }
        return $nb;
    }

    public function findBySaison(SaisonInterface $saison): array {
        return $this->createQueryBuilder('s')
                        ->where('s.saison = :saison')
                        ->setParameter('saison', $saison->getId())
                        ->orderBy('g.licence', 'ASC')
                        ->getQuery()
                        ->getResult();
    }
}
