<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use App\Entity\Club\Club;
use App\Entity\Contact\Organisation;
use App\Entity\Contact\OrganisationExterne;
use App\Entity\Import\AppliMaitre;
use App\Model\AppliExterne;

/**
 * @author Antoine BOUET
 */
final class ClubFixture extends AbstractDataFixture implements FixtureGroupInterface {

    public function load(ObjectManager $manager) {
        $appli = new AppliMaitre();
        $appli->setNom(AppliExterne::Ovale->value);
        $date = new \DateTimeImmutable();
        foreach ($this->loadDataFile() as $data) {
            if ('club' == strtolower($data['type'])) {
                $organisation = new Club();
                $organisation->setClubId($data['id']);
            } else {
                $organisation = new Organisation();
            }

            $organisation->setNom($data['nom']);
            $organisation->getDateTime()->setCreatedAt($date);
            $organisation->getDateTime()->setUpdatedAt($date);
            // Adresse
            $organisation->getAdresse()->setAdresse($data['adresse']);
            $organisation->getAdresse()->setCodePostal($data['code_postal']);
            $organisation->getAdresse()->setVille($data['ville']);
            $organisation->getAdresse()->setPays('FR');
            // Applis Externes
            $appliExterne = new OrganisationExterne();
            $appliExterne->setAppliMaitre($appli);
            $appliExterne->setExternalId($data['id']);
            $appliExterne->setSynchronizedAt($date);
            $organisation->addApplisExterne($appliExterne);
            $manager->persist($appliExterne);
            //
            $manager->persist($organisation);
        }
        $manager->flush();
    }

    public static function getGroups(): array {
        return ['affilie', 'club'];
    }
}
