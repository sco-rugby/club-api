<?php

namespace App\Service\Import\Ovale;

use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Club\Club;
use App\Entity\Affilie\Affilie;
use App\Entity\Affilie\AffilieSection;
use App\Entity\Affilie\Licence;
use App\Entity\Affilie\Qualite;
use App\Entity\Club\Section;
use App\Manager\Affilie\AffilieManager;
use App\Model\Import\RapportOvale;
use App\Event\ImportLicenceEvent;
use ScoRugby\TraitementBundle\Exception\FailedExecutionException;
use ScoRugby\CoreBundle\Event\ExceptionEvent;

//TODO : activer transaction
final class ImportLicence extends AbstractImportLicence {

    public const JOB = 'licence:import';

    ############
    # Execution du service
    ############

    public function init(): void {
        try {
            parent::init();
            $this->fichier->setType(RapportOvale::OVALE2001);
        } catch (\Exception $e) {
            //$this->dispatcher->dispatch(new ExceptionEvent($e), ImportLicenceEvent::ERROR);
            throw new FailedExecutionException($e->getMessage());
        }
        //
        $this->event = new ImportLicenceEvent($this->fichier);
        $this->dispatcher->dispatch($this->event, ImportLicenceEvent::INIT);
    }

    public function import(mixed $data): void {
        try {
            parent::import($data);
            $worksheet = $this->spreadsheet->getActiveSheet();
            if ($this->hasLogger()) {
                $this->logger->info('Ouvrir ' . $worksheet->getTitle());
            }
            $rows = $this->readSpreadsheet($this->fichier->getType(), $worksheet);
        } catch (\Exception $e) {
            $this->setRollback();
            if ($this->hasLogger()) {
                $this->getLogger()->debug($e->getFile() . ':' . $e->getMessage());
            }
            //$this->dispatcher->dispatch(new ExceptionEvent($e), ImportLicenceEvent::ERROR);
            throw new FailedExecutionException($e->getMessage());
        }
        //
        $result = true;
        try {
            $this->fichier->setNbLignes(count($rows));
            $this->dispatcher->dispatch($this->event, ImportLicenceEvent::START);
            $this->startCommand(count($rows));
            foreach ($rows as $no => $data) {
                if (!$this->isAffilieCreated(intval($data['licence']))) {
                    $affilie = $this->createAffilie($data);
                } else {
                    $affilie = $this->findAffilie(intval($data['licence']));
                }
                $saison = $this->managers['saison']->convertir($data['saison']);
                $this->fichier->addSaison($saison);
                // correspondance
                $qualite = $this->convertirQualite($data['qualite']);
                $section = $this->convertirSection($data['qualite'], $data['classe_age']);

                $this->src->setExternalId($data['licence']);
                try {
                    if ($this->hasLogger()) {
                        $trmnt['saison'] = (string) $saison;
                        $trmnt['licence'] = $affilie->getId();
                        $trmnt['affilie'] = (string) $affilie;
                        $trmnt['origine'] = $data['qualite'];
                        $trmnt['qualite'] = $qualite;
                        $trmnt['section'] = $section;
                        $this->logger->info('ligne ' . $no + 1, $trmnt);
                    }
                    // création
                    if (!$this->isLicenceCreated($affilie, $saison, $qualite)) {
                        $licence = $this->createLicence($affilie, $saison, $qualite, $data);
                    }
                    if (null !== $section && !$this->isSectionAffilieCreated($affilie, $saison, $section)) {
                        $section = $this->createSectionAffilie($affilie, $saison, $section, $data);
                    }
                    $this->affilies->set($affilie->getId(), $affilie);
                    $this->managers['affilie']->setResource($affilie);
                    $this->managers['affilie']->update();
                } catch (\Exception $ex) {
                    //$this->setFailure($ex->getMessage() . ';' . implode(';', $data));
                    $result = false;
                    throw new FailedExecutionException($ex->getMessage());
                }
                //
                $this->lecture++;
                $this->nextStep();
            }
            $this->createCompteRendu($result);
        } catch (FailedExecutionException $e) {
            //$this->dispatcher->dispatch(new ExceptionEvent($e), ImportLicenceEvent::ERROR);
            throw new FailedExecutionException($e->getMessage());
        } catch (\Exception $e) {
            //$this->dispatcher->dispatch(new ExceptionEvent($e), ImportLicenceEvent::ERROR);
            throw new FailedExecutionException($e->getMessage());
        } finally {
            $this->setRollback();
        }
        $this->dispatcher->dispatch($this->event, ImportLicenceEvent::IMPORTED);
    }

    ############
    # Méthodes spécifiques
    ############

    protected function convertirSection(string $qualite, ?string $classeAge): ?string {
        if (null === $classeAge) {
            return null;
        }
        if ($this->hasLogger()) {
            $this->logger->debug(__METHOD__, ['qualite' => $qualite, 'classe_age' => $classeAge]);
        }
        switch (strtoupper($qualite)) {
            case 'VET':
            case 'RLO':
                return 'RLO';
            case 'RLSP':
                return 'RLSP';
        }
        return match ($classeAge) {
            'M+18', 'M+19' => 'M+18',
            'F+18' => 'F+18',
            'F-18' => 'F-18',
            'M-17', 'M-18', 'M-19' => 'M-19',
            'M-16', 'M-15' => 'M-16',
            'F-15' => 'F-15',
            'F-13', 'M-13', 'M-14' => 'U14',
            'F-12', 'M-12', 'F-11', 'M-11' => 'U12',
            'F-10', 'M-10', 'M-9', 'F-9' => 'U10',
            'F-8', 'M-8', 'M-7' => 'U8',
            'F-6', 'M-6' => 'U6'
        };
    }
}
