<?php

namespace App\Repository\Affilie;

use App\Entity\Affilie\Qualite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use ScoRugby\CoreBundle\Entity\SluggifyInterface;
use ScoRugby\CoreBundle\Repository\SlugRepositoryInterface;

/**
 * @extends ServiceEntityRepository<Qualite>
 *
 * @method Qualite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Qualite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Qualite[]    findAll()
 * @method Qualite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QualiteRepository extends ServiceEntityRepository implements SlugRepositoryInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Qualite::class);
    }

    public function save(Qualite $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Qualite $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findBySlug(string $slug): ?SluggifyInterface {
        return $this->findBy(['slug' => $slug]);
    }
}
