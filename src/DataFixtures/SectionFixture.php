<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use App\Entity\Club\Section;

/**
 * @author Antoine BOUET
 */
final class SectionFixture extends AbstractDataFixture implements FixtureGroupInterface {

    public function load(ObjectManager $manager) {
        $liste = [];
        foreach ($this->loadDataFile() as $data) {
            $section = new Section();
            $section->setId($data['id']);
            if (!empty($data['parent_id'])) {
                $section->setParent($liste[$data['parent_id']]);
            }
            $section->setLibelle($data['libelle']);
            if (!empty($data['slug'])) {
                $section->getSlug()->setString($data['slug']);
            } else {
                $section->getSlug()->setString($data['libelle']);
            }
            if (null != $data['description']) {
                $section->setDescription($data['description']);
            }
            $section->setCouleur($data['couleur']);
            if (null != $data['sexe']) {
                $section->setSexe($data['sexe']);
            }
            if (null != $data['age_debut']) {
                $section->setAgeDebut($data['age_debut']);
            }
            if (null != $data['age_fin']) {
                $section->setAgeFin($data['age_fin']);
            }
            $manager->persist($section);
            $liste[$data['id']] = $section;
        }
        $manager->flush();
    }

    public static function getGroups(): array {
        return ['affilie', 'club'];
    }
}
