<?php

namespace App\Entity;

use App\Model\Saison as BaseSaison;
use App\Entity\Competition\Agenda;
use App\Entity\Affilie\AffilieSection;
use App\Entity\Affilie\DoubleLicence;
use App\Entity\Affilie\Adhesion;
use App\Entity\Affilie\TarifLicence;
use App\Entity\Club\FonctionContact;
use App\Entity\Competition\Poule;
use App\Repository\SaisonRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\Affilie\Licence;
use ScoRugby\CoreBundle\Model\ManagedResourceInterface;
use ScoRugby\CoreBundle\Entity\EntityInterface;

#[ORM\Entity(repositoryClass: SaisonRepository::class)]
class Saison extends BaseSaison implements EntityInterface, ManagedResourceInterface {

    #[ORM\Id]
    
    #[ORM\GeneratedValue(strategy: "NONE")]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    protected ?\DateTimeInterface $debut = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    protected ?\DateTimeInterface $fin = null;
    protected ?string $libelle = null;

    #[ORM\OneToMany(mappedBy: 'saison', targetEntity: Licence::class, orphanRemoval: true)]
    private Collection $licences;

    #[ORM\OneToMany(mappedBy: 'saison', targetEntity: FonctionContact::class, orphanRemoval: true)]
    private Collection $fonctions;

    #[ORM\OneToMany(mappedBy: 'saison', targetEntity: AffilieSection::class, orphanRemoval: true)]
    private Collection $affilieSections;

    #[ORM\OneToMany(mappedBy: 'saison', targetEntity: DoubleLicence::class)]
    private Collection $doublesLicences;

    #[ORM\OneToMany(mappedBy: 'saison', targetEntity: Adhesion::class)]
    private Collection $adhesions;

    /**
     * @var Collection<int, TarifLicence>
     */
    #[ORM\OneToMany(targetEntity: TarifLicence::class, mappedBy: 'saison', orphanRemoval: true)]
    private Collection $tarifs;

    public function __construct(?int $annee = null) {
        parent::__construct($annee);
        $this->licences = new ArrayCollection();
        $this->fonctions = new ArrayCollection();
        $this->affilieSections = new ArrayCollection();
        $this->doublesLicences = new ArrayCollection();
        $this->tarifs = new ArrayCollection();
        $this->adhesions = new ArrayCollection();
        $this->agenda = new ArrayCollection();
        $this->poules = new ArrayCollection();
    }

    /**
     * @return Collection<int, Licence>
     */
    public function getLicences(): Collection {
        return $this->licences;
    }

    public function addLicence(Licence $licence): self {
        if (!$this->licences->contains($licence)) {
            $this->licences->add($licence);
            $licence->setSaison($this);
        }

        return $this;
    }

    public function removeLicence(Licence $licence): self {
        if ($this->licences->removeElement($licence)) {
            // set the owning side to null (unless already changed)
            if ($licence->getSaison() === $this) {
                $licence->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, FonctionSection>
     */
    public function getFonctions(): Collection {
        return $this->fonctions;
    }

    public function addFonctions(FonctionContact $fonction): self {
        if (!$this->fonctions->contains($fonction)) {
            $this->fonctions->add($fonction);
            $fonction->setSaison($this);
        }

        return $this;
    }

    public function removeFonctions(FonctionContact $fonction): self {
        if ($this->fonctions->removeElement($fonction)) {
            // set the owning side to null (unless already changed)
            if ($fonction->getSaison() === $this) {
                $fonction->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AffilieSection>
     */
    public function getAffilieSections(): Collection {
        return $this->affilieSections;
    }

    public function addAffilieSection(AffilieSection $affilieSection): self {
        if (!$this->affilieSections->contains($affilieSection)) {
            $this->affilieSections->add($affilieSection);
            $affilieSection->setSaison($this);
        }

        return $this;
    }

    public function removeAffilieSection(AffilieSection $affilieSection): self {
        if ($this->affilieSections->removeElement($affilieSection)) {
            // set the owning side to null (unless already changed)
            if ($affilieSection->getSaison() === $this) {
                $affilieSection->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, DoubleLicence>
     */
    public function getDoublesLicences(): Collection {
        return $this->doublesLicences;
    }

    public function addDoublesLicence(DoubleLicence $doublesLicence): self {
        if (!$this->doublesLicences->contains($doublesLicence)) {
            $this->doublesLicences->add($doublesLicence);
            $doublesLicence->setSaison($this);
        }

        return $this;
    }

    public function removeDoublesLicence(DoubleLicence $doublesLicence): self {
        if ($this->doublesLicences->removeElement($doublesLicence)) {
            // set the owning side to null (unless already changed)
            if ($doublesLicence->getSaison() === $this) {
                $doublesLicence->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Tarif>
     */
    public function getTarifs(): Collection {
        return $this->tarifs;
    }

    public function addTarif(Tarif $tarif): self {
        if (!$this->tarifs->contains($tarif)) {
            $this->tarifs->add($tarif);
            $tarif->setSaison($this);
        }

        return $this;
    }

    public function removeTarif(Tarif $tarif): self {
        if ($this->tarifs->removeElement($tarif)) {
            // set the owning side to null (unless already changed)
            if ($tarif->getSaison() === $this) {
                $tarif->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Paiement>
     */
    public function getPaiements(): Collection {
        return $this->paiements;
    }

    public function addPaiement(Paiement $paiement): self {
        if (!$this->paiements->contains($paiement)) {
            $this->paiements->add($paiement);
            $paiement->setSaison($this);
        }

        return $this;
    }

    public function removePaiement(Paiement $paiement): self {
        if ($this->paiements->removeElement($paiement)) {
            // set the owning side to null (unless already changed)
            if ($paiement->getSaison() === $this) {
                $paiement->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Agenda>
     */
    public function getAgenda(): Collection {
        return $this->agenda;
    }

    public function addAgenda(Agenda $agenda): self {
        if (!$this->agenda->contains($agenda)) {
            $this->agenda->add($agenda);
            $agenda->setSaison($this);
        }

        return $this;
    }

    public function removeAgenda(Agenda $agenda): self {
        if ($this->agenda->removeElement($agenda)) {
            // set the owning side to null (unless already changed)
            if ($agenda->getSaison() === $this) {
                $agenda->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Poule>
     */
    public function getPoules(): Collection {
        return $this->poules;
    }

    public function addPoule(Poule $poule): self {
        if (!$this->poules->contains($poule)) {
            $this->poules->add($poule);
            $poule->setSaison($this);
        }

        return $this;
    }

    public function removePoule(Poule $poule): self {
        if ($this->poules->removeElement($poule)) {
            // set the owning side to null (unless already changed)
            if ($poule->getSaison() === $this) {
                $poule->setSaison(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TarifLicence>
     */
    public function getTarifLicences(): Collection {
        return $this->tarifLicences;
    }

    public function addTarifLicence(TarifLicence $tarifLicence): static {
        if (!$this->tarifLicences->contains($tarifLicence)) {
            $this->tarifLicences->add($tarifLicence);
            $tarifLicence->setSaison($this);
        }

        return $this;
    }

    public function removeTarifLicence(TarifLicence $tarifLicence): static {
        if ($this->tarifLicences->removeElement($tarifLicence)) {
            // set the owning side to null (unless already changed)
            if ($tarifLicence->getSaison() === $this) {
                $tarifLicence->setSaison(null);
            }
        }

        return $this;
    }
}
