<?php

namespace App\Entity\Contact;

use Doctrine\ORM\Mapping as ORM;
use ScoRugby\CoreBundle\Entity\EntityInterface;

#[ORM\Entity]
#[ORM\Table(name: "contact_email")]
class ContactEmail implements EntityInterface {

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Column(length: "100")]
    protected ?string $email = null;

    #[ORM\Embedded(class: TypeMoyenContact::class, columnPrefix: "email_")]
    protected TypeMoyenContact $type;

    #[ORM\ManyToOne(inversedBy: "emails")]
    protected ?Contact $contact = null;

    public function __construct() {
        $this->type = new TypeMoyenContact();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getContact(): ?Contact {
        return $this->contact;
    }

    public function setContact(?Contact $contact): self {
        $this->contact = $contact;

        return $this;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    public function getType(): TypeMoyenContact {
        return $this->type;
    }
}
