<?php

namespace App\Entity\Datawarehouse\Effectif;

use App\Repository\Datawarehouse\Effectif\SectionGenreRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Club\Section as SectionClub;
use App\Entity\Saison;

#[ORM\Entity(repositoryClass: SectionGenreRepository::class)]
#[ORM\Table(name: "dwh_section_genre")]
#[ORM\UniqueConstraint(name: 'unq_section_genre', columns: ["saison_id", "section_id", "genre"])]
class SectionGenre {

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Saison $saison = null;

    #[ORM\ManyToOne(cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?SectionClub $section = null;

    #[ORM\Column]
    private ?string $genre = null;

    #[ORM\Embedded(class: StatsEffectifGenre::class)]
    protected StatsEffectif $actuel;

    #[ORM\Embedded(class: StatsEffectifGenre::class)]
    protected StatsEffectif $debut;

    #[ORM\Embedded(class: StatsEffectifGenre::class)]
    protected StatsEffectif $encours;

    #[ORM\Embedded(class: StatsEffectifGenre::class)]
    protected StatsEffectif $arret;

    public function __construct() {
        $this->actuel = new StatsEffectifGenre();
        $this->debut = new StatsEffectifGenre();
        $this->encours = new StatsEffectifGenre();
        $this->arret = new StatsEffectifGenre();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getSaison(): ?Saison {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self {
        $this->saison = $saison;

        return $this;
    }

    public function getSection(): ?SectionClub {
        return $this->section;
    }

    public function setSection(?SectionClub $section): self {
        $this->section = $section;

        return $this;
    }

    public function getGenre(): ?string {
        return $this->genre;
    }

    public function setGenre(string $genre): self {
        $this->genre = $genre;
        return $this;
    }

    public function getActuel(): StatsEffectif {
        return $this->actuel;
    }

    public function getDebut(): StatsEffectif {
        return $this->debut;
    }

    public function getEnCours(): StatsEffectif {
        return $this->encours;
    }

    public function getArret(): StatsEffectif {
        return $this->arret;
    }
}
