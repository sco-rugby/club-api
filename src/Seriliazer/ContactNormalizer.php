<?php

namespace App\Seriliazer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\String\UnicodeString;
use Symfony\Component\String\Slugger\AsciiSlugger;
use App\Model\Contact\ContactInterface;
use App\Entity\Contact\Contact as ContactEntity;
use App\Model\Contact\Contact as ContactModel;

/**
 * Description of ContactNormalizer
 *
 * @author Antoine BOUET
 */
class ContactNormalizer implements NormalizerInterface {

    public function normalize(mixed $object, ?string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null {
        if (empty($context)) {
            $context[] = 'nom';
            $context[] = 'prenom';
        }
        $result = [];
        if (in_array('nom', $context)) {
            $result['nom'] = $this->normalizeNom($object->getNom());
        }
        if (in_array('prenom', $context)) {
            $result['prenom'] = $this->normalizePrenom($object->getPrenom());
        }
        return $result;
    }

    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool {
        return $data instanceof ContactInterface;
    }

    public function getSupportedTypes(?string $format): array {
        return [
            ContactInterface::class => true,
            ContactEntity::class => true,
            ContactModel::class => true,
        ];
    }

    public function canonize(ContactInterface $contact): string {
        $result['prenom'] = '';
        $result['nom'] = '';
        if (null != $contact->getNom()) {
            $result['nom'] = $this->normalizeNom($contact->getNom());
        }
        if (null != $contact->getPrenom()) {
            $result['prenom'] = $this->normalizePrenom($contact->getPrenom());
        }
        $str = (new AsciiSlugger())->slug($result['prenom'] . ' ' . $result['nom'], ' ');
        return (new UnicodeString($str))
                        ->folded()
                        ->upper()
                        ->trimStart()
                        ->trimEnd()
                        ->toString();
    }

    /**
     * Normalisation du nom pour recherche
     */
    private function normalizeNom(string $nom): string {
        return (string) (new UnicodeString($nom))
                        ->trim()
                        ->folded()
                        ->title(true)
                        ->replace('De ', 'de ')
                        ->replace("D'", "d'");
    }

    /**
     * Normalisation du prénom pour recherche
     */
    private function normalizePrenom(string $prenom): string {
        return (string) (new UnicodeString($prenom))
                        ->trim()
                        ->folded()
                        ->title(true)
                        ->replace(' ', '-');
    }
}
