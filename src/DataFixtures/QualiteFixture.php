<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use App\Entity\Affilie\Qualite;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;

/**
 * @author Antoine BOUET
 */
final class QualiteFixture extends AbstractDataFixture implements FixtureGroupInterface {

    public function load(ObjectManager $manager) {
        foreach ($this->loadDataFile() as $data) {
            $qualite = new Qualite();
            $qualite->setId($data['id']);
            $qualite->setLibelle($data['libelle']);
            if (!empty($data['slug'])) {
                $qualite->getSlug()->setString($data['slug']);
            } else {
                $qualite->getSlug()->setString($data['libelle']);
            }
            if (null != $data['description']) {
                $qualite->setDescription($data['description']);
            }
            if (null != $data['groupe']) {
                $qualite->setGroupe($data['groupe']);
            }
            $manager->persist($qualite);
        }
        $manager->flush();
    }

    public static function getGroups(): array {
        return ['affilie'];
    }
}
