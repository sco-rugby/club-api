<?php

namespace App\Entity\Affilie;

use App\Entity\Club\Section;
use App\Entity\Saison;
use App\Repository\Affilie\TarifLicenceRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Tarif;

#[ORM\Entity(repositoryClass: TarifLicenceRepository::class)]
class TarifLicence extends Tarif {

    #[ORM\Column]
    private ?int $partClub = null;

    #[ORM\ManyToOne(inversedBy: 'tarifs')]
    private ?Saison $saison = null;

    #[ORM\ManyToOne(inversedBy: 'tarifs')]
    private ?Section $section = null;

    public function getId(): ?int {
        return $this->id;
    }

    public function getPartClub(): ?int {
        return $this->partClub;
    }

    public function setPartClub(int $partClub): static {
        $this->partClub = $partClub;

        return $this;
    }

    public function getSaison(): ?Saison {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): static {
        $this->saison = $saison;

        return $this;
    }

    public function getSection(): ?Section {
        return $this->section;
    }

    public function setSection(?Section $section): static {
        $this->section = $section;

        return $this;
    }
}
