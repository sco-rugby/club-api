<?php

namespace App\Model;

class Saison implements SaisonInterface, \Stringable {

    protected ?int $id = null;
    protected ?\DateTimeInterface $debut = null;
    protected ?\DateTimeInterface $fin = null;
    protected ?string $libelle = null;

    public function __construct(?int $annee = null) {
        if (!is_null($annee)) {
            $this->setId($annee);
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;
        $this->setDebut(new \DateTime($id . '-07-01'));
        $fin = $this->getDebut()->add(new \DateInterval('P1Y'));
        $fin->sub(new \DateInterval('P1D'));
        $this->setFin($fin);
        return $this;
    }

    public function getLibelle(): ?string {
        if (is_null($this->libelle) & !is_null($this->debut) & !is_null($this->fin)) {
            $this->libelle = $this->getDebut()->format('Y') . '-' . $this->getFin()->format('Y');
        }
        return $this->libelle;
    }

    public function getDebut(): ?\DateTimeInterface {
        return $this->debut;
    }

    public function setDebut(\DateTimeInterface $debut): self {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeInterface {
        return $this->fin;
    }

    public function setFin(\DateTimeInterface $fin): self {
        $this->fin = $fin;

        return $this;
    }

    public function __toString() {
        return (string) $this->getLibelle();
    }
}
