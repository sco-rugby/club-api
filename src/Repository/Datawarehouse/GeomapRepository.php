<?php

namespace App\Repository\Datawarehouse;

use App\Model\SaisonInterface;
use App\Entity\Datawarehouse\Geomap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\SaisonManagedRepositoryInterface;

/**
 * @extends ServiceEntityRepository<Geomap>
 *
 * @method Geomap|null find($id, $lockMode = null, $lockVersion = null)
 * @method Geomap|null findOneBy(array $criteria, array $orderBy = null)
 * @method Geomap[]    findAll()
 * @method Geomap[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeomapRepository extends ServiceEntityRepository implements SaisonManagedRepositoryInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Geomap::class);
    }

    public function save(Geomap $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Geomap $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function deleteSaison(SaisonInterface $saison, bool $flush = false) {
        $nb = $this->createQueryBuilder('g')
                ->delete()
                ->where('g.saison = :saison')
                ->setParameter('saison', $saison->getId())
                ->getQuery()
                ->execute();
        if ($flush) {
            $this->getEntityManager()->flush();
        }
        return $nb;
    }

    public function findBySaison(SaisonInterface $saison): array {
        return $this->createQueryBuilder('g')
                        ->where('g.saison = :saison')
                        ->setParameter('saison', $saison->getId())
                        ->orderBy('g.licence', 'ASC')
                        ->getQuery()
                        ->getResult();
    }
}
