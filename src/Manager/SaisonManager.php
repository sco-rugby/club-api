<?php

namespace App\Manager;

use ScoRugby\CoreBundle\Manager\AbstractDispatchingManager;
use ScoRugby\CoreBundle\Model\ManagedResourceInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Saison;
use App\Repository\SaisonRepository;
use App\Model\Affilie\AffilieInterface;
use ScoRugby\CoreBundle\Exception\Resource\ResourceNotFoundException;

final class SaisonManager extends AbstractDispatchingManager {

    private ?int $annee = null;

    public function __construct(SaisonRepository $repository, private ContainerBagInterface $params, ?EventDispatcherInterface $dispatcher = null, ?ValidatorInterface $validator = null, ?FormFactoryInterface $formFactory = null) {
        parent::__construct($repository, $dispatcher, $validator, $formFactory);
    }

    public function convertir($annee): Saison {
        if (is_string($annee)) {
            $annee = substr($annee, 0, 4);
        }
        if (preg_match("/\D/", $annee)) {
            throw new \InvalidArgumentException(sprintf('L\'année doit être de type "integer". (%s) "%s" fourni', gettype($annee), $annee));
        }
        try {
            $this->get(intval($annee));
        } catch (ResourceNotFoundException $e) {
            $this->create(intval($annee));
        }
        return $this->getResource();
    }

    public function getAnnee(): int {
        if (null === $this->annee) {
            $this->annee = (new \DateTime())->format('Y');
        }
        return $this->annee;
    }

    public function setAnnee(int $annee) {
        $this->annee = $annee;
    }

    public function getDateDebut(): ?\DateTimeInterface {
        $date = new \DateTime();
        list($j, $m) = explode('/', $this->getParam('debut'));
        $date->setDate($this->getAnnee(), $m, $j);
        return $date;
    }

    public function getDateFin(): ?\DateTimeInterface {
        return $this->getDateDebut()
                        ->add(new \DateInterval('P1Y'))
                        ->sub(new \DateInterval('P1D'));
    }

    public function determinerSaison(\DateTimeInterface $date): int {
        $annee = (int) $date->format('Y');
        $mois = (int) $date->format('m');
        list($j, $m) = explode('/', $this->getParam('debut'));
        if ($mois < intval($m)) {
            $annee--;
        }
        return $annee;
    }

    public function create($properties): ManagedResourceInterface {
        if (is_int($properties)) {
            $annee = $properties;
        } else {
            $annee = $this->getAnnee();
        }
        $this->setAnnee($annee);
        $saison = new Saison($annee);
        $saison->setDebut($this->getDateDebut());
        $saison->setFin($this->getDateFin());
        $this->repository->save($saison, true);
        $this->setResource($saison);
        $this->dispatchEvent('saison.create');
        return $saison;
    }

    public function calculerAge(AffilieInterface $affilie): int {
        $this->checkForAction('calculerAge');
        if (null == $affilie->getDateNaissance()) {
            $date = new \DateTime(sprintf('%s-%s-01', substr($affilie->getId(), 3, 2), substr($affilie->getId(), 0, 4)));
        } else {
            $date = $affilie->getDateNaissance();
        }
        $interval = $date->diff($this->getResource()->getDebut());
        return (int) $interval->format('%y');
    }

    public function enCours(): bool {
        /*    $debut = new \DateTime();
          list($j, $m) = explode('/', $this->getParam('debut'));
          $debut->setDate($this->getAnnee(), $m, $j);
          $debut = new \DateTime();
          list($j, $m) = explode('/', $this->getParam('debut'));
          $debut->setDate($this->getAnnee(), $m, $j); */
    }

    private function getParams() {
        return $this->params->get('saison');
    }

    private function getParam($key) {
        $container = $this->getParams();
        if (array_key_exists($key, $container)) {
            return $container[$key];
        }
        return false;
    }
}
