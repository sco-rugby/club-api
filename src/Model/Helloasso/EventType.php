<?php

namespace App\Model\Helloasso;

enum EventType: string {

    case Order = 'Order';
    case Payment = 'Payment';
    case Form = 'Form';
}
