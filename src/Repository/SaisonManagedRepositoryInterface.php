<?php

namespace App\Repository;

use App\Model\SaisonInterface;

interface SaisonManagedRepositoryInterface {

    public function findBySaison(SaisonInterface $saison): array;

    public function deleteSaison(SaisonInterface $saison, bool $flush = false);
}
