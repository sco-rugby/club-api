<?php

namespace App\Service\Import\Helloasso;

use App\Service\Import\AbstractApiImportService;
use App\Entity\Affilie\Adhesion;
use App\Manager\Affilie\AdhesionManager;
use Helloasso\HelloassoClientFactory;

/**
 * Description of ImportAdhesion
 *
 * @author Antoine BOUET
 */
final class ImportAdhesion extends AbstractApiImportService {

    const APP_MAITRE = 'helloasso';

    ############
    # Execution du service
    ############

    public function init(): void {
        $helloassoClient = HelloassoClientFactory::create(
                        'hello_asso_id',
                        'hello_asso_secret',
                        'hello_asso_organization_slug',
                        true # sandbox
        );
    }

    public function import(mixed $data): void {
        
    }

    public function shutdown(): void {
        
    }
}
