<?php

namespace App\Service\Import;

use App\Service\Import\FileImportInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use ScoRugby\TraitementBundle\Exception\FailedExecutionException;
use ScoRugby\TraitementBundle\Exception\NoExecutionException;
use ScoRugby\CoreBundle\Exception\InvalidParameterException;
use App\Model\Import\ImportInterface;
use App\Model\Import\Fichier;

abstract class AbstractFileImportService extends AbstractImportService implements FileImportInterface {

    protected Fichier $fichier;
    protected Spreadsheet $spreadsheet;

    public function init(): void {
        $this->fichier = new Fichier();
        parent::init();
    }

    public function import(mixed $data): void {
        if (empty($data) && !$this->isLoaded()) {
            throw new InvalidParameterException('No file provided');
        } elseif (!empty($data) && !$this->isLoaded()) {
            if (!is_object($data)) {
                throw new InvalidParameterException(sprintf('import() parameter must be a %s instance. %s given', \SplFileInfo::class, get_type($data)));
            } elseif (!($data instanceof \SplFileInfo)) {
                throw new InvalidParameterException(sprintf('import() parameter must be a %s instance. %s given', \SplFileInfo::class, get_class($data)));
            }
            $this->load($data);
        }
    }

    public function load(\SplFileInfo $fichier): void {
        try {
            if (!$fichier->isReadable()) {
                throw new NoExecutionException(sprintf('Le fichier %s ne peut pas être lu', $fichier));
            }
            if ($this->hasLogger()) {
                $this->getLogger()->notice(sprintf('Chargement du fichier %s', $fichier));
            }
            $this->fichier->setFichier($fichier);
            $reader = IOFactory::createReaderForFile($fichier->getPathname());
            $reader->setReadDataOnly(true);
            $this->spreadsheet = $reader->load($fichier);
        } catch (\Exception $e) {
            throw new FailedExecutionException($e->getMessage());
        }
    }

    public function isLoaded(): bool {
        if (!isset($this->spreadsheet)) {
            return false;
        }
        return (bool) (null != $this->spreadsheet);
    }

    public function shutdown(): void {
        parent::shutdown();
        // supprimer le fichier
        if ($this->isLoaded()) {
            if ($this->hasLogger()) {
                $this->logger->info('supression de ' . $this->fichier);
            }
            //$this->filesystem->remove($this->fichier); //TODO reactiver
        }
    }

    ############
    # Méthodes spécifiques
    ############

    /**
     * 
     * @param ImportInterface $rapport
     * @param Worksheet $worksheet
     * @return array
     */
    protected function readSpreadsheet(ImportInterface $rapport, Worksheet $worksheet): array {
        $endCol = Coordinate::stringFromColumnIndex(count($rapport->getMapping()));
        $rows = [];
        foreach ($worksheet->getRowIterator() as $row) {
            if ($row->isEmpty(1) || 1 == $row->getRowIndex()) {
                continue;
            }
            $values = [];
            foreach ($row->getCellIterator('A', $endCol) as $cell) {
                //$cell->setDataType(DataType::TYPE_STRING);
                $values[] = $cell->getValue();
            }
            $rows[] = array_combine($rapport->getMapping(), $values);
        }
        return $rows;
    }

    protected function toBoolean($bool): bool {
        return match (strtolower($bool)) {
            'oui' => true,
            'non' => false,
            'true' => true,
            'false' => false,
            '1' => true,
            '0' => false,
            'vrai' => true,
            'faux' => false,
            default => false
        };
    }

    protected function convertirDate($date): ?\DateTime {
        if ($date == $ts = intval($date)) {
            $dateTime = new \DateTime();
            $dateTime->setTimestamp($ts);
        } else {
            $dateTime = \DateTime::createFromFormat('d/m/Y', $date);
        }
        if (false === $dateTime) {
            if ($this->hasLogger()) {
                $this->logger->debug(self::class . "::convertirDate() : conversion impossible pour " . $date);
            }
            return null;
        }
        return $dateTime;
    }
}
