<?php

namespace App\Repository\Club;

use App\Entity\Club\FonctionContact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Model\SaisonInterface;
use App\Entity\Club\Fonction;
use App\Entity\Club\Section;
use App\Repository\SaisonManagedRepositoryInterface;

/**
 * @extends ServiceEntityRepository<FonctionContact>
 *
 * @method FonctionContact|null find($id, $lockMode = null, $lockVersion = null)
 * @method FonctionContact|null findOneBy(array $criteria, array $orderBy = null)
 * @method FonctionContact[]    findAll()
 * @method FonctionContact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FonctionContactRepository extends ServiceEntityRepository implements SaisonManagedRepositoryInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, FonctionContact::class);
    }

    public function save(FonctionContact $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(FonctionContact $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getNbBenevoles(SaisonInterface $saison): int {
        return $this->createQueryBuilder('f')->select('COUNT(distinct f.contact)')
                        ->andWhere('f.saison <= :saison')
                        ->andWhere("f.fonction = 'BENE'")
                        ->setParameter('saison', $saison->getId())
                        ->getQuery()
                        ->getSingleScalarResult();
    }

    public function findBenevolesBySaison(SaisonInterface $saison): array {
        $fonction = new Fonction();
        $fonction->setId('BENE');
        return $this->findByFonctionAndSaison($fonction, $saison);
    }

    public function findByFonctionAndSaison(Fonction $fonction, SaisonInterface $saison): array {
        return $this->findBy(['fonction' => $fonction->getId(), 'saison' => $saison->getId()]);
    }

    public function findBySaison(SaisonInterface $saison): array {
        return $this->findBy(['saison' => $saison->getId()]);
    }

    public function deleteSaison(SaisonInterface $saison, bool $flush = false) {
        $nb = $this->createQueryBuilder('fc')
                ->delete()
                ->where('fc.saison = :saison')
                ->setParameter('saison', $saison->getId())
                ->getQuery()
                ->execute();
        if ($flush) {
            $this->getEntityManager()->flush();
        }
        return $nb;
    }

    public function getNbEducateurs(Section $section, SaisonInterface $saison, bool $parent = false): int {
        if (false == $parent) {
            return $this->createQueryBuilder('fs')
                            ->select('COUNT(distinct fs.contact)')
                            ->andWhere('fs.section = :section')
                            ->andWhere('fs.saison = :saison')
                            ->andWhere("fs.fonction = 'EDU'")
                            ->setParameter('section', $section->getId())
                            ->setParameter('saison', $saison->getId())
                            ->getQuery()
                            ->getSingleScalarResult();
        } else {
            return $this->createQueryBuilder('fs')
                            ->select('COUNT(distinct fs.contact)')
                            ->join('fs.section', 's')
                            ->andWhere('s.parent = :section')
                            ->andWhere('fs.saison = :saison')
                            ->andWhere("fs.fonction = 'EDU'")
                            ->setParameter('section', $section->getId())
                            ->setParameter('saison', $saison->getId())
                            ->getQuery()
                            ->getSingleScalarResult();
        }
    }
}
