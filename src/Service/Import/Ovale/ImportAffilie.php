<?php

namespace App\Service\Import\Ovale;

use App\Model\Import\RapportOvale;
use App\Event\ImportLicenceEvent;
use ScoRugby\TraitementBundle\Exception\FailedExecutionException;
use ScoRugby\CoreBundle\Event\ExceptionEvent;
use App\Entity\Affilie\Affilie;
use App\Entity\Affilie\Tuteur;
use App\Entity\Club\Section;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\String\UnicodeString;

//TODO : activer transaction
final class ImportAffilie extends AbstractImportLicence {

    public const JOB = 'affilie:import';

    private ArrayCollection $tuteurs;

    ############
    # Execution du service
    ############

    public function init(): void {
        try {
            parent::init();
            // sections
            foreach ($this->em->getRepository(Section::class)->findAll() as $section) {
                $this->sections->set($section->getId(), $section);
            }
            // tuteurs
            $this->tuteurs = new ArrayCollection();
            foreach ($this->em->getRepository(Tuteur::class)->findAll() as $tuteur) {
                $this->setTuteurList($tuteur);
            }
            $this->creations['tuteur'] = 0;
            //
            $this->fichier->setType(RapportOvale::OVALE2004);
        } catch (\Exception $e) {
            //$this->dispatcher->dispatch(new ExceptionEvent($e), ImportLicenceEvent::ERROR);
            throw new FailedExecutionException($e->getMessage());
        }
        //
        $this->event = new ImportLicenceEvent($this->fichier);
        $this->dispatcher->dispatch($this->event, ImportLicenceEvent::INIT);
    }

    public function import(mixed $data): void {
        try {
            parent::import($data);
            $worksheet = $this->spreadsheet->getActiveSheet();
            if ($this->hasLogger()) {
                $this->logger->info('Ouvrir ' . $worksheet->getTitle());
            }
            $rows = $this->readSpreadsheet($this->fichier->getType(), $worksheet);
        } catch (\Exception $e) {
            $this->setRollback();
            if ($this->hasLogger()) {
                $this->getLogger()->debug($e->getFile() . ':' . $e->getMessage());
            }
            //$this->dispatcher->dispatch(new ExceptionEvent($e), ImportLicenceEvent::ERROR);
            throw new FailedExecutionException($e->getMessage());
        }
        //
        $result = true;
        try {
            $this->fichier->setNbLignes(count($rows));
            $this->dispatcher->dispatch($this->event, ImportLicenceEvent::START);
            $this->startCommand(count($rows));
            foreach ($rows as $no => $data) {
                $saison = $this->managers['saison']->convertir($data['saison']);
                $this->fichier->addSaison($saison);
                $this->src->setExternalId($data['licence']);
                try {
                    if (!$this->isAffilieCreated(intval($data['licence']))) {
                        $affilie = $this->createAffilie($data);
                        /* if ($this->hasTuteur($data)) {
                          if (!$this->isTuteurCreated($affilie, $data["nom_tuteur_1"], $data["prenom_tuteur_1"], $data['code_postal'])) {
                          $tuteurData = $data;
                          $tuteurData['nom'] = $data["nom_tuteur_1"];
                          $tuteurData['prenom'] = $data["prenom_tuteur_1"];
                          $tuteurData['type'] = $data["fonction_tuteur_1"];
                          $this->addTuteur($affilie, $tuteurData);
                          }
                          if (!$this->isTuteurCreated($affilie, $data["nom_tuteur_2"], $data["prenom_tuteur_2"], $data['code_postal'])) {
                          $tuteurData = $data;
                          $tuteurData['nom'] = $data["nom_tuteur_2"];
                          $tuteurData['prenom'] = $data["prenom_tuteur_2"];
                          $tuteurData['type'] = $data["fonction_tuteur_2"];
                          $this->addTuteur($affilie, $tuteurData);
                          }
                          } */
                    } else {
                        $affilie = $this->findAffilie(intval($data['licence']));
                    }
                    forEach (explode(',', $data['qualites']) as $elmQualite) {
                        // correspondance
                        $qualite = $this->convertirQualite($elmQualite);
                        $section = $this->convertirSection($affilie, $qualite, $this->convertirGenre($data['sexe']));
                        if ($this->hasLogger()) {
                            $trmnt['saison'] = (string) $saison;
                            $trmnt['licence'] = $affilie->getId();
                            $trmnt['affilie'] = (string) $affilie;
                            $trmnt['origine'] = $elmQualite;
                            $trmnt['qualite'] = $qualite;
                            $trmnt['section'] = $section;
                            $this->logger->info('ligne ' . $no, $trmnt);
                        }
                        // création
                        if (!$this->isLicenceCreated($affilie, $saison, $qualite)) {
                            $licence = $this->createLicence($affilie, $saison, $elmQualite, $data);
                        }
                        if (null !== $section && !$this->isSectionAffilieCreated($affilie, $saison, $section)) {
                            $section = $this->createSectionAffilie($affilie, $saison, $section, $data);
                        }
                    }
                    $this->affilies->set($affilie->getId(), $affilie);
                    $this->managers['affilie']->setResource($affilie);
                    $this->managers['affilie']->update();
                } catch (\Exception $ex) {
                    $result = false;
                    throw new FailedExecutionException($ex->getMessage());
                }
                //
                $this->lecture++;
                $this->nextStep();
            }
            if ($this->creations['tuteur'] > 0) {
                $this->addCompteRendu($this->creations['tuteur'] . '  tuteurs créés');
            }
            $this->createCompteRendu($result);
        } catch (FailedExecutionException $e) {
            //$this->dispatcher->dispatch(new ExceptionEvent($e), ImportLicenceEvent::ERROR);
            throw new FailedExecutionException($e->getMessage());
        } catch (\Exception $e) {
            //$this->dispatcher->dispatch(new ExceptionEvent($e), ImportLicenceEvent::ERROR);
            throw new FailedExecutionException($e->getMessage());
        } finally {
            $this->setRollback();
        }
        $this->dispatcher->dispatch($this->event, ImportLicenceEvent::IMPORTED);
    }

    ############
    # Méthodes spécifiques
    ############

    protected function convertirSection(Affilie $affilie, string $qualite, string $genre): ?string {
        $age = $this->managers['saison']->calculerAge($affilie);
        if ($this->hasLogger()) {
            $this->logger->debug(__METHOD__, ['age' => $age, 'qualite' => $qualite, 'genre' => $genre]);
        }
        switch (strtoupper($qualite)) {
            case 'VET':
            case 'RLO':
                return 'RLO';
            case 'RLSP':
                return 'RLSP';
        }
        if (!in_array($qualite, ['A', 'B', 'C'])) {
            return null;
        }
        $age = $this->managers['saison']->calculerAge($affilie);
        //$exprBuilder = Criteria::expr();
        //$expr = $exprBuilder->eq('canonizedNom', $canonizedNom);
        //return $this->sections->matching(new Criteria($expr));
        if ($age >= 18) {
            return $genre . '+18';
        } elseif ($age < 6) {
            return 'U6';
        } elseif ($age < 8) {
            return 'U8';
        } elseif ($age < 10) {
            return 'U10';
        } elseif ($age < 12) {
            return 'U12';
        } elseif ($age <= 15 & 'F' == $genre) {
            return 'F-15';
        } elseif ($age < 14) {
            return 'U14';
        } elseif ($age < 16) {
            if ('F' == $genre) {
                return 'F-18';
            }
            return 'M-16';
        } elseif ($age < 18) {
            if ('F' == $genre) {
                return 'F-18';
            }
            return 'M-19';
        }
    }

    /**
     * TUTEUR
     */
    protected function hasTuteur(array $data): bool {
        if (!empty($data["nom_tuteur_1"])) {
            return true;
        } elseif (!empty($data["prenom_tuteur_1"])) {
            return true;
        } elseif (!empty($data["nom_tuteur_2"])) {
            return true;
        } elseif (!empty($data["prenom_tuteur_2"])) {
            return true;
        }
        return false;
    }

    protected function isTuteurCreated(Affilie $affilie, string $nom, string $prenom, string $cp): bool {
        var_dump(array_keys($this->tuteurs->toArray()));
        return null !== $this->findTuteur($affilie, $nom, $prenom, $cp);
    }

    protected function findTuteur(Affilie $affilie, string $nom, string $prenom, string $cp): ?Tuteur {
        $parentId = $this->findContact($nom, $prenom, null, $cp, true);
        $key = sprintf("%s/%s", $affilie->getId(), $parentId);
        return $this->tuteurs->get($key);
    }

    protected function addTuteur(Affilie &$affilie, array $data) {
        if (null == $data['nom'] | null == $data['prenom'] | null == $data['code_postal']) {
            return;
        }
        if ($this->isContactCreated($data['nom'], $data['prenom'], null, $data['code_postal'])) {
            $contact = $this->findContact($data['nom'], $data['prenom'], null, $data['code_postal']);
        } else {
            $data['id'] = uniqid();
            $data['sexe'] = '';
            $contact = $this->createContact($data, ['contact']);
        }
        //normalize fonction tuteur
        $str = (new AsciiSlugger())->slug($data['type']);
        $str = (new UnicodeString($str))
                ->title(true)
                ->upper()
                ->toString();
        $type = match ($str) {
            'MERE' => 'M',
            'PERE' => 'P',
            default => 'A',
        };
        $tuteur = (new Tuteur())
                ->setTuteur($contact)
                ->setType($type);
        $affilie->addTuteur($tuteur);
        $tuteur->setAffilie($affilie);
        $this->setTuteurList($tuteur);
        $this->creations['tuteur']++;
    }

    protected function setTuteurList(Tuteur $tuteur) {
        $key = sprintf("%s/%s", $tuteur->getAffilie()->getId(), $tuteur->getTuteur()->getId());
        $this->tuteurs->set($key, $tuteur);
    }
}
