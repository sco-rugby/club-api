<?php

namespace App\Entity\Import;

/**
 *
 * @author Antoine BOUET
 */
interface ImportApplicationInterface {

    public function getNom(): ?string;

    public function setNom(string $appli): self;
}
