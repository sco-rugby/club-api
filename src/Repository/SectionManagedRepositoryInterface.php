<?php

namespace App\Repository;

use App\Entity\Club\Section;

interface SectionManagedRepositoryInterface {

    public function findBySection(Section $section): array;
}
