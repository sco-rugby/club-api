<?php

namespace App\Entity\Affilie;

use App\Entity\Paiement;
use App\Entity\Saison;
use App\Repository\Affilie\AdhesionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Import\ImportableInterface;
use App\Entity\Import\ImportApplicationInterface;
use App\Entity\Import\SingleSourceImport;

#[ORM\Entity(repositoryClass: AdhesionRepository::class)]
class Adhesion implements ImportableInterface {

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'adhesions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Affilie $affilie = null;

    #[ORM\ManyToOne(inversedBy: 'adhesions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Saison $saison = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?TarifLicence $tarif = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $commentaire = null;

    #[ORM\Column(length: 1)]
    private ?string $status = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $due_at = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $completed_at = null;

    #[ORM\Embedded(class: SingleSourceImport::class)]
    private SingleSourceImport $appli;

    /**
     * @var Collection<int, Paiement>
     */
    #[ORM\OneToMany(targetEntity: Paiement::class, mappedBy: 'adhesion')]
    private Collection $paiements;

    public function __construct() {
        $this->paiements = new ArrayCollection();
        $this->appli = new SingleSourceImport();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getAffilie(): ?Affilie {
        return $this->affilie;
    }

    public function setAffilie(?Affilie $affilie): self {
        $this->affilie = $affilie;

        return $this;
    }

    public function getSaison(): ?Saison {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self {
        $this->saison = $saison;

        return $this;
    }

    public function getTarif(): ?TarifLicence {
        return $this->tarif;
    }

    public function setTarif(?TarifLicence $tarif): self {
        $this->tarif = $tarif;

        return $this;
    }

    public function getCommentaire(): ?string {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getStatus(): ?string {
        return $this->status;
    }

    public function setStatus(string $status): self {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDueAt(): ?\DateTimeImmutable {
        return $this->due_at;
    }

    public function setDueAt(\DateTimeImmutable $due_at): self {
        $this->due_at = $due_at;

        return $this;
    }

    public function getCompletedAt(): ?\DateTimeImmutable {
        return $this->completed_at;
    }

    public function setCompletedAt(?\DateTimeImmutable $completed_at): self {
        $this->completed_at = $completed_at;

        return $this;
    }

    public function getAppliMaitre(): ?ImportApplicationInterface {
        return $this->appli;
    }

    public function setAppliMaitre(ImportApplicationInterface $appli): self {
        $this->appli = $appli;
        return $this;
    }

    /**
     * @return Collection<int, Paiement>
     */
    public function getPaiements(): Collection {
        return $this->paiements;
    }

    public function addPaiement(Paiement $paiement): static {
        if (!$this->paiements->contains($paiement)) {
            $this->paiements->add($paiement);
            $paiement->setAdhesion($this);
        }

        return $this;
    }

    public function removePaiement(Paiement $paiement): static {
        if ($this->paiements->removeElement($paiement)) {
            // set the owning side to null (unless already changed)
            if ($paiement->getAdhesion() === $this) {
                $paiement->setAdhesion(null);
            }
        }

        return $this;
    }
}
