<?php

namespace App\Manager\Contact;

use App\Entity\Contact\Adresse;
use App\Entity\Contact\Commune;
use App\Repository\Contact\CommuneRepository;
use App\Seriliazer\AdresseNormalizer;

/**
 * Description of AdresseManager
 *
 * @author Antoine BOUET
 */
class AdresseManager {

    public function __construct(private CommuneRepository $repository) {
        return;
    }

    public function findByVille(string $ville): ?Commune {
        return $this->repository->findByNom($ville);
    }

    public function findApproxNom(string $ville, string $cp): ?Commune {
        return $this->repository->findByNomEtCp($ville, $cp);
    }

    static public function normalize(Adresse &$adresse): void {
        $normalizer = new AdresseNormalizer();
        $adr = $normalizer->normalize($adresse, Adresse::class);
        //
        $adresse->setAdresse(null);
        $adresse->setComplement(null);
        $adresse->setCodePostal(null);
        $adresse->setVille(null);
        if (!empty($adr['adresse'])) {
            $adresse->setAdresse($adr['adresse']);
        }
        if (!empty($adr['complement'])) {
            $adresse->setComplement($adr['complement']);
        }
        if (!empty($adr['code_postal'])) {
            $adresse->setCodePostal($adr['code_postal']);
        }
        if (!empty($adr['ville'])) {
            $adresse->setVille($adr['ville']);
        }
    }
}
