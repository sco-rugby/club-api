<?php

namespace App\Webhook\Parser;

use Symfony\Component\HttpFoundation\ChainRequestMatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\RemoteEvent\RemoteEvent;
use Symfony\Component\Webhook\Client\AbstractRequestParser;
use Symfony\Component\Webhook\Exception\RejectWebhookException;
use Symfony\Component\HttpFoundation\RequestMatcher\HostRequestMatcher;
use Symfony\Component\HttpFoundation\RequestMatcher\IsJsonRequestMatcher;
use Symfony\Component\HttpFoundation\RequestMatcher\MethodRequestMatcher;
use Helloasso\Models\Event;

final class HelloassoRequestParser extends AbstractRequestParser {

    protected function getRequestMatcher(): RequestMatcherInterface {
        return new ChainRequestMatcher([
            new HostRequestMatcher('api.helloasso'),
            new IsJsonRequestMatcher(),
            new MethodRequestMatcher('POST'),
        ]);
    }

    protected function doParse(Request $request, string $secret): ?RemoteEvent {
        // Validate the request payload.
        if (!$request->getPayload()->has('eventType') || !$request->getPayload()->has('data')) {
            throw new RejectWebhookException(Response::HTTP_BAD_REQUEST, 'Request payload does not contain required fields.');
        }
        return new RemoteEvent(Event::class, $request->getPayload()->get('eventType'), $request->getPayload()->get('data'));
    }
}
