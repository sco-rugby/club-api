<?php

namespace App\Service\Import\Helloasso;

use App\Service\Import\AbstractApiImportService;
use Helloasso\Service\EventService;
use Helloasso\Enums\FormType;
use App\Model\AppliExterne;
use App\Entity\Evenement\Evenement;
use App\Manager\Evenement\EvenementManager;
use App\Entity\Affilie\Adhesion;
use App\Manager\Affilie\AdhesionManager;

/**
 * Description of ImportEvenement
 *
 * @author Antoine BOUET
 */
class ImportForm extends AbstractApiImportService {

    public function import(mixed $data): void {
        $eventService = new EventService($this->params->get('appli.helloasso.client_id'), $this->params->get('appli.helloasso.secret'), $this->params->get('appli.helloasso.slug'));
        $event = $eventService->decode($data);
        switch ($event->getEventType()) {
            case FormType::Event :
                $repository = $this->em->getRepository(Evenement::class);
                $manager = new EvenementManager($repository, $this->dispatcher);
                $event = $manager->getBySlug($event->getData()->getFormSlug());
                if (null == $event) {
                    $manager->create($event->getData());
                } else {
                    $manager->update($event->getData());
                }
                break;
            case FormType::Membership :
                $repository = $this->em->getRepository(Adhesion::class);
                $manager = new AdhesionManager($repository, $this->dispatcher);
                $adhesion = $manager->getBySlug($event->getData()->getFormSlug());
                if (null == $adhesion) {
                    $manager->create($event->getData());
                } else {
                    $manager->update($event->getData());
                }
                break;
        }
    }
}
