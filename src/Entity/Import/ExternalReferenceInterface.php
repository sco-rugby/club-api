<?php

namespace App\Entity\Import;

/**
 *
 * @author Antoine BOUET
 */
interface ExternalReferenceInterface {

    public function getAppliMaitre(): ?ImportApplicationInterface;

    public function setAppliMaitre(ImportApplicationInterface $appli): self;

    public function getExternalId(): ?string;

    public function setExternalId(string $id): self;

    public function getSynchronizedAt(): ?\DateTimeInterface;

    public function setSynchronizedAt(?\DateTimeInterface $date = null): self;
}
