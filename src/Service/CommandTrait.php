<?php

namespace App\Service;

trait CommandTrait {

    public function nextStep(): void {
        if ($this->hasProgressBar()) {
            $this->progressBar->advance();
        }
    }

    public function hasProgressBar(): bool {
        return !is_null($this->progressBar);
    }

    public function startCommand(int $steps = null): void {
        if ($this->hasProgressBar() && null !== $steps) {
            $this->progressBar->start();
            $this->progressBar->setMaxSteps($steps);
        }
    }

    public function endCommand(): void {
        if ($this->hasProgressBar()) {
            $this->progressBar->finish();
        }
    }
}
