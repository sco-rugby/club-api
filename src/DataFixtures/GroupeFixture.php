<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use App\Entity\Contact\Groupe;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * @author Antoine BOUET
 */
final class GroupeFixture extends AbstractDataFixture implements FixtureGroupInterface {

    public function load(ObjectManager $manager) {
        foreach ($this->loadDataFile() as $data) {
            $groupe = new Groupe();
            $groupe->setId($data['id']);
            $groupe->setLibelle($data['libelle']);
            $groupe->getSlug()->setString((new AsciiSlugger())->slug($data['libelle']));
            if (null != $data['description']) {
                $groupe->setDescription($data['description']);
            }
            if (null != $data['mailing_list']) {
                $groupe->setEmailList($data['mailing_list']);
            }
            $manager->persist($groupe);
        }
        $manager->flush();
    }

    public static function getGroups(): array {
        return ['club'];
    }
}
