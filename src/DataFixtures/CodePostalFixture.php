<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use App\Entity\Contact\CodePostal;
use App\Entity\Contact\Commune;

/**
 * Description of CommuneFixture
 *
 * @author Antoine BOUET
 */
class CodePostalFixture extends AbstractDataFixture implements DependentFixtureInterface, FixtureGroupInterface {

    public function load(ObjectManager $manager) {
        foreach ($this->loadDataFile() as $data) {
            $commune = $manager->getRepository(Commune::class)->find($data['commune_id']);
            $cp = new CodePostal();
            $cp->setCommune($commune);
            $cp->setCodePostal($data['codepostal']);
            $manager->persist($cp);
        }
        $manager->flush();
    }

    public function getDependencies() {
        return [
            CommuneFixture::class,
        ];
    }

    public static function getGroups(): array {
        return ['adresse', 'contact'];
    }
}
