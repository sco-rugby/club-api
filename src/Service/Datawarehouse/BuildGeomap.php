<?php

namespace App\Service\Datawarehouse;

use App\Model\SaisonInterface;
use App\Entity\Datawarehouse\Geomap;
use App\Entity\Datawarehouse\Licence\LicenceSaison;
use ScoRugby\TraitementBundle\Model\TraitementInterface;
use ScoRugby\TraitementBundle\Exception\FailedExecutionException;
use ScoRugby\TraitementBundle\Exception\NoExecutionException;

/**
 * Construit les stats geo par commune, par saison, par section
 *    Geomap
 */
final class BuildGeomap extends AbstractBuildService {

    public const JOB = 'datawarehouse:build:geomap';

    private $communes = [],
            $stats = [];

    public function purge(SaisonInterface $saison): int {
        if ($this->hasLogger()) {
            $this->getLogger()->info(sprintf('supprimer les "geomap" de la saison %s', $saison));
        }
//      $nb = $this->em->getRepository(Geomap::class, 'datawarehouse')->deleteSaison($saison, false);
        $nb = $this->em->getRepository(Geomap::class)->deleteSaison($saison, false);
        if ($this->hasLogger()) {
            $this->getLogger()->debug($nb . ' données supprimées');
        }
        return $nb;
    }

    /**
     * Traitement de construction
     * 
     * @return void
     * @throws FailedExecutionException
     */
    public function build(): void {
        try {
            $this->buildStats();
            $this->buildData();
        } catch (NoExecutionException $ex) {
            //$this->setSuccess($ex->getMessage());
        } catch (\Exception $e) {
            $this->setRollback();
            throw new FailedExecutionException($e->getMessage());
        }
    }

    public function buildStats(): void {
        // Déterminer champs du SQL
        /* $rsm = new ResultSetMapping();
          $rsm->addScalarResult('licence_id', 'licence_id');
          $rsm->addScalarResult('debut', 'debut', 'boolean');
          $rsm->addScalarResult('arret', 'arret', 'boolean');
          $rsm->addScalarResult('joueur', 'joueur', 'boolean');
          $rsm->addScalarResult('educateur', 'educateur', 'boolean');
          $rsm->addScalarResult('arbitre', 'arbitre', 'boolean');
          $rsm->addScalarResult('dirigeant', 'dirigeant', 'boolean');
          $rsm->addScalarResult('section_id', 'section_id');
          $rsm->addScalarResult('parent_id', 'parent_id');
          $rsm->addScalarResult('commune_id', 'commune_id');
          $rsm->addScalarResult('nom', 'nom');
          $rsm->addScalarResult('latitude', 'latitude');
          $rsm->addScalarResult('longitude', 'longitude');
          // Requête SQL pour optimiser temps de réponse (toutes les entités n'ont pas de relations et ne doivent pas en avoir)
          $query = $this->em->createNativeQuery('SELECT ls.licence_id, ls.debut, ls.arret, ls.joueur, ls.educateur, ls.arbitre, ls.dirigeant, afs.section_id, s.parent_id, v.id commune_id, v.nom, v.latitude, v.longitude FROM dwh_licence_saison ls JOIN affilie_section afs ON afs.affilie_id=ls.licence_id AND afs.saison_id=ls.saison_id JOIN section s ON s.id=afs.section_id JOIN affilie a ON a.id=ls.licence_id JOIN contact c ON c.id=a.contact_id JOIN commune v ON v.id=c.commune_id WHERE ls.saison_id = :saison', $rsm);
          $query->setParameter('saison', $this->saison->getId());
          $licences = $query->getResult(); */
        $licences = $this->em->getRepository(LicenceSaison::class)->findGeomapDataBySaison($this->saison);
        //
        if (empty($licences)) {
            throw new NoExecutionException(sprintf('Aucune licence pour la saison %s', $this->saison));
        }
        //
        if ($this->hasLogger()) {
            $this->getLogger()->debug('Construire les stats');
            $this->getLogger()->debug(sprintf('    %s licences à traiter', count($licences)));
        }
        try {
            $this->startCommand(count($licences));
            //
            $affilies = [];
            foreach ($licences as $licence) {
                if (!key_exists($licence['commune_id'], $this->communes)) {
                    $this->communes[$licence['commune_id']]['nom'] = $licence['nom'];
                    $this->communes[$licence['commune_id']]['latitude'] = $licence['latitude'];
                    $this->communes[$licence['commune_id']]['longitude'] = $licence['longitude'];
                }
                // Aggréger au niveau club, si licence pas déja recencée
                if (!in_array($licence['licence_id'], $affilies)) {
                    $affilies[] = $licence['licence_id'];
                    $this->calculerStats($licence, 'club');
                }
                // Aggréger au niveau regroupement section, si regroupement section
                if (!is_null($licence['parent_id'])) {
                    $this->calculerStats($licence, $licence['parent_id']);
                }
                // Aggréger au niveau section
                $this->calculerStats($licence, $licence['section_id']);
                //
                $this->nextStep();
            }
        } catch (Exception $e) {
            if ($this->hasLogger()) {
                $this->getLogger()->error('buildStats : ' . $e->getMessage(), get_class($e));
            }
            throw $e;
        } finally {
            $this->endCommand();
        }
    }

    public function buildData(): void {
        if ($this->hasLogger()) {
            $this->getLogger()->debug('Enregistements des stats geomap');
            $this->getLogger()->debug(sprintf('   %s communes à créer', count($this->communes)));
        }
        try {
            $this->startCommand(count($this->communes));
            ksort($this->communes);
            foreach ($this->communes as $id => $commune) {
                forEach ($this->stats[$id] as $section => $data) {
                    $geomap = new Geomap();
                    $geomap->setSaison($this->saison);
                    $geomap->setCommune($id);
                    $geomap->setNom($commune['nom']);
                    $geomap->setLatitude($commune['latitude']);
                    $geomap->setLongitude($commune['longitude']);
                    if ('club' != $section) {
                        $geomap->setSection($section);
                    }
                    $this->setData($geomap, $data);
                }
                $this->nextStep();
            }
            $this->em->flush();
        } catch (Exception $e) {
            if ($this->hasLogger()) {
                $this->getLogger()->error('buildData : ' . $e->getMessage(), get_class($e));
            }
            throw $e;
        } finally {
            $this->endCommand();
        }
    }

    private function calculerStats($licence, $section): void {
        if (!key_exists($licence['commune_id'], $this->stats)) {
            $this->stats[$licence['commune_id']] = [];
        }
        if (!key_exists($section, $this->stats[$licence['commune_id']])) {
            $this->stats[$licence['commune_id']][$section]['nb'] = 0;
            $this->stats[$licence['commune_id']][$section]['joueurs'] = 0;
            $this->stats[$licence['commune_id']][$section]['joueuses'] = 0;
            $this->stats[$licence['commune_id']][$section]['educateurs'] = 0;
            $this->stats[$licence['commune_id']][$section]['debutants'] = 0;
            $this->stats[$licence['commune_id']][$section]['encours'] = 0;
            $this->stats[$licence['commune_id']][$section]['arrets'] = 0;
        }
        $agg = $this->stats[$licence['commune_id']][$section];
        $agg['nb']++;
        if (true === $licence['joueur'] && 1 == substr($licence['licence_id'], 6, 1)) {
            $agg['joueurs']++;
        } elseif (true === $licence['joueur'] && 2 == substr($licence['licence_id'], 6, 1)) {
            $agg['joueuses']++;
        }
        if (true === $licence['educateur']) {
            $agg['educateurs']++;
        }
        if (true === $licence['debut']) {
            $agg['debutants']++;
        }
        If (true == $licence['arret']) {
            $agg['arrets']++;
        }
        if (false === $licence['debut'] && false == $licence['arret']) {
            $agg['encours']++;
        }
        $this->stats[$licence['commune_id']][$section] = $agg;
    }

    private function setData(Geomap &$geomap, array $data): void {
        $geomap->setNb($data['nb']);
        $geomap->setPourcentage('0.00');
        $geomap->setJoueurs($data['joueurs']);
        $geomap->setPourcentageJoueurs('0.00');
        $geomap->setJoueuses($data['joueuses']);
        $geomap->setPourcentageJoueuses('0.00');
        $geomap->setEducateurs($data['educateurs']);
        $geomap->setPourcentageEducateurs('0.00');
        $geomap->setDebutants($data['debutants']);
        $geomap->setPourcentageDebutants('0.00');
        $geomap->setEncours($data['encours']);
        $geomap->setPourcentageEncours('0.00');
        $geomap->setArrets($data['arrets']);
        $geomap->setPourcentageArrets('0.00');
//        $this->em->getRepository(Geomap::class, 'datawarehouse')->save($geomap);
        $this->em->getRepository(Geomap::class)->save($geomap);
        if ($this->hasLogger()) {
            $this->getLogger()->debug('    Enregistrement');
        }
    }

    public function getTraitement(): ?TraitementInterface {
        
    }
}
