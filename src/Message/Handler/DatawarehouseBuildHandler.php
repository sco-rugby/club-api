<?php

namespace App\Message\Handler;

use App\Message\DatawarehouseBuild;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use App\Manager\SaisonManager;
use App\Entity\Saison;
use App\Service\Datawarehouse\BuildDatawarehouse;

#[AsMessageHandler]
final class DatawarehouseBuildHandler {

    public function __construct(private EntityManagerInterface $em, private ContainerBagInterface $params, private EventDispatcherInterface $dispatcher, private LoggerInterface $logger) {
        return;
    }

    public function __invoke(DatawarehouseBuild $message) {
        $saisonManager = new SaisonManager($this->em->getRepository(Saison::class), $this->params);
        $service = new BuildDatawarehouse($this->em, $saisonManager, $this->dispatcher, $this->logger);
        $service->init($message->getSaison());
        $service->build();
        $service->shutdown();
    }
}
