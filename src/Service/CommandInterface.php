<?php

namespace App\Service;

interface CommandInterface {

    public function start(int $steps = null): void;

    public function end(): void;

    public function nextStep(): void;

    public function hasProgressBar(): bool;
}
