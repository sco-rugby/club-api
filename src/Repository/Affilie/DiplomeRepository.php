<?php

namespace App\Repository\Affilie;

use App\Entity\Affilie\Diplome;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use ScoRugby\CoreBundle\Entity\SluggifyInterface;
use ScoRugby\CoreBundle\Repository\SlugRepositoryInterface;

/**
 * @extends ServiceEntityRepository<Diplome>
 *
 * @method Diplome|null find($id, $lockMode = null, $lockVersion = null)
 * @method Diplome|null findOneBy(array $criteria, array $orderBy = null)
 * @method Diplome[]    findAll()
 * @method Diplome[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiplomeRepository extends ServiceEntityRepository implements SlugRepositoryInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Diplome::class);
    }

    public function save(Diplome $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Diplome $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findBySlug(string $slug): ?SluggifyInterface {
        return $this->findBy(['slug' => $slug]);
    }
}
