<?php

namespace App\Entity\Import;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @author Antoine BOUET
 */
#[ORM\MappedSuperclass]
abstract class ExternalReference implements ExternalReferenceInterface {

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Embedded(class: AppliMaitre::class, columnPrefix: false)]
    private AppliMaitre $appli;

    #[ORM\Column(length: 15)]
    private ?string $externalId = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $synchronizedAt = null;

    public function __contruct() {
        $this->appli = new AppliMaitre();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getAppliMaitre(): ?ImportApplicationInterface {
        return $this->appli;
    }

    public function setAppliMaitre(ImportApplicationInterface $appli): self {
        $this->appli = $appli;
        return $this;
    }

    public function getExternalId(): ?string {
        return $this->externalId;
    }

    public function setExternalId(string $id): self {
        $this->externalId = $id;

        return $this;
    }

    public function getSynchronizedAt(): ?\DateTimeInterface {
        return $this->synchronizedAt;
    }

    public function setSynchronizedAt(?\DateTimeInterface $date = null): self {
        if (null === $date) {
            $this->synchronizedAt = new \DateTime();
        } else {
            $this->synchronizedAt = $date;
        }

        return $this;
    }

    public function __toString(): string {
        return (string) $this->getAppliMaitre() . '-' . $this->getExternalId();
    }
}
