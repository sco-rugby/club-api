<?php

namespace App\Service\Import\Ovale;

use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Club\Club;
use App\Entity\Affilie\Affilie;
use App\Entity\Affilie\AffilieSection;
use App\Entity\Affilie\Licence;
use App\Entity\Affilie\Qualite;
use App\Entity\Club\Section;
use App\Model\SaisonInterface;
use App\Manager\Affilie\AffilieManager;
use App\Event\ImportLicenceEvent;
use ScoRugby\TraitementBundle\Exception\FailedExecutionException;
use App\Exception\ContactException;

//TODO : activer transaction
abstract class AbstractImportLicence extends AbstractOvaleService {

    protected ArrayCollection $affilies;
    protected ArrayCollection $licences;
    protected ArrayCollection $affilieSections;
    protected ArrayCollection $sections;
    protected ArrayCollection $qualites;
    protected Club $club;
    protected ImportLicenceEvent $event;

    ############
    # Execution du service
    ############

    public function init(): void {
        $this->creations['affilie'] = 0;
        $this->creations['licence'] = 0;
        $this->creations['section'] = 0;
        $this->affilies = new ArrayCollection();
        $this->licences = new ArrayCollection();
        $this->sections = new ArrayCollection();
        $this->qualites = new ArrayCollection();
        $this->affilieSections = new ArrayCollection();
        parent::init();
        //
        $club = $this->params->get('club');
        $this->club = $this->em->getRepository(Club::class)->findOneBy(['club_id' => $club['code']]);
        foreach ($this->em->getRepository(Qualite::class)->findAll() as $qualite) {
            $this->qualites->set($qualite->getId(), $qualite);
        }
        $this->managers['affilie'] = new AffilieManager($this->em->getRepository(Affilie::class), $this->dispatcher, $this->validator);
        foreach ($this->managers['affilie']->findAll() as $affilie) {
            $this->affilies->set($affilie->getId(), $affilie);
        }
        //
        foreach ($this->em->getRepository(Licence::class)->findAll() as $licence) {
            $this->setLicenceList($licence);
        }
        foreach ($this->em->getRepository(AffilieSection::class)->findAll() as $section) {
            $this->sections->set($section->getSection()->getId(), $section->getSection());
            $this->setSectionAffilieList($section);
        }
    }

    protected function createCompteRendu(bool $result): void {
        if ($this->creations['contact'] > 0) {
            $this->addCompteRendu($this->creations['contact'] . ' contacts créés');
        }
        if ($this->creations['affilie'] > 0) {
            $this->addCompteRendu($this->creations['affilie'] . ' affiliés créés');
        }
        if ($this->creations['licence'] > 0) {
            $this->addCompteRendu($this->creations['licence'] . ' licences créées');
        }
        if ($this->creations['section'] > 0) {
            $this->addCompteRendu($this->creations['section'] . ' affiliés ajoutés aux sections');
        }
        if (false === $result) {
            throw new FailedExecutionException('Des erreurs sont survenues lors de l\'import.');
        }
        if ($this->hasCompteRendu()) {
            $msg[] = sprintf('%s lignes lues', $this->lecture);
            $msg = array_merge($msg, $this->getCompteRendu());
            $msg[] = sprintf('Fichier %s importé', $this->fichier->getFichier()->getFilename());
            $this->setCompteRendu($msg);
        } else {
            $this->addCompteRendu(sprintf('%s lignes lues : Aucune modification', $this->lecture));
            $this->addCompteRendu(sprintf('Fichier %s ignoré', $this->fichier->getFichier()->getFilename()));
        }
        $this->event->setCompteRendu($this->getCompteRendu());
    }

    public function shutdown(): void {
        unset($this->creations);
        unset($this->affilies);
        unset($this->licences);
        unset($this->sections);
        unset($this->qualites);
        unset($this->affilieSections);
        unset($this->contacts);
        unset($this->saisons);
        parent::shutdown();
        if (isset($this->event)) {
            //$this->dispatcher->dispatch($this->event, ImportLicenceEvent::SHUTDOWN);
        }
    }

    ############
    # Méthodes spécifiques
    ############

    public function getEvent(): ?ImportLicenceEvent {
        return $this->event;
    }

    protected function isMutation(?string $qualite): bool {
        return match (substr($qualite, 1)) {
            'M', 'MC' => true,
            default => false
        };
    }

    protected function isEnFormation(?string $qualite): bool {
        return match (substr($qualite, 2)) {
            'CF' => true,
            default => false
        };
    }

    /**
     * QUALITE
     */
    protected function convertirQualite(string $qualite): string {
        if ($this->hasLogger()) {
            $this->logger->debug(__METHOD__, [$qualite]);
        }
        $qualiteId = $qualite;
        if ('VET' == $qualite) {
            $qualiteId = 'RLO';
        } elseIf (in_array($qualite, ['EDE', 'EBF'])) {
            $qualiteId = 'EDU';
        } elseIf (true === $this->isMutation($qualite)) {
            $qualiteId = substr($qualite, 0, 1);
        }
        return $qualiteId;
    }

    protected function findQualite(string $qualite): Qualite {
        return $this->qualites->get($qualite);
    }

    /**
     * AFFILIE
     */
    protected function isAffilieCreated(int $licenceId) {
        return null !== $this->findAffilie($licenceId);
    }

    protected function findAffilie(int $licenceId): ?Affilie {
        return $this->affilies->get($licenceId);
    }

    protected function createAffilie(array $data): Affilie {
        if ($this->hasLogger()) {
            $this->logger->info("Création affilie " . $data['licence']);
            $this->logger->debug(__METHOD__, $data);
        }
        $dob = $this->convertirDate($data['date_naissance']);
        if (false !== $this->isContactCreated($data['nom'], $data['prenom'], $dob, $data['code_postal'])) {
            $data['id'] = $this->findContact($data['nom'], $data['prenom'], $dob, $data['code_postal'], true);
            $contact = $this->contacts->get($data['id']);
            if (null !== $contact['contact']) {
                $contact = $this->createContact($data, ['create' => false]);
            } else {
                throw new ContactException(sprintf('Le contact %d (%s %s) ne contient aucune donnée et ne peut être complété.', $data['id'], $data['nom'], $data['prenom']));
            }
        } else {
            $data['id'] = $data['licence'];
            $contact = $this->createContact($data);
        }
        $affilie = $this->managers['affilie']->createObject();
        $affilie->setId($data['licence'])
                ->setContact($contact)
                ->setDateNaissance($dob)
                ->setClub($this->club)
                ->setAppliMaitre($this->src);
        //
        $saison = null;
        $date = $this->convertirDate($data['date_première_affiliation']);
        /* if (null !== $date) {
          $saison = $this->managers['saison']->findByDate($date);
          if (null === $saison) {
          $annee = $this->managers['saison']->determinerSaison($date);
          $saison = $this->managers['saison']->convertir($annee);
          $this->setSaison($saison);
          }
          } */
        //$affilie->setPremiereAffiliation($saison);
        $this->creations['affilie']++;
        // Ajout à liste
        $this->affilies->set($affilie->getId(), $affilie);
        //
        return $affilie;
    }

    /**
     * LICENCE
     */
    protected function isLicenceCreated(Affilie $affilie, SaisonInterface $saison, string $qualite) {
        $key = sprintf("%s/%s/%s", $affilie->getId(), $saison->getId(), $qualite);
        return null !== $this->licences->get($key);
    }

    /**
     * Créer une licence pour un affilié, une saison
     * et l'ajouter à l'affilié
     * 
     * @param Affilie $affilie passage par réference
     * @param SaisonInterface $saison
     * @param string $qualite donnée brute, car besoin pour mutation et formation
     * @param array $data
     * @return Licence
     */
    protected function createLicence(Affilie &$affilie, SaisonInterface $saison, string $qualite, array $data): Licence {
        if ($this->hasLogger()) {
            $this->logger->info("   Création licence", ['licence' => $data['licence'], 'qualite' => $qualite]);
            $this->logger->debug(__METHOD__);
        }
        //
        $qual = $this->convertirQualite($qualite);
        $licence = (new Licence())
                ->setAffilie($affilie)
                ->setSaison($saison)
                ->setQualite($this->findQualite($qual))
                ->setMutation($this->isMutation($qualite))
                //->setPremiereLigne($this->toBoolean($data['1ere_ligne']))
                ->setEnFormation($this->isEnFormation($qualite))
                ->setDebut($saison->getDebut())
                ->setFin($saison->getFin());
        $affilie->addLicence($licence);
        $this->creations['licence']++;
        // Ajout à liste
        $this->setLicenceList($licence);
        //
        return $licence;
    }

    protected function setLicenceList(Licence $licence) {
        $key = sprintf("%s/%s/%s", $licence->getAffilie()->getId(), $licence->getSaison()->getId(), $licence->getQualite()->getId());
        $this->licences->set($key, $licence);
    }

    /**
     * SECTION
     */
    protected function createSectionAffilie(Affilie &$affilie, SaisonInterface $saison, string $section, array $data): AffilieSection {
        if ($this->hasLogger()) {
            $this->logger->info("   Création section affilié", ['licence' => $affilie->getId(), 'saison' => $saison->getId(), 'section' => $section]);
            $this->logger->debug(__METHOD__);
        }
        $sectionAffilie = (new AffilieSection())
                ->setAffilie($affilie)
                ->setSaison($saison)
                ->setSection($this->findSection($section));
        $affilie->addSection($sectionAffilie);
        $this->creations['section']++;
        // Ajout à liste
        $this->setSectionAffilieList($sectionAffilie);
        //
        return $sectionAffilie;
    }

    protected function findSection(string $id): Section {
        if (!$this->sections->containsKey($id)) {
            $this->sections->set($id, $this->em->getRepository(Section::class)->find($id));
        }
        return $this->sections->get($id);
    }

    protected function isSectionAffilieCreated(Affilie $affilie, SaisonInterface $saison, string $section): bool {
        $key = sprintf("%s/%s/%s", $affilie->getId(), $saison->getId(), $section);
        return null !== $this->affilieSections->get($key);
    }

    protected function setSectionAffilieList(AffilieSection $section) {
        $key = sprintf("%s/%s/%s", $section->getAffilie()->getId(), $section->getSaison()->getId(), $section->getSection()->getId());
        $this->affilieSections->set($key, $section);
    }
}
