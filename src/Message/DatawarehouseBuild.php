<?php

namespace App\Message;

use App\Model\SaisonInterface;

final class DatawarehouseBuild {

    public function __construct(private SaisonInterface $saison) {
        return;
    }

    public function getSaison(): SaisonInterface {
        return $this->saison;
    }
}
