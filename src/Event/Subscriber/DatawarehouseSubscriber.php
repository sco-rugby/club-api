<?php

namespace App\Event\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use ScoRugby\TraitementBundle\Manager\TraitementManager;

/**
 * Description of DatawarehouseSubscriber
 *
 * @author Antoine BOUET
 */
final class DatawarehouseSubscriber implements EventSubscriberInterface {

    public static function getSubscribedEvents() {
        return [];
//        return [
//            /* TraitementEvent::INIT => [
//              'onInit',
//              ], */
//            TraitementEvent::START => [
//                'onStart',
//            ],
//            /* TraitementEvent::END => [
//              'onEnd',
//              ], */
//            TraitementEvent::SUCCESS => [
//                'onSuccess',
//            ],
//            TraitementEvent::ERROR => [
//                'onError'
//            ],
//            TraitementEvent::FAILURE => [
//                'onFailure'
//            ],
//                /* TraitementEvent::EXCEPTION => [
//                  'onException'
//                  ], */
//        ];
    }
}
