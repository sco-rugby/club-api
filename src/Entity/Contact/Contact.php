<?php

namespace App\Entity\Contact;

use App\Repository\Contact\ContactRepository;
use App\Entity\Paiement;
use App\Model\Contact\Contact as BaseContact;
use App\Collection\CollectionFonction;
use App\Entity\Club\FonctionContact;
use App\Entity\Affilie\Affilie;
use App\Entity\Affilie\Tuteur;
use App\Entity\Evenement\Participant;
use App\Entity\Import\MultiSourceImportableInterface;
use App\Entity\Import\MultiSourceImportableTrait;
use App\Entity\Import\ExternalReferenceInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use ScoRugby\CoreBundle\Entity\EntityInterface;
use ScoRugby\CoreBundle\Model\ManagedResourceInterface;
use ScoRugby\CoreBundle\Model\GroupableInterface;
use ScoRugby\CoreBundle\Model\GeolocalisableInterface;
use ScoRugby\CoreBundle\Entity\DateTimeBlameableInterface;
use ScoRugby\CoreBundle\Entity\DateTimeBlameable;
use App\Seriliazer\ContactNormalizer;

#[ORM\Entity(repositoryClass: ContactRepository::class)]
class Contact extends BaseContact implements EntityInterface, DateTimeBlameableInterface, ManagedResourceInterface, MultiSourceImportableInterface, GeolocalisableInterface {

    use MultiSourceImportableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "IDENTITY")]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Column]
    protected bool $public = false;

    #[ORM\Column]
    protected bool $listeRouge = false;

    #[ORM\Column(length: 100)]
    protected ?string $nom = null;

    #[ORM\Column(length: 100)]
    protected ?string $prenom = null;

    #[ORM\Column(length: 200)]
    protected ?string $canonizedNom = null;

    #[ORM\Column(length: 1, nullable: true)]
    protected ?string $genre = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    protected ?string $note = null;

    #[ORM\Embedded(class: Adresse::class, columnPrefix: false)]
    protected Adresse $adresse;

    #[ORM\ManyToOne]
    protected ?Commune $commune = null;

    #[ORM\OneToMany(targetEntity: GroupeContact::class, mappedBy: "contact", orphanRemoval: "true")]
    protected Collection $groupes;

    #[ORM\ManyToMany(targetEntity: Organisation::class, mappedBy: "contacts", cascade: ['persist', 'remove'])]
    protected Collection $organisations;

    #[ORM\OneToMany(targetEntity: ContactEmail::class, mappedBy: "contact", orphanRemoval: "true", cascade: ['persist', 'remove'])]
    protected Collection $emails;

    #[ORM\OneToMany(targetEntity: Participant::class, mappedBy: "contact", orphanRemoval: "true")]
    protected Collection $participations;

    #[ORM\Embedded(DateTimeBlameable::class, columnPrefix: false)]
    protected DateTimeBlameable $datetime;

    #[ORM\OneToOne(inversedBy: 'contact', cascade: ['all'])]
    protected ?Affilie $affilie = null;

    /**
     * @var Collection<int, Affilie>
     */
    #[ORM\OneToMany(mappedBy: 'tuteur', targetEntity: Tuteur::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    protected Collection $enfants;

    /**
     * @var Collection<int, Paiement>
     */
    #[ORM\OneToMany(targetEntity: Paiement::class, mappedBy: 'payeur', orphanRemoval: true, cascade: ['remove'])]
    protected Collection $paiements;

    /**
     * @var Collection<int, FonctionContact>
     */
    #[ORM\OneToMany(mappedBy: 'contact', targetEntity: FonctionContact::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    protected Collection $fonctions;

    /**
     * @var Collection<int, ContactExterne>
     */
    #[ORM\OneToMany(mappedBy: 'contact', targetEntity: ContactExterne::class, cascade: ["persist", "remove"])]
    protected Collection $applisExternes;

    /**
     * @var Collection<int, ContactTelephone>
     */
    #[ORM\OneToMany(targetEntity: ContactTelephone::class, mappedBy: 'contact', cascade: ['persist', 'remove'])]
    private Collection $telephones;

    public function __construct() {
        parent::__construct();
        $this->groupes = new ArrayCollection();
        $this->organisations = new ArrayCollection();
        $this->emails = new ArrayCollection();
        $this->datetime = new DateTimeBlameable();
        $this->fonctions = new CollectionFonction();
        $this->applisExternes = new ArrayCollection();
        $this->paiements = new ArrayCollection();
        $this->participations = new ArrayCollection();
        $this->telephones = new ArrayCollection();
    }

    public function setPrenom(string $prenom): self {
        $this->prenom = $prenom;
        $result = (new ContactNormalizer())->normalize($this, self::class, ['prenom']);
        parent::setPrenom($result['prenom']);
        $this->setCanonizedNom();
        return $this;
    }

    public function setNom(string $nom): self {
        $this->nom = $nom;
        $result = (new ContactNormalizer())->normalize($this, self::class, ['nom']);
        parent::setNom($result['nom']);
        $this->setCanonizedNom();
        return $this;
    }

    public function getCanonizedNom(): ?string {
        return $this->canonizedNom;
    }

    private function setCanonizedNom(): void {
        $this->canonizedNom = (new ContactNormalizer())->canonize($this);
    }

    public function getDateTime(): DateTimeBlameable {
        return $this->datetime;
    }

    public function getCommune(): ?Commune {
        return $this->commune;
    }

    public function setCommune(?Commune $commune): self {
        $this->commune = $commune;

        return $this;
    }

    public function getLatitude(): ?string {
        if (null != $this->getCommune()) {
            return $this->getCommune()->getLatitude();
        }
        return null;
    }

    public function getLongitude(): ?string {
        if (null != $this->getCommune()) {
            return $this->getCommune()->getLongitude();
        }
        return null;
    }

    public function getCoordonnees(): ?string {
        if ($this->isGeolocalisable()) {
            return $this->getLongitude() . ',' . $this->getLatitude();
        } else {
            return null;
        }
    }

    public function isGeolocalisable(): bool {
        return (null !== $this->getLongitude() & null !== $this->getLatitude());
    }

    /**
     * @return Collection<int, Tuteur>
     */
    public function getEnfants(): Collection {
        return $this->enfants;
    }

    public function addtEnfant(Tuteur $tuteur): self {
        if (!$this->enfants->contains($tuteur)) {
            $this->enfants->add($tuteur);
        }

        return $this;
    }

    public function removetEnfant(Tuteur $tuteur): self {
        $this->enfants->removeElement($tuteur);

        return $this;
    }

    /**
     * @return Collection<int, Organisation>
     */
    public function getOrganisations(): Collection {
        return $this->organisations;
    }

    public function addOrganisation(Organisation $organisation): self {
        if (!$this->organisations->contains($organisation)) {
            $this->organisations->add($organisation);
        }

        return $this;
    }

    public function removeOrganisation(Organisation $organisation): self {
        $this->organisations->removeElement($organisation);

        return $this;
    }

    /**
     * @return Collection<int, ContactTelephone>
     */
    public function getEmails(): Collection {
        return $this->emails;
    }

    public function addEmail(ContactEmail $email): self {
        if (!$this->emails->contains($email)) {
            $this->emails->add($email);
            $email->setContact($this);
        }

        return $this;
    }

    public function removeEmail(ContactEmail $email): self {
        if ($this->emails->removeElement($email)) {
            // set the owning side to null (unless already changed)
            if ($email->getContact() === $this) {
                $email->setContact(null);
            }
        }

        return $this;
    }

    public function getAffilie(): ?Affilie {
        return $this->affilie;
    }

    public function setAffilie(?Affilie $affilie): static {
        $this->affilie = $affilie;

        return $this;
    }

    public function isAffilie(): bool {
        return ( null != $this->getAffilie());
    }

    public function isTuteur(): ?bool {
        if ($this->isAffilie()) {
            return (count($this->getAffilie()->getTuteurs()) > 0);
        }
        return false;
    }

    public function isPartenaire(): ?bool {
        return false;
    }

    /**
     * @return Collection<int, FonctionsClub>
     */
    public function getFonctions(): Collection {
        return $this->fonctions;
    }

    public function addFonctions(FonctionsContact $fonction): self {
        if (!$this->fonctions->contains($fonction)) {
            $this->fonctions->add($fonction);
            $fonction->setContact($this);
        }

        return $this;
    }

    public function removeFonctions(FonctionsContact $fonction): self {
        if ($this->fonctions->removeElement($fonction)) {
            // set the owning side to null (unless already changed)
            if ($fonction->getContact() === $this) {
                $fonction->setContact(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Paiement>
     */
    public function getPaiements(): Collection {
        return $this->paiements;
    }

    public function addPaiement(Paiement $paiement): static {
        if (!$this->paiements->contains($paiement)) {
            $this->paiements->add($paiement);
            $paiement->setPayeur($this);
        }

        return $this;
    }

    public function removePaiement(Paiement $paiement): static {
        if ($this->paiements->removeElement($paiement)) {
            // set the owning side to null (unless already changed)
            if ($paiement->getPayeur() === $this) {
                $paiement->setPayeur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Inscription>
     */
    public function getInscriptions(): Collection {
        return $this->inscriptions;
    }

    public function addInscription(Inscription $inscription): static {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions->add($inscription);
            $inscription->setContact($this);
        }

        return $this;
    }

    public function removeInscription(Inscription $inscription): static {
        if ($this->inscriptions->removeElement($inscription)) {
            // set the owning side to null (unless already changed)
            if ($inscription->getContact() === $this) {
                $inscription->setContact(null);
            }
        }

        return $this;
    }

    public function addApplisExterne(ExternalReferenceInterface $applisExterne): self {
        if (!$this->applisExternes->contains($applisExterne)) {
            $this->applisExternes->add($applisExterne);
            $applisExterne->setContact($this);
        }

        return $this;
    }

    public function removeApplisExterne(ExternalReferenceInterface $applisExterne): self {
        if ($this->applisExternes->removeElement($applisExterne)) {
            // set the owning side to null (unless already changed)
            if ($applisExterne->getContact() === $this) {
                $applisExterne->setContact(null);
            }
        }

        return $this;
    }

    public function __toString() {
        return sprintf('%s %s', $this->getPrenom(), $this->getNom());
    }

    /**
     * @return Collection<int, ContactTelephone>
     */
    public function getTelephones(): Collection {
        return $this->telephones;
    }

    public function addTelephone(ContactTelephone $telephone): static {
        if (!$this->telephones->contains($telephone)) {
            $this->telephones->add($telephone);
            $telephone->setContact($this);
        }

        return $this;
    }

    public function removeTelephone(ContactTelephone $telephone): static {
        if ($this->telephones->removeElement($telephone)) {
            // set the owning side to null (unless already changed)
            if ($telephone->getContact() === $this) {
                $telephone->setContact(null);
            }
        }

        return $this;
    }
}
