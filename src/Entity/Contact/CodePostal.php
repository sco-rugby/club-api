<?php

namespace App\Entity\Contact;

use Doctrine\ORM\Mapping as ORM;
use ScoRugby\CoreBundle\Entity\EntityInterface;
use App\Repository\Contact\CodePostalRepository;

#[ORM\Entity(repositoryClass: CodePostalRepository::class, readOnly: true)]
final class CodePostal implements EntityInterface {

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "IDENTITY")]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: "10", nullable: "true")]
    private ?string $codePostal = null;

    #[ORM\ManyToOne(inversedBy: "codePostaux")]
    private ?Commune $commune = null;

    public function getId(): ?int {
        return $this->id;
    }

    public function getCodePostal(): ?string {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getCommune(): ?Commune {
        return $this->commune;
    }

    public function setCommune(?Commune $commune): self {
        $this->commune = $commune;

        return $this;
    }
}
