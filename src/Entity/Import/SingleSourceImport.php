<?php

namespace App\Entity\Import;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[ORM\Embeddable]
final class SingleSourceImport {

    #[ORM\Column(length: 15)]
    private ?string $appli = null;

    #[ORM\Column(length: 15)]
    private ?string $externalId = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $synchronizedAt = null;

    public function getNom(): ?string {
        return $this->appli;
    }

    public function setNom(string $appli): self {
        $this->appli = $appli;

        return $this;
    }

    public function getExternalId(): ?string {
        return $this->externalId;
    }

    public function setExternalId(string $id): self {
        $this->externalId = $id;

        return $this;
    }

    public function getSynchronizedAt(): ?\DateTimeInterface {
        return $this->synchronizedAt;
    }

    public function setSynchronizedAt(?\DateTimeInterface $date = null): self {
        if (null === $date) {
            $this->synchronizedAt = new \DateTime();
        } else {
            $this->synchronizedAt = $date;
        }

        return $this;
    }
}
