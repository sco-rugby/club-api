<?php

namespace App\Model\Import;

use App\Model\Import\ImportInterface;
use App\Model\SaisonInterface;

class Fichier implements \Stringable {

    protected int $lignes;
    protected array $saisons = [];

    public function __construct(?SaisonInterface $saison = null, protected ?ImportInterface $type = null, protected ?\SplFileInfo $fichier = null) { {
            if (null !== $saison) {
                $this->addSaison($saison);
            }
        }
        return;
    }

    public function getNbLignes(): int {
        return (int) $this->lignes;
    }

    public function setNbLignes(int $lignes): void {
        $this->lignes = $lignes;
    }

    public function getSaisons(): array {
        return array_unique($this->saisons);
    }

    public function addSaison(SaisonInterface $saison): void {
        $this->saisons[] = $saison;
    }

    public function setSaisons(array $saisons): void {
        $this->saisons = $saisons;
    }

    public function getType(): ?ImportInterface {
        return $this->type;
    }

    public function setType(ImportInterface $type): void {
        $this->type = $type;
    }

    public function getFichier(): ?\SplFileInfo {
        return $this->fichier;
    }

    public function setFichier(\SplFileInfo $fichier): void {
        $this->fichier = $fichier;
    }

    public function __toString(): string {
        return (string) $this->getFichier();
    }
}
